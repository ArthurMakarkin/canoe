const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const payment = require('./payment');

const app = express();
const port = 3002;

if (process.env.NODE_ENV === 'development') {
  const webpack = require('webpack');
  const config = require('../config/webpack.development');

  const compiler = webpack(config);
  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
    stats: {
      colors: true,
    },
  }));
  app.use(require('webpack-hot-middleware')(compiler));
}

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));

app.post('/payment', payment);

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../public/index.html'));
});

// app.listen(port, '10.0.1.8', (error) => {
app.listen(port, (error) => {
// app.listen(port, '192.168.1.65', (error) => {
  if (error) {
    console.log(error); // eslint-disable-line
  } else {
    console.info(`==> 🌎  Listening on port ${port}`); // eslint-disable-line
  }
});
