/*
 *
 * You must use body-parser or similar
 *
 */

const xml = require('xml');
const crypto = require('crypto');

module.exports = function ym(body, check, aviso, secret) {
  const {
    md5,
    shopId,
    invoiceId,
    orderSumAmount,
  } = body;
  const performedDatetime = new Date().toISOString();
  const _md5 = createMD5fromOpts(body, secret);

  /*
   * Код 200 Ошибка разбора запроса
   * Магазин не в состоянии разобрать запрос. Окончательная ошибка.
   */
  if (!_md5) return createXml(body.action, {
    code: 200,
    performedDatetime,
    invoiceId,
    shopId
  });

 /*
  * Код 1 Ошибка авторизаци
  * Несовпадение значения параметра md5 с результатом расчета хэш-функции. Окончательная ошибка.
  */
  if(md5 !== _md5) return createXml(body.action, {
    code: 1,
    performedDatetime,
    invoiceId,
    shopId
  });

  switch (body.action) {
    case 'checkOrder':
      return check((
        exist,
        message = 'Магазин отказал в принятии платежа',
        techMessage = ''
      ) => {
        /*
         * Код 0 Успешно
         * Магазин дал согласие и готов принять перевод.
         */
        if (exist) return createXml(body.action, {
          code: 0,
          performedDatetime,
          invoiceId,
          shopId,
          orderSumAmount
        });

        // Object.assign({}, {
        //   code: 0,
        //   performedDatetime,
        //   invoiceId,
        //   shopId,
        //   orderSumAmount
        // }, (params || {}));

        /*
         * Код 100 Отказ в приеме перевода
         * Отказ в приеме перевода с заданными параметрами. Окончательная ошибка.
         */
        return createXml(body.action, {
          code: 100,
          performedDatetime,
          invoiceId,
          shopId,
          message,
          techMessage
        });
      });
    case 'paymentAviso':
      return aviso((
        state,
        message = 'Магазин не в состоянии принять запрос',
        techMessage = ''
      ) => {
        /*
         * Код 0 Успешно
         * Магазин дал согласие и готов принять перевод.
         */
        if (state) return createXml(body.action, {
          code: 0,
          performedDatetime,
          invoiceId,
          shopId
        });
        return createXml(body.action, {
          code: 200,
          performedDatetime,
          invoiceId,
          shopId,
          message,
          techMessage
        });
      });
    default:
      throw new Error(`Unexpected action, ${body.action}`);
  }
};

/*
 * Generete MD5 from object of opts and secret shop pass
 * return MD5 string or false if can't parse;
*/
function createMD5fromOpts(opts, secret) {
  const md5Hash = crypto.createHash('md5');
  const fields = ['action', 'orderSumAmount', 'orderSumCurrencyPaycash', 'orderSumBankPaycash', 'shopId', 'invoiceId', 'customerNumber'];

  if (typeof opts !== 'object' || opts.length === 0) return false;

  const allKeysExist = fields.every((el) => opts[el] !== undefined);

  if (!allKeysExist) return false;

  const array = fields.reduce((p, n) => {
    p.push(opts[n]);
    return p;
  }, []);
  array.push(secret);
  const str = array.join(';');
  return md5Hash.update(str, 'utf-8').digest('hex').toUpperCase();
}

function createXml(action, _attr) {
  const x = xml({ [`${action}Response`]: { _attr } }, { declaration: { encoding: 'UTF-8' } });
  return x;
}
