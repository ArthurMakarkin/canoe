const ymPN = require('./ym');
const request = require('request');
const secret = 'caNOE2016';

module.exports = function payments(req, res) {
  ymPN(
    req.body,
    // checkOrder
    (ymResXml) => {
      request({
        //url: 'https://service.canoe.ru/order',
        url: 'http://10.69.40.102:8100/order',
        method: 'POST',
        json: {
          AccountUUID: req.body.customerNumber,
          OrderUUID: req.body.orderUUID,
        },
      },
      (error, response, body) => {
        // TODO проверка заказа на существование
        if (body.return && body.return.Id === req.body.orderUUID) return res.send(ymResXml(true));
        return res.send(ymResXml(false));
      });
    },
    // paymentAviso
    (ymResXml) => {
      request({
        //url: 'https://service.canoe.ru/payment_aviso',
        url: 'http://10.69.40.102:8100/payment_aviso',
        method: 'POST',
        json: {
          OrderUUID: req.body.orderUUID,
          OrderSumAmount: req.body.orderSumAmount,
          ShopSumAmount: req.body.shopSumAmount,
          PaymentType: req.body.paymentType,
          InvoiceId: req.body.invoiceId,
        },
      },
      (error, response, body) => {
        if (body.return !== null) return res.send(ymResXml(true));
        return res.send(ymResXml(false));
      });
    },
    secret
  );
};
