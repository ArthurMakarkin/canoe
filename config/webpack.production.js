const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: {
    app: [
      'babel-polyfill',
      'whatwg-fetch',
      path.resolve(__dirname, '../client/index.jsx'),
    ],
    vendors: [
      'react',
      'react-dom',
      'react-router',
      'react-redux',
      'redux',
      'redux-thunk',
      'scrollreveal',
    ],
  },
  output: {
    path: path.resolve(__dirname, '../public'),
    publicPath: '/',
    filename: 'scripts/app.js',
  },
  resolve: {
    alias: {
      app: path.resolve(__dirname, '../client/app'),
    },
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      include: path.resolve(__dirname, '../client/'),
      use: [{
        loader: 'babel-loader'
      }]
    }, {
      test: /\.s?css$/,
      use: [
        'style-loader',
        'css-loader',
        {
          loader: 'postcss-loader',
          options: {
            plugins: () => [autoprefixer({ browsers: ['last 2 versions', 'ie >= 9', 'ios >= 8'] })]
          }
        },
        'sass-loader',
      ]
    }]
  },
  plugins: [
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    // new webpack.optimize.DedupePlugin(),
    // new webpack.optimize.OccurenceOrderPlugin(),
    // new webpack.optimize.UglifyJsPlugin({ compressor: { warnings: false }, exclude: ['../node_modules/fullpage-react/'] }),
    // new webpack.optimize.CommonsChunkPlugin('vendors', 'scripts/vendors.js'),
    new HtmlPlugin({ template: 'client/index.html' }),
    new CopyPlugin([{
      from: 'client/assets',
      to: 'assets',
    }]),
  ],
  optimization: {
    // splitChunks: { // CommonsChunkPlugin()
    //   name: 'vendors',
    //   filename: 'scripts/vendors.js'
    // },
    splitChunks: {
      chunks: 'all',
      // minSize: 30000,
      // maxSize: 0,
      // minChunks: 1,
      // maxAsyncRequests: 5,
      // maxInitialRequests: 3,
      // automaticNameDelimiter: '~',
      // name: 'vendors',
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          filename: 'scripts/vendors.js',
          priority: -10
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    }
  }
};
