const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const HtmlPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: "development",
  entry: {
    app: [
      'webpack-hot-middleware/client?reload=true',
      path.resolve(__dirname, '../client/index.jsx'),
    ],
  },
  output: {
    path: path.resolve(__dirname, '../public'),
    publicPath: '/',
    filename: 'scripts/app.js',
  },
  resolve: {
    alias: {
      app: path.resolve(__dirname, '../client/app'),
    },
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [{
      test: /\.jsx?$/,
      include: path.resolve(__dirname, '../client/'),
      use: [{
        loader: 'eslint-loader'
      },{
        loader: 'babel-loader'
      }]
    }, {
      test: /\.s?css$/,
      use: [
        'style-loader',
        'css-loader',
        {
          loader: 'postcss-loader',
          options: {
            plugins: () => [autoprefixer()]
          }
        },
          'sass-loader',
      ]
    }],
  },
  devtool: 'source-map',
  optimization: {
    noEmitOnErrors: true, // NoEmitOnErrorsPlugin
  },
  plugins: [
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlPlugin({ template: 'client/index.html' }),
    new CopyPlugin([{
      from: 'client/assets',
      to: 'assets',
    }]),
  ],
};
