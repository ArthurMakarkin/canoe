FROM node:10-alpine
RUN mkdir -p /var/www/app/canoe
COPY . /var/www/app/canoe
WORKDIR /var/www/app/canoe
RUN npm install -g && npm install && npm build
EXPOSE 3000
CMD [ "npm","run","start:production" ]
