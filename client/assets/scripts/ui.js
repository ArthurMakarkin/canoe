// UI
$(function() {
  svg4everybody();

  $(window).scroll(function(e) {
    if ($(window).scrollTop() > 110) {
      $('body').addClass('minimized-header');
    } else {
      $('body').removeClass('minimized-header');
    }
  });

  $(document).on('click', '.js-collapsed-toggler', function() {
    $(this).toggleClass('active');
  });

  $(document).on('click', '.js-toggle-long-text', function() {
    $(this).parents('.long-text').toggleClass('show-all');
  });

  $(document).on('click', '.js-add-to-basket', function() {
    var button = $(this).addClass('clicked');
    button.find('.text').text('В корзине');
  });

  $(document).on('click', '.js-toggle-filter-group', function() {
    var filters = $(this).parents('.filter-group');
    // filters.filter('.active').removeClass('active');
    filters.toggleClass('active');
  });

});
