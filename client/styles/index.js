import 'normalize.css';

import './fonts.scss';
import './icons.scss';
import './base.scss';
import './common.scss';
import './tables.scss';
import './forms.scss';
import './sliders.scss';

import './header.scss';
import './footer.scss';

import './pages/catalogue.scss';
import './pages/product.scss';
import './pages/order.scss';
import './pages/contacts.scss';
import './pages/conditions.scss';
import './pages/not-found.scss';

import './pages/main.scss';
import './pages/technologies.scss';
import './pages/collections.scss';
import './pages/brands.scss';
import './pages/collections2018.scss';
import './pages/sizes.scss';
import './pages/about.scss';
import './pages/philosophy.scss';

import './pages/contest.scss';
import './blocks/partners-and-quotes.scss';


import './swiper.scss';


// Mobile

import './mobile/icons.scss';
import './mobile/base.scss';
import './mobile/common.scss';
import './mobile/iscroll.scss';
// import './tables.scss';
import './mobile/forms.scss';
// import './sliders.scss';

import './mobile/header.scss';
import './mobile/footer.scss';

import './mobile/pages/catalogue.scss';
import './mobile/pages/product.scss';
import './mobile/pages/order.scss';
import './mobile/pages/contacts.scss';
import './mobile/pages/conditions.scss';
import './mobile/pages/not-found.scss';

import './mobile/pages/main.scss';
import './mobile/pages/technologies.scss';
import './mobile/pages/collections.scss';
import './mobile/pages/brands.scss';
import './mobile/pages/collections2018.scss';
import './mobile/pages/sizes.scss';
import './mobile/pages/about.scss';
import './mobile/pages/philosophy.scss';

import './mobile/pages/contest.scss';
import './mobile/blocks/partners-and-quotes.scss';
