@media only screen and (max-width: 768px) {
  .basket-page {
    padding-bottom: 0px;

    .main-button {
      height: 48px;
      width: 100%;
      text-align: center;
      font-size: 14px;
      line-height: 48px;

      &.dark {
        background-color: #00263A;
        color: #fff;
        border: none;

        &:disabled {
          opacity: 1;
          background-color: #A5B2BE;
        }
      }
    }

    .input-group.small-margin {
      margin-bottom: 10px;
    }

    .two-columns-inputs {
      .column + .column {
        margin-bottom: 24px;
      }
    }

    .form {
      .info {
        padding-left: 0;
      }
    }
  }

  .order-steps {
    padding: 16px 0;
    position: relative;
    height: 80px;
    background: none;
    max-width: 480px;
    width: auto;
    margin: 0 auto 116px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    // text-align: center;
    // color: #9da1a5;
    // background: #f2f2f2;
    // font-size: 0;

    // .payment-step {
    //   display: inline-block;
    // }

    .step {
      position: static;
      // display: inline-block;
      // vertical-align: middle;
      color: #DEE2E6;

      // &.active {
      //   color: #00263a;

      //   .text {
      //     visibility: visible;
      //     opacity: 1;
      //   }
      // }

      &.past {
        color: #677D80;
      }
    }

    .text {
      // position: absolute;
      top: 96px;
      right: auto;
      left: 0px;
      text-align: left;
      // visibility: hidden;
      margin-top: 0px;
      text-transform: none;
      // opacity: 0;
      font-size: 32px;
      font-weight: 300;
      line-height: 36px;
      width: 300px;
      display: block;

      span {
        display: block;
      }
    }

    .arrow {
      display: none;
      // margin: 0 25px;
      // vertical-align: middle;
    }

    .arrow-order-mobile {
      display: inline-block;
      width: 7px;
      height: 12px;
      vertical-align: middle;
      margin: 0 9px;
      color: #677D80;

      &.hidden {
        // visibility: hidden;
        color: #DEE2E6;
      }

      @media only screen and (max-width: 360px) {
        margin: 0 7px;
      }
    }

    .svg-icon-order {
      width: 48px;
      height: 48px;

      @media only screen and (max-width: 360px) {
        width: 40px;
        height: 40px;
      }
    }
  }

  .mobile-basket {
    &.margin-top {
      margin-top: -30px;
    }

    .mobile-product {
      &:first-child {
        padding-top: 16px;
        border-top: 1px solid #DEE2E6;

        .quantity,
        .btns {
          top: 16px;

          &.in-basket {
            top: 50%;
            margin-top: -20px;
          }
        }
      }

      .image {
        width: 67px;
      }

      .quantity {
        width: 62px;
        right: 40px;

        .number-input {
          .input-box {
            width: 60px;
          }
        }

        .button-remove {
          left: 80px;
        }
      }

      .price {
        position: absolute;
        bottom: 16px;
        right: 0;

        .icon-ruble {
          width: 10px;
          height: 11px;
        }
      }

      .info {
        .name {
          font-family: "Geometria", sans-serif;
          font-weight: 700;
        }

        .vendor-code {
          font-family: Georgia, sans-serif;
          font-weight: 400;
        }
      }
    }

    .products {
      margin-bottom: 35px;
    }

    .basket-info,
    .g-info {
      margin-bottom: 12px;

      label,
      .coupon-input,
      .price,
      .full-price {
        display: inline-block;
        width: 50%;
        text-align: right;
      }

      label {
        font-family: Georgia, sans-serif;
        text-align: left;
      }
    }

    .currency {
      color: #677D80;
    }

    .basket-info {
      .full-price {
        font-size: 24px;
      }

      .currency-big {
        font-size: 21px;

        .icon-ruble {
          width: 13px;
          height: 17px;
        }
      }
    }

    .coupon-input {
      font-size: 16px;
      line-height: 1;
      padding: 3px 8px;
      font-weight: 400;
      text-align: left;
    }
  }

  .orders-history-list {
    .order {
      display: flex;
      justify-content: space-between;
      color: #00263A;
      font-size: 14px;
      line-height: 20px;
      font-family: Geometria, sans-serif;
      letter-spacing: 1px;
      border-bottom: 1px solid #DEE2E6;
      padding: 16px 0;

      &:first-child {
        border-top: 1px solid #DEE2E6;
      }

      a {
        color: #00263A;
      }

      p {
        margin: 0;
      }

      .wholesale-info {
        font-family: Georgia, sans-serif;
        letter-spacing: 0;
      }

      .currency {
        .icon-ruble {
          width: 11px;
          height: 13px;
          color: #677D80;
        }
      }

      .status,
      .payment {
        font-family: Georgia, sans-serif;
        font-size: 16px;
        letter-spacing: 0;
      }

      .payment {
        color: #2AD65B;

        &.not-paid {
          color: #E7544B;
        }
      }

      .status {
        color: #677D80;
      }

      .order-number,
      .price {
        font-size: 16px;
        line-height: 20px;
        font-weight: 700;
        margin-bottom: 5px;
      }
    }
  }

  .mobile-history {
    h1 {
      margin-bottom: 4px;
    }

    .order-status,
    .payment-status {
      font-family: Georgia, sans-serif;
      font-size: 16px;
      line-height: 20px;
      margin-bottom: 24px;
    }

    .payment-status {
      color: #2AD65B;

      &.not-paid {
        color: #E7544B;
      }
    }

    .mobile-basket {
      .mobile-product {
        .quantity {
          font-size: 14px;
          line-height: 20px;
          height: auto;
          width: auto;
          right: 0;
          top: 40px;
        }
      }
    }
  }

  // .payment-type {
  //   white-space: nowrap;

  //   .cell {
  //     padding-top: 80px;
  //   }
  // }

  .order-info {
    text-align: left;

    &.margin-top {
      margin-top: -30px;
    }

    .info-heading {
      margin: 0px 0 10px;
      text-align: left;
      font-size: 14px;
      line-height: 24px;
    }

    .info-text {
      font-size: 16px;
      line-height: 20px;
      // margin-bottom: 20px;
      // font-family: 'Georgia';

      a,
      span {
        font-weight: 700;
      }

    }

    .mobile-link {
      display: block;
      margin-top: 40px;
      color: #00263A;
    }
  }

  .order-button-wrapper {
    // position: relative;
    width: auto;
    margin: 40px 0 0;
    // text-align: center;

    .back-link {
      // position: absolute;
      // top: 0;
      // left: 0;
      // line-height: 40px;
      display: none;
    }
  }

  .payment-method {
    margin: 0 0 40px;

    li {
      margin: 0 0 20px;
    }

    li:before {
      content: "";
      display: none;
    }

    label {
      white-space: nowrap;
      position: relative;
      top: -6px;
    }
  }

  // .payment-form {
  //   .radio-group {
  //     position: relative;
  //     margin-top: 25px;

  //     &:last-child {
  //       margin-bottom: 35px;
  //     }

  //     .link {
  //       position: absolute;
  //       bottom: 2px;
  //       left: 250px;
  //       white-space: nowrap;
  //       text-decoration: underline;
  //       font-family: 'Georgia';
  //       font-size: 14px;
  //       line-height: 1;
  //     }
  //   }

  //   .price {
  //     margin-top: 25px;
  //     margin-bottom: 90px;
  //     padding-left: 30px;
  //     font-size: 24px;
  //   }
  // }

  .delivery-price {
    // position: relative;
    top: 0px;

    // .label {
    //   display: inline-block;
    //   width: 185px;
    // }

    .price {
      display: block;

      .currency {
        color: #677D80;

        .icon-ruble {
          width: 12px;
          height: 17px;
        }
      }
    }

    // .sum {
    //   font-size: 24px;
    //   margin-right: 3px;
    // }
  }

  .pvz-selector {
    margin: 0px 0 24px;

    .mobile-map-label {
      display: block;
      font-size: 14px;
      text-transform: uppercase;
      color: #677d80;
      font-weight: 700;
      margin-bottom: 8px;
    }

    .map {
      display: block;
      width: 100%;
      height: 300px;
      margin-bottom: 24px;
      // vertical-align: top;
    }
  }


  .pvz-info {
    display: block;
    width: 100%;
    margin-left: 0px;
    padding-top: 0px;
    // vertical-align: top;
    border-bottom: 1px solid #A5B2BE;

    .map-label {
      display: none;
    }

    // .label {
    //   padding-left: 0;
    // }

    .title {
      font-size: 32px;
      line-height: 36px;
      font-weight: 300;
      margin: 20px 0 6px;
    }

    .address {
      font-family: Georgia, sans-serif;
      font-size: 16px;
      line-height: 20px;
      margin: 0px 0 24px;
    }

    .details {
      margin-top: 0px;
      padding-bottom: 24px;

      .row + .row {
        margin-top: 24px;
      }

      .label {
        display: block;
        // vertical-align: top;
        width: auto;
        line-height: 16px;
      }

      .value {
        display: block;
        // vertical-align: top;
        width: auto;
        font-family: 'Georgia', sans-serif;
      }

      .value:not(.how-to-go)  {
        font-weight: 400;
        font-size: 16px;
        text-transform: none;
        line-height: 24px;
      }

      .how-to-go {
        font-family: 'Georgia';
        font-weight: 400;
        font-size: 16px;
        margin-top: -3px;
      }
    }
  }
}