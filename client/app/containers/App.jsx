import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';

import Header from 'app/components/Header';
import Footer from 'app/components/Footer';
import MediaQuery from 'react-responsive';

import MobileDetect from 'mobile-detect';
import * as actions from 'app/actions';

class App extends Component {
  componentWillMount() {
    const md = new MobileDetect(window.navigator.userAgent);
    window.device = {
      mobile: md.mobile(),
      phone: md.phone(),
      tablet: md.tablet(),
    }
  }

  componentDidMount() {
    const { setWholesaleMode, setPageWidth } = this.props.actions;
    const { user } = this.props;
    let resizeTimeout = undefined;
    setWholesaleMode(user.AccountTYPE === 'wholesale');

    const resizeHandler = () => {
      clearTimeout(resizeTimeout);
      resizeTimeout = setTimeout(() => {
        setPageWidth(window.innerWidth);
      }, 100);
    }

    if ('onorientationchange' in window) {
      window.addEventListener('orientationchange', resizeHandler);
    }

    window.addEventListener('resize', resizeHandler);

    document.body.addEventListener('focusout', (event) => {
      const target = event.target;
      if (target.classList.contains('text-input') && target.value) {
        target.parentNode.parentNode.classList.add('correct');
      }
    });

    document.body.addEventListener('focusin', (event) => {
      const target = event.target;
      if (target.classList.contains('text-input')) {
        target.parentNode.parentNode.classList.remove('error');
      }
    });

    resizeHandler();
  }

  render() {
    const footer = this.props.app.footer;

    return (
      <div
        id="appContainer"
        className={classNames({
          'app-container': true,
          wholesale: this.props.app.wholesaleMode,
          'mobile': window.device.mobile,
          'phone': window.device.phone,
          'tablet': window.device.tablet,
        })}
      >
        <Header />
        {this.props.children}
        <MediaQuery query='(min-width: 769px)'>
          { footer &&
            <Footer />
          }
        </MediaQuery>
        <MediaQuery query='(max-width: 768px)'>
          { footer &&
            <div className="mobile-footer">
              <svg className="svg-logo-feather">
                <use xlinkHref="/assets/images/sprite.svg#logo-feather" />
              </svg>
              <p className="footer-text">© 2017 Canoe, Inc. All Rights Reserved.</p>
            </div>
          }
        </MediaQuery>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { app, user } = state;
  return {
    app,
    user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
