import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';


const BottomButtons = ({
  buttons,
  title,
  description,
}) => {

  const hasTitle = title.length > 0;
  const hasDescription = description.length > 0;
  const hasButtons = buttons.length > 0;

  return (
    <div className="collections-buttons">
      <div className="collections-buttons__header">
        {
          hasTitle &&
          <div className="collections-buttons__header__title">
            {title}
          </div>
        }
        {
          hasDescription &&
          <div className="collections-buttons__header__description">
            {description}
          </div>
        }
      </div>
      {
        hasButtons &&
        <div className="collections-buttons__list">
          {
            buttons.map((button, index) => (
              <Link key={index} className="collections-buttons__list__item" to={button.url}>
                {button.name}
                <svg className="arrow-order-mobile">
                  <use xlinkHref="/assets/images/sprite.svg#arrow-order-mobile" />
                </svg>
              </Link>
            ))
          }
        </div>
      }
    </div>
  );
};

BottomButtons.propTypes = {
  buttons: PropTypes.array,
  title: PropTypes.string,
  description: PropTypes.string,
};

export default BottomButtons;
