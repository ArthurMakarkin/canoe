import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import classNames from 'classnames';

import MediaQuery from 'react-responsive';

import * as actions from 'app/actions';

import api from 'app/actions/api';

const $ = window.jQuery;

export class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      restoration: null,
      mobileMenuShowed: false,
      mobileSubmenu: '',
      registrationStep: '',
    };

    this.handleErrors = this.handleErrors.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleRestore = this.handleRestore.bind(this);
    this.handleRegisterWholesale = this.handleRegisterWholesale.bind(this);
    this.handleRegisterRetail = this.handleRegisterRetail.bind(this);
  }

  componentDidUpdate() {
    this.handleErrors();
  }

  handleErrors() {
    const { errors } = this.props;
    const formWithErrors = document.forms[errors.form];

    if (formWithErrors && errors.data) {
      [].map.call(document.querySelectorAll('.input-group.error'), (group) => {
        group.classList.remove('error');
      });

      errors.data.forEach((error) => {
        const input = formWithErrors.elements[error.name];
        const classes = input.parentNode.parentNode.classList;
        classes.remove('correct');
        classes.add('error');
      });
    }
  }

  handleLogin(event) {
    event.preventDefault();
    const form = event.target;
    this.props.actions.fetchUser({
      username: form.elements.username.value,
      password: form.elements.password.value,
    });
  }

  handleRestore(event) {
    event.preventDefault();
    const form = event.target;

    api('restore_password', {
      email: form.elements.email.value,
    }).then((data) => {
      form.elements.email.value = '';
      this.setState({ restoration: data });
    });
  }

  handleRegisterWholesale(event) {
    event.preventDefault();
    const form = event.target;
    this.props.actions.registerWholesale({
      email: form.elements.email.value,
      phone: form.elements.phone.value,
      firstname: form.elements.firstname.value,
      lastname: form.elements.lastname.value,
      patronymic: form.elements.patronymic.value,
      password: form.elements.password.value,
      password_confirmed: form.elements.password_confirmed.value,
      company_name: form.elements.company_name.value,
      resident: true,
      inn: form.elements.inn.value,
    });
  }

  handleRegisterRetail(event) {
    event.preventDefault();
    const form = event.target;
    this.props.actions.registerRetail({
      email: form.elements.email.value,
      phone: form.elements.phone.value,
      firstname: form.elements.firstname.value,
      lastname: form.elements.lastname.value,
      patronymic: form.elements.patronymic.value,
      password: form.elements.password.value,
      password_confirmed: form.elements.password_confirmed.value,
    });
  }

  closeMobileMenu() {
    let state = this.state;
    state.mobileSubmenu = '';
    state.mobileMenuShowed = false;
    $('.page-content').removeClass('crop')
    this.setState(state);
  }

  toggleMobileMenu() {
    let state = this.state;
    if(this.state.mobileSubmenu !== ''){
      state.mobileSubmenu = '';
    } else {
      state.mobileMenuShowed = !state.mobileMenuShowed;
      $('.page-content').toggleClass('crop')
    }
    this.setState(state);
  }

  showSubmenu(name) {
    let state = this.state;
    state.mobileSubmenu = name;
    this.setState(state);
  }

  toggleRegistration(step) {
    let state = this.state;
    state.registrationStep = step;
    this.setState(state);
    if(step == ''){
      document.querySelector('.header-forms').classList.remove('active');
    }
  }

  render() {
    const { app, basket, user, errors } = this.props;
    const { restoration } = this.state;
    const anonymous = user.AccountUUID === 0;

    return (
      <header className="page-header">
        <div className="header-main">
          <div className="content-wrapper">
            <div className="menu-btn" onClick={this.toggleMobileMenu.bind(this)}>
              <svg className="icon svg-icon-hamburger"><use xlinkHref="/assets/images/sprite.svg#icon-hamburger" /></svg>
            </div>
            <nav className={classNames("site-nav", {
              "active": this.state.mobileMenuShowed,
              "submenu-active": this.state.mobileSubmenu !== '',
            })}>
              <MediaQuery query='(min-width: 769px)'>
                <Link className="link" to="/catalogue">Магазин</Link>
              </MediaQuery>
              <MediaQuery query='(max-width: 768px)'>
                <span className="switch-group">
                  <span className="switch">
                    <input
                      className="switch-input js-customer-type"
                      type="checkbox"
                      id="customer-type-switch"
                      disabled={!anonymous}
                      checked={app.wholesaleMode}
                      onChange={() => this.props.actions.setWholesaleMode(!app.wholesaleMode)}
                    />
                    <span className="switch-circle" />
                  </span>
                  <label className="switch-label" htmlFor="customer-type-switch"><span className="desktop-text">{app.wholesaleMode ? 'Опт' : 'Розница'}</span><span className="mobile-text">Опт</span></label>
                </span>
              </MediaQuery>
              <MediaQuery query='(max-width: 768px)'>
                <span className="link no-action" onClick={() => (this.showSubmenu('catalogue'))}>Магазин</span>
              </MediaQuery>
              <div className={classNames("site-subnav", {'active': this.state.mobileSubmenu === 'catalogue'})}>
                <div className="subnav-wrapper">
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/catalogue/men">Мужчины</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/catalogue/women">Женщины</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/catalogue/boys">Мальчики</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/catalogue/girls">Девочки</Link>
                  <div className="link close-btn" onClick={this.toggleMobileMenu.bind(this)}>
                    <svg className="icon svg-icon-menu-arrow"><use xlinkHref="/assets/images/sprite.svg#icon-menu-arrow-mobile" /></svg>
                  </div>
                </div>
              </div>
              <span className="link no-action" onClick={() => (this.showSubmenu('lookbook'))}>Лукбук</span>
              <div className={classNames("site-subnav", {'active': this.state.mobileSubmenu === 'lookbook'})}>
                <div className="subnav-wrapper">
                  {/*<Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/contest">Конкурс</Link>*/}
                  {/*<Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/collections/art-project">Арт-проект</Link>*/}
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/collections/night">Кампания 2020</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/collections/winter_2019">Зима 20</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/collections/winter_2018">Зима 19</Link>
                  <div className="link close-btn" onClick={this.toggleMobileMenu.bind(this)}>
                    <svg className="icon svg-icon-menu-arrow"><use xlinkHref="/assets/images/sprite.svg#icon-menu-arrow-mobile" /></svg>
                  </div>
                </div>
              </div>
              <span className="link no-action" onClick={() => (this.showSubmenu('company'))}>Бренд</span>
              <div className={classNames("site-subnav", {'active': this.state.mobileSubmenu === 'company'})}>
                <div className="subnav-wrapper">
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/brand/legend">Легенда</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/brand/about">О нас</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/brand/philosophy">Философия</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/contacts">Контакты</Link>
                  <div className="link close-btn" onClick={this.toggleMobileMenu.bind(this)}>
                    <svg className="icon svg-icon-menu-arrow"><use xlinkHref="/assets/images/sprite.svg#icon-menu-arrow-mobile" /></svg>
                  </div>
                </div>
              </div>
              <span className="link no-action" onClick={() => (this.showSubmenu('tech'))}>Инновации</span>
              <div className={classNames("site-subnav", {'active': this.state.mobileSubmenu === 'tech'})}>
                <div className="subnav-wrapper">
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/technologies">Обзор</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/technologies/teflon">Teflon</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/technologies/h2dry">H2Dry</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/technologies/high-twist">High Twist</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/technologies/3d-tube">3D Tube</Link>
                  <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/technologies/nano-silver">Nano Silver</Link>
                  <div className="link close-btn" onClick={this.toggleMobileMenu.bind(this)}>
                    <svg className="icon svg-icon-menu-arrow"><use xlinkHref="/assets/images/sprite.svg#icon-menu-arrow-mobile" /></svg>
                  </div>
                </div>
              </div>
              <MediaQuery query='(max-width: 768px)'>
                {anonymous ? (
                  <span className="link no-action js-toggle-header-forms" onClick={() => (this.toggleRegistration('login'))}>Войти</span>
                ) : (
                  <div className="profile-links">
                    <span className="link no-action" onClick={() => (this.showSubmenu('profile'))}>Ваш кабинет</span>
                    <div className={classNames("site-subnav", {'active': this.state.mobileSubmenu === 'profile'})}>
                      <div className="subnav-wrapper">
                        <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/profile">Профиль</Link>
                        <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/basket">Корзина</Link>
                        <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/orders">Ваши заказы</Link>
                        {/*<Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/sizes">Размеры</Link>*/}
                        <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/conditions">Условия работы</Link>
                        <Link className="link" onClick={this.closeMobileMenu.bind(this)} to="/logout">Выйти</Link>
                        <div className="link close-btn" onClick={this.toggleMobileMenu.bind(this)}>
                          <svg className="icon svg-icon-menu-arrow"><use xlinkHref="/assets/images/sprite.svg#icon-menu-arrow-mobile" /></svg>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </MediaQuery>
              <div className="link close-btn" onClick={this.toggleMobileMenu.bind(this)}>
                <svg className="icon svg-icon-cross"><use xlinkHref="/assets/images/sprite.svg#icon-cross-mobile" /></svg>
              </div>
              {/*
              <span className="link no-action">Бренд</span>
              <div className="site-subnav">
                <div className="content-wrapper">
                  <Link className="link" to="/contacts">Контакты</Link>
                </div>
              </div>
              */}
            </nav>
            {' '}
            <Link className="logo" to="/">
              <svg className="svg-logo">
                <use xlinkHref="/assets/images/sprite.svg#logo" />
              </svg>
            </Link>
            {' '}
            <div className="controls">
              <MediaQuery query='(min-width: 769px)' className="user">
                <span
                  className={classNames({
                    link: true,
                    // 'js-toggle-header-forms': anonymous,
                  })}
                 onClick={anonymous ? (() => (this.toggleRegistration('login'))) : undefined}
                >
                  <svg className="icon svg-icon-user"><use xlinkHref="/assets/images/sprite.svg#icon-user" /></svg>
                  {anonymous ? (
                    <span className="text">Вход</span>
                  ) : (
                    <Link to="/profile" className="text">
                      {user.AccountName}
                    </Link>
                  )}
                </span>
                {anonymous ? null : (
                  <div className="site-subnav">
                    <div className="subnav-wrapper">
                      <Link className="link" to="/basket">Корзина</Link>
                      <Link className="link" to="/profile">Профиль</Link>
                      <Link className="link" to="/orders">Ваши заказы</Link>
                      {/*<Link className="link" to="/sizes">Размеры</Link>*/}
                      <Link className="link" to="/conditions">Условия работы</Link>
                      <Link className="link" to="/logout">Выйти</Link>
                    </div>
                  </div>
                )}
              </MediaQuery>
              <Link className="link basket-link" to="/basket">
                <svg className="icon svg-icon-basket">
                  <use xlinkHref="/assets/images/sprite.svg#icon-basket" />
                </svg>
                <MediaQuery query='(max-width: 768px)'>
                  { basket.BasketList &&
                    <span className={classNames("text", {"hidden": basket.BasketList.length == 0})}>{basket.BasketList.length}</span>
                  }
                </MediaQuery>
                <MediaQuery query='(min-width: 769px)'>
                  <span className="text">{basket.BasketList ? basket.TotalSumm : ' '} ₽</span>
                </MediaQuery>
              </Link>
              {/*
              <Link className="link" to="/en">
                <span className="text">Eng</span>
              </Link>
              */}
              <MediaQuery query='(max-width: 768px)'>
                <span className="switch-group">
                  {app.wholesaleMode &&
                    <span className="mobile-text">Опт</span>
                  }
                </span>
              </MediaQuery>
              <MediaQuery query='(min-width: 769px)'>
                <span className="switch-group">
                  <span className="switch">
                    <input
                      className="switch-input js-customer-type"
                      type="checkbox"
                      id="customer-type-switch"
                      disabled={!anonymous}
                      checked={app.wholesaleMode}
                      onChange={() => this.props.actions.setWholesaleMode(!app.wholesaleMode)}
                    />
                    <span className="switch-circle" />
                  </span>
                  <label className="switch-label" htmlFor="customer-type-switch"><span className="desktop-text">{app.wholesaleMode ? 'Опт' : 'Розница'}</span><span className="mobile-text">Опт</span></label>
                </span>
              </MediaQuery>
            </div>
          </div>
        </div>
        {anonymous ? (
          <div className={classNames("header-forms", {
            'registration': this.state.registrationStep === 'registration',
            'restoration': this.state.registrationStep === 'restoration',
            'active': this.state.registrationStep !== '',
          })}>
            <div className="content-wrapper">
              <div className={classNames("forms-header", {
                "active": this.state.registrationStep === 'registration'
              })}>
                <p className="title">Регистрация</p>
                <div className="next-btn js-toggle-registration" onClick={() => (this.toggleRegistration('registration'))}>
                  <svg className="svg-arrow-right">
                    <use xlinkHref="/assets/images/sprite.svg#arrow-right" />
                  </svg>
                </div>
                <span className="switch-group">
                  <span className="switch">
                    <input
                      className="switch-input js-customer-type"
                      type="checkbox"
                      id="customer-type-switch"
                      disabled={!anonymous}
                      checked={app.wholesaleMode}
                      onChange={() => this.props.actions.setWholesaleMode(!app.wholesaleMode)}
                    />
                    <span className="switch-circle" />
                  </span>
                  <label className="switch-label" htmlFor="customer-type-switch">Опт</label>
                </span>

              </div>
              <button className="close-button js-toggle-header-forms" onClick={() => this.toggleRegistration('')}>
                <svg className="svg-icon-cross">
                  <use xlinkHref="/assets/images/sprite.svg#icon-cross" />
                </svg>
              </button>

              <form className="form login-form" onSubmit={this.handleLogin} name="user_info">
                <div className="inputs">
                  <div className="input-group">
                    <label className="label" htmlFor="#">Электронная почта</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" name="username" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                  <div className="input-group">
                    <label className="label" htmlFor="#">Пароль</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="password" name="password" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                  <span className="pseudo-link js-toggle-restoration" onClick={() => (this.toggleRegistration('restoration'))}>Забыли пароль?</span>
                </div>
                <div className="buttons">
                  <div>
                    <button className="round-button" type="submit">Вход</button>
                    <span className="round-button js-toggle-registration" onClick={() => (this.toggleRegistration('registration'))}>Регистрация</span>
                  </div>
                  {errors.form === 'user_info' ? (
                    <div className="errors">
                      {errors.data ? errors.data.map((error, index) => (
                        <div className="error" key={index}>{error.caption.ru}</div>
                      )) : null}
                      {errors.message ? (
                        <div className="error">{errors.message.ru}</div>
                      ) : null}
                    </div>
                  ) : null}
                </div>
              </form>

              <form className="form restore-form" onSubmit={this.handleRestore}>
                <div className="inputs">
                  <div className="input-group">
                    <MediaQuery query='(min-width: 769px)'>
                      <label className="label" htmlFor="#">Электронная почта для восстановления пароля</label>
                      <div className="input-wrapper">
                        <input className="text-input" type="text" name="email" />
                        <svg className="check svg-icon-input-check">
                          <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                        </svg>
                      </div>
                    </MediaQuery>
                    <MediaQuery query='(max-width: 768px)'>
                      <label className="label" htmlFor="#">Электронная почта</label>
                      <div className="input-wrapper">
                        <input className="text-input" type="text" name="email" placeholder="Адрес почты для восстановления пароля" />
                        <svg className="check svg-icon-input-check">
                          <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                        </svg>
                      </div>
                      <p className="comment">На указанный адрес будет отправлен временный пароль.</p>
                      <p className="comment"><span onClick={() => (this.toggleRegistration('login'))}>Вспомнили пароль?</span></p>
                    </MediaQuery>
                  </div>
                </div>
                <div className="buttons">
                  <div>
                    <button className="round-button" type="submit">Отправить</button>
                    <span className="round-button js-toggle-restoration" onClick={() => (this.toggleRegistration('login'))}>Вспомнил</span>
                  </div>
                  {restoration && restoration.name === 'DATA_VALIDATION_ERROR' ? (
                    <div className="errors">
                      {restoration.error.data.map(error => (
                        <div className="error">{error.caption.ru}</div>
                      ))}
                    </div>
                  ) : null}
                </div>
                {restoration && restoration.ru ? (
                  <div className="success">{restoration.ru}</div>
                ) : null}
              </form>

              {app.wholesaleMode ? (
                <form
                  className="form registration-form"
                  onSubmit={this.handleRegisterWholesale}
                  name="reg_wholesale"
                >
                  <div className="form-description">
                    Для регистрации в качестве нового <strong>оптового покупателя</strong>, пожалуйста, заполните указанные ниже поля.
                  </div>
                  <p className="already-registered"><span className="js-toggle-registration" onClick={() => (this.toggleRegistration('login'))}>Вы уже зарегистрированы?</span></p>
                  <div className="inputs two-columns-inputs">
                    <div className="column">
                      <div className="input-group">
                        <label className="label" htmlFor="#">Название компании</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="company_name" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Номер телефона</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="phone" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Имя</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="firstname" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Фамилия</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="lastname" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Отчество</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="patronymic" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                    </div>
                    <div className="column">
                      <div className="input-group">
                        <label className="label" htmlFor="#">ИНН</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="inn" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Электронная почта</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="email" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Пароль</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="password" name="password" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Повторите пароль</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="password" name="password_confirmed" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="checkbox-input">
                          <input className="input" type="checkbox" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                        <label className="checkbox-label" htmlFor="#">С условиями <a className="link" href="http://media.canoe.ru/downloads/wholesale_agreement.pdf" target="_blank" rel="noopener noreferrer">типового договора</a> ознакомлен</label>
                      </div>
                      {errors.form === 'reg_wholesale' ? (
                        <div className="errors">
                          {errors.data.map(e => (
                            <div className="error">{e.caption.ru}</div>
                          ))}
                        </div>
                      ) : null}
                    </div>
                  </div>
                  <div className="buttons">
                    <button className="round-button" type="submit">Готово</button>
                    <div className="link-wrapper">
                      <span className="pseudo-link js-toggle-registration" onClick={() => (this.toggleRegistration('login'))}>Вы уже<br />зарегистрированы?</span>
                    </div>
                  </div>
                </form>
              ) : (
                <form
                  className="form registration-form"
                  onSubmit={this.handleRegisterRetail}
                  name="reg_retail"
                >
                  <div className="form-description">
                    Для регистрации нового <strong>розничного покупателя</strong>, пожалуйста, заполните указанные ниже поля
                  </div>
                  <p className="already-registered"><span className="js-toggle-registration" onClick={() => (this.toggleRegistration('login'))}>Вы уже зарегистрированы?</span></p>
                  <div className="inputs two-columns-inputs">
                    <div className="column">
                      <div className="input-group">
                        <label className="label" htmlFor="#">Имя</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="firstname" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Фамилия</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="lastname" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Отчество</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="patronymic" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Телефон</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="phone" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                    </div>
                    <div className="column">
                      <div className="input-group">
                        <label className="label" htmlFor="#">Электронная почта</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="text" name="email" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Пароль</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="password" name="password" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <label className="label" htmlFor="#">Повторите пароль</label>
                        <div className="input-wrapper">
                          <input className="text-input" type="password" name="password_confirmed" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                      </div>
                      <div className="input-group">
                        <div className="checkbox-input">
                          <input className="input" type="checkbox" />
                          <svg className="check svg-icon-input-check">
                            <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                          </svg>
                        </div>
                        <label className="checkbox-label" htmlFor="#">Согласен на обработку персональной информации</label>
                      </div>
                      {errors.form === 'reg_retail' ? (
                        <div className="errors">
                          {errors.data.map((error, index) => (
                            <div className="error" key={index}>{error.caption.ru}</div>
                          ))}
                        </div>
                      ) : null}
                    </div>
                  </div>
                  <div className="buttons">
                    <button className="round-button" type="submit">Готово</button>
                    <div className="link-wrapper">
                      <span className="pseudo-link js-toggle-registration" onClick={() => (this.toggleRegistration('login'))}>Вы уже<br />зарегистрированы?</span>
                    </div>
                  </div>
                </form>
              )}
            </div>
          </div>
        ) : null}
      </header>
    );
  }
}

function mapStateToProps(state) {
  const { app, basket, errors, user } = state;
  return {
    app,
    basket,
    errors,
    user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
