import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import * as actions from 'app/actions';

export class Footer extends Component {
  render() {
    const anonymous = this.props.user.AccountUUID === 0;

    return (
      <footer className="page-footer">
        <div className="content-wrapper">
          <nav className="site-nav">
            <div className="logo-column">
              <svg className="svg-logo-feather">
                <use xlinkHref="/assets/images/sprite.svg#logo-feather" />
              </svg>
            </div>
            <div className="nav-column">
              <div className="column-title">Магазин</div>
              <Link className="link" to="/catalogue/men">Мужчины</Link><br />
              <Link className="link" to="/catalogue/women">Женщины</Link><br />
              <Link className="link" to="/catalogue/boys">Мальчики</Link><br />
              <Link className="link" to="/catalogue/girls">Девочки</Link><br />
            </div>
            <div className="nav-column">
              <div className="column-title">Лукбук</div>
              <Link className="link" to="/contest">Конкурс</Link><br />
              {/*<Link className="link" to="/collections/art-project">Арт-проект</Link><br />*/}
              <Link className="link" to="/collections/night">Кампания 19</Link><br />
              <Link className="link" to="/collections/winter_2019">Зима 19</Link><br />
              <Link className="link" to="/collections/summer_2018">Лето 18</Link><br />
            </div>
            <div className="nav-column">
              <div className="column-title">Бренд</div>
              <Link className="link" to="/brand/legend">Легенда</Link><br />
              <Link className="link" to="/brand/philosophy">Философия</Link><br />
              <Link className="link" to="/brand/about">О нас</Link><br />
              <Link className="link" to="/contacts">Контакты</Link><br />
            </div>
            <div className="nav-column">
              <div className="column-title">Инновации</div>
              <Link className="link" to="/technologies">Обзор</Link><br />
              <Link className="link" to="/technologies/teflon">Teflon</Link><br />
              <Link className="link" to="/technologies/h2dry">H2Dry</Link><br />
              <Link className="link" to="/technologies/high-twist">High Twist</Link><br />
              <Link className="link" to="/technologies/3d-tube">3D Tube</Link><br />
              <Link className="link" to="/technologies/nano-silver">Nano Silver</Link><br />
            </div>
            <div className="nav-column">
              <div className="column-title">Личный кабинет</div>
              <Link className="link" to="/basket">Корзина</Link><br />
              {/*<Link className="link" to="/sizes">Размеры</Link><br />*/}
              <Link className="link" to="/conditions">Условия работы</Link><br />
              {anonymous ? null : (
                <div>
                  <Link className="link" to="/profile">Профиль</Link><br />
                  <Link className="link" to="/orders">Ваши заказы</Link><br />
                  <Link className="link" to="/logout">Выйти</Link><br />
                </div>
              )}
            </div>
          </nav>
          <div className="copyright">&copy; 2016 Canoe, Inc. All Rights Reserved.</div>
          <div className="social-links">
            <a className="link" href="https://vk.com/headwear" target="_blank" rel="noopener noreferrer">
              <svg className="icon">
                <use xlinkHref="/assets/images/sprite.svg#icon-vk" />
              </svg>
            </a>
            <a className="link" href="https://www.instagram.com/canoeheadwear/" target="_blank" rel="noopener noreferrer">
              <svg className="icon">
                <use xlinkHref="/assets/images/sprite.svg#icon-instagram" />
              </svg>
            </a>
            <a className="link" href="https://www.facebook.com/canoe.ru" target="_blank" rel="noopener noreferrer">
              <svg className="icon">
                <use xlinkHref="/assets/images/sprite.svg#icon-facebook" />
              </svg>
            </a>
            {/*
            <a className="link" href="https://www.linkedin.com/company/canoe.ru" target="_blank" rel="noopener noreferrer">
              <svg className="icon">
                <use xlinkHref="/assets/images/sprite.svg#icon-linkedin" />
              </svg>
            </a>
            */}
          </div>
        </div>
      </footer>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state;
  return {
    user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
