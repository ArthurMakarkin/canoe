import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

export default class TechnologiesBanner extends Component {
  static propTypes = {
    technologies: PropTypes.array,
  }

  constructor(props) {
    super(props);

    this.state = {
      activeIndex: 0,
    };
  }

  handleClick = (activeIndex) => {
    this.setState({ activeIndex });
  }

  render() {
    const { technologies } = this.props;
    const { activeIndex } = this.state;

    return (
      <div className="banner-with-technologies full-width" id="technologies-banner">
        <div className="content-wrapper">
          <div className="columns">
            <div className="column">
              <h2 className="heading">Использованные<br />материалы и технологии</h2>
              <div className="divider" />
              <div className="buttons">
                {technologies.map((tech, index) =>
                  <button
                    key={index}
                    className={classNames({
                      button: true,
                      active: index === activeIndex,
                    })}
                    onClick={() => this.handleClick(index)}
                  >
                    <img className="icon" src={tech.Icon.Url} role="presentation" />
                  </button>
                )}
              </div>
              <div className="technologies">
                {technologies.map((tech, index) =>
                  <div
                    key={index}
                    className={classNames({
                      technologie: true,
                      active: index === activeIndex,
                    })}
                  >
                    <div className="title">{tech.Name.ru}</div>
                    <div className="text">{tech.Description.ru}</div>
                    {/* tech.DetailsURL != "" &&
                      <Link key={'link-' + index} className="b-with-media__content__link" to={tech.DetailsURL}>Подробнее</Link>
                    */}

                  </div>
                )}
              </div>
            </div>
            <div className="column">
              {technologies.map((tech, index) =>
                <div
                  key={index}
                  className={classNames({
                    image: true,
                    active: index === activeIndex,
                  })}
                  style={{ backgroundImage: `url(${tech.Picture.Url})` }}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
