import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class NumberInput extends Component {
  static defaultProps = {
    value: 1,
    maxValue: 999,
  }

  static propTypes ={
    id: PropTypes.string,
    value: PropTypes.number,
    maxValue: PropTypes.number,
    onChangeCallback: PropTypes.func,
  }

  constructor(props) {
    super(props);

    this.state = {
      value: props.value,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.onChangeCallback && this.state.value != this.props.value && this.state.value != ''){
      this.props.onChangeCallback();
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ value: nextProps.value });
  }

  handleChange = (event) => {
    const value = +event.target.value;
    const { maxValue } = this.props;
    if (value) {
      if (value > maxValue) {
        this.setState({ value: maxValue });
      } else if (value > 0 && value < maxValue) {
        this.setState({ value });
      }
    } else {
      this.setState({ value: '' });
    }
  }

  handleIncrement = () => {
    const { value } = this.state;
    const { maxValue } = this.props;
    if (value < maxValue) this.setState({ value: value + 1 });
  }

  handleDecrement = () => {
    const { value } = this.state;
    if (value > 1) this.setState({ value: value - 1 });
  }

  render() {
    const { id } = this.props;
    const { value } = this.state;

    return (
      <div className="number-input">
        <input
          className="input-box"
          id={id}
          type="text"
          value={value}
          onChange={this.handleChange}
          data-quantity
        />
        <div className="input-arrows">
          <button className="input-arrow input-arrow-up" onClick={this.handleIncrement}>
            <svg className="arrow svg-arrow-up">
              <use xlinkHref="/assets/images/sprite.svg#arrow-up" />
            </svg>
          </button>
          <button className="input-arrow input-arrow-down" onClick={this.handleDecrement}>
            <svg className="arrow svg-arrow-down">
              <use xlinkHref="/assets/images/sprite.svg#arrow-down" />
            </svg>
          </button>
        </div>
      </div>
    );
  }
}
