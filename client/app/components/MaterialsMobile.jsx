import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';
import { Link } from 'react-router';

const $ = window.jQuery;

export default class MaterialsMobile extends Component {
  static propTypes = {
    materials: PropTypes.array,
    noLinks: PropTypes.bool,
  }

  onRefresh(inst) {
    const scroller = $(inst.scroller);
    scroller.width(scroller.find(".b-materials__item").length * 130);
  }

  render() {
    const { materials, noLinks } = this.props;
    let blocks;

    const options = {
      scrollX: true,
      scrollY: false,
      // snap: '.item',
      // probeType: 3,
      // bounce: false,
      // bounceTime: 0,
      // deceleration: 0,
      // momentum: false,
      // mouseWheel: true,
      click: true,
      preventDefault: false,
      eventPassthrough: true,
    }

    if(noLinks){
      blocks = materials.map((material, index) =>
              <span className="b-materials__item" key={index}>
                <span className="b-materials__item__icon" style={{ backgroundImage: `url(${material.Icon})` }} />
                <p className="b-materials__item__title">{material.Title.split(' ').map((word, index) => (
                  <span key={index}>{word}</span>
                ))}</p>
              </span>
            )
    } else {
      blocks = materials.map((material, index) =>
              <Link className="b-materials__item" to={material.URL} key={index}>
                <span className="b-materials__item__icon" style={{ backgroundImage: `url(${material.Icon})` }} />
                <p className="b-materials__item__title">{material.Title.split(' ').map((word, index) => (
                  <span key={index}>{word}</span>
                ))}</p>
              </Link>
            )
    }

    return (
      <ReactIScroll
        className="b-materials__list b-iscroll"
        onRefresh={(inst) => this.onRefresh(inst)}
        options={options}
        iScroll={iScroll}>

        <div className="b-iscroll-container">
          {blocks}
        </div>
      </ReactIScroll>
    );
  }
}
