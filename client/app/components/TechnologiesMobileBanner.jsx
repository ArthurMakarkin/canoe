import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';
import { Link } from 'react-router';

const $ = window.jQuery;

export default class TechnologiesMobileBanner extends Component {
  static propTypes = {
    technologies: PropTypes.array,
  }

  onRefresh(inst) {
    const scroller = $(inst.scroller);
    scroller.width(scroller.find(".button").length * 130);
  }

  getBlock(tech, index){
    // if(tech.DetailsURL != ''){
    //   return (
    //     <Link key={'link-' + index} className="button" to={tech.DetailsURL}>
    //       <img className="icon" src={tech.Icon.Url} role="presentation" />
    //       <p>{tech.Name.ru.split(' ').map((word, index) => (
    //         <span key={index} className={word == '-' ? 'minus' : ''}>{word == '-' ? '–' : word}</span>
    //       ))}</p>
    //     </Link>
    //   )
    // } else {
      return (
        <span key={'link-' + index} className="button">
          <img className="icon" src={tech.Icon.Url} role="presentation" />
          <p>{tech.Name.ru.split(' ').map((word, index) => (
            <span key={index} className={word == '-' ? 'minus' : ''}>{word == '-' ? '–' : word}</span>
          ))}</p>
        </span>
      )
    // }
  }

  render() {
    const { technologies } = this.props;
    // const techs = technologies.filter((tech) => tech.DetailsURL != "");

    const options = {
      scrollX: true,
      scrollY: false,
      // snap: '.item',
      // probeType: 3,
      // bounce: false,
      // bounceTime: 0,
      // deceleration: 0,
      // momentum: false,
      // mouseWheel: true,
      click: true,
      preventDefault: false,
      eventPassthrough: true,
    }

    return (
      <div className="banner-with-technologies full-width" id="technologies-banner">
        <h2 className="heading">Использованные<br />материалы и технологии</h2>
        <ReactIScroll
          className="b-iscroll"
          onRefresh={(inst) => this.onRefresh(inst)}
          options={options}
          iScroll={iScroll}>

          <div className="buttons b-iscroll-container">
            {technologies.map((tech, index) =>
              this.getBlock(tech, index)
            )}
          </div>
        </ReactIScroll>
      </div>
    );
  }
}
