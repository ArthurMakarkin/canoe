import React, { Component } from 'react';
import { Link } from 'react-router';
import fetchJsonp from 'fetch-jsonp';

import FontAwesome from 'react-fontawesome';
import moment from 'moment';
import ru from 'moment/locale/ru';
moment.updateLocale('ru', ru);

export default class Instagram extends Component {
  state = {
    feed: [],
  }

  componentDidMount() {
    this.getPhotos();
  }

  getPhotos = () => {
    const URL = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=558996003.5dc505e.327739f7f7c34fe8805a7a5714cdfa3c&count=4';
    fetchJsonp(URL).then(response => response.json()).then((responseJson) => {
      this.setState({ feed: responseJson.data });
    });
  }

  render() {

    return (
      <div className="instagram">
        <div className="instagram__header">
          <div className="instagram__header__title">{this.props.hashtag}</div>
          <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
          <div className="instagram__header__description">
            Следуйте за нами на инстаграм</div>
        </div>
        <div className="instagram__feed">
          {this.state.feed ? this.state.feed.map((item, index) => {
            const elapsed = moment().diff(moment.unix(parseInt(item.created_time, 10)));
            const elapsedTimeHuman = moment.duration(elapsed).humanize();
            return (
              <div className="instagram__feed__item" key={index}>
                <a href={item.link} target="_blank" rel="noopener noreferrer" className="instagram__feed__item__photo" style={{ backgroundImage: `url(${item.images.standard_resolution.url})` }} />
                <div className="instagram__feed__item__caption">
                  <div className="instagram__feed__item__caption__timeago">
                    {elapsedTimeHuman} назад
                  </div>
                  <div className="instagram__feed__item__caption__name">
                    <FontAwesome name="heart" />
                    {item.likes.count}
                  </div>
                </div>
              </div>
            );
          }) : null}
        </div>
      </div>
    );
  }
}
