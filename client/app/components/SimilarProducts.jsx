import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import MediaQuery from 'react-responsive';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';

import api from 'app/actions/api';
import * as actions from 'app/actions';
import _ from 'lodash';

const $ = window.jQuery;

class SimilarProducts extends Component {
  static defaultProps = {
    limit: 4,
    title: 'В комплекте с этим товаром покупают',
  }

  static propTypes = {
    list: PropTypes.array,
    filters: PropTypes.array,
    limit: PropTypes.number,
    title: PropTypes.string,
  }

  state = {
    products: [],
  }

  next(){
    this.instance.next();
  }

  prev(){
    this.instance.prev();
  }

  // onRefresh(inst) {
  //   this.instance = inst;
  //   $(".similar-list .b-iscroll-container").width($(".similar-list .b-iscroll-container .product-tile").length * 96);
  // }

  componentDidMount() {
    const { filters, user, limit } = this.props;

    if (filters) {
      api('products', {
        ContractUUID: user.ContractUUID,
        filter: filters,
        limit,
      }).then((products) => {
        this.setState({ products });
      });
    }
  }

  onRefresh(inst) {
    const scroller = $(inst.scroller);
    this.instance = inst;
    const items = scroller.find(".product-tile");
    scroller.width(items.length * (items.eq(0).width() + parseInt(items.eq(0).css("padding-left")) + parseInt(items.eq(0).css("padding-right"))));
  }


  render() {
    const { limit, title } = this.props;
    const products = this.props.list || this.state.products || [];
    const filteredProducts = _.uniqBy(products, p => p.Id).filter(product => !_.isEqual(this.props.product, product));


    if (filteredProducts.length > 0) {
      const options = {
        scrollX: true,
        scrollY: false,
        snap: '.product-tile',
        // probeType: 3,
        // bounce: false,
        // bounceTime: 0,
        // deceleration: 0,
        // momentum: false,
        // mouseWheel: true,
        click: true,
        preventDefault: false,
        eventPassthrough: true,
      }

      const similarList = <div className="similar-list">
                      {filteredProducts.slice(0, products.length).map((product, index) =>
                        <div className="product-tile" key={index}>
                          <Link
                            className="photo"
                            to={`/catalogue/product/${product.URLName}`}
                          >
                            <img src={product.Img.ThumbUrl} alt="" />
                          </Link>
                          <div className="info">
                            <div className="name">{product.Name}</div>
                            {' '}
                            <div className="price">{product.Price} <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
      return (
        <div className="similar-products">
          <div className="similar-heading">
            <Link className="back-link" to="/catalogue">

              <MediaQuery query='(max-width: 768px)'>
                <span className="text">Каталог товаров</span>
                <svg className="arrow arrow-order-mobile">
                  <use xlinkHref="/assets/images/sprite.svg#arrow-order-mobile" />
                </svg>
              </MediaQuery>
              <MediaQuery query='(min-width: 769px)'>
                <svg className="arrow svg-arrow-left">
                  <use xlinkHref="/assets/images/sprite.svg#arrow-left" />
                </svg>
                <span className="text">Каталог</span>
              </MediaQuery>
            </Link>
            {' '}
            {products.length ? (
              <div className="title">{title}</div>
            ) : null}
            {' '}
            <div className="search">
              {/*
               <svg className="icon svg-icon-search"><use xlinkHref="/assets/images/sprite.svg#icon-search" /></svg>
               <input className="input" type="text" placeholder="Поиск" />
               */}
            </div>
          </div>
          <div className="products-slider-container">
            {products.length >= 5 ? (
              <button type="button" onClick={() => this.prev()} className="slick-arrow slick-prev"><svg className="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" strokeMiterlimit="10"><path d="M7.707.354l-7 7 7 7M.707 7.354h18" vectorEffect="non-scaling-stroke" /></g></svg></button>
            ) : null}
            {products.length >= 5 ? (
              <button type="button" onClick={() => this.next()} className="slick-arrow slick-next"><svg className="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" strokeMiterlimit="10"><path d="M11 14.354l7-7-7-7M18 7.354H0" vectorEffect="non-scaling-stroke" /></g></svg></button>
            ) : null}
            <ReactIScroll
              className="b-iscroll"
              onRefresh={(inst) => this.onRefresh(inst)}
              options={options}
              iScroll={iScroll}>
              <div className="products b-iscroll-container">
                {similarList}
              </div>
            </ReactIScroll>
          </div>
        </div>
      );
    }
    return null;
  }
}

function mapStateToProps(state) {
  const { user, product } = state;
  return {
    user,
    product,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SimilarProducts);
