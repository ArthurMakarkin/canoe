import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll-probe';
import getImageSize from 'app/mixins/imageSizes';

const $ = window.jQuery;


export class MobileGallery extends Component {
  static propTypes = {
    content: PropTypes.array,
    name: PropTypes.string,
    noText: PropTypes.bool,
    textHeight: PropTypes.number,
    showPage: PropTypes.number,
    colors: PropTypes.bool,
  }

  iscrollInstance = null;
  slides = null;
  texts = null;
  currentPageIndex = 0;
  currentPage = undefined;
  nextPage = undefined;
  startX = undefined;
  pages = {};
  itemWidth = undefined;
  direction = 0;
  bg = undefined


  _handleScroll(evt){
    let nextTop = 0;
    let curTop = 0;
    let nextTextStyles = {};
    let curTextStyles = {};

    if(this.iscrollInstance.directionX == 0){
      this.iscrollInstance.directionX = this.direction;
    }

    const half = Math.abs((this.iscrollInstance.x - this.startX)/this.itemWidth) >= 0.5;


    if(this.iscrollInstance.directionX == 1){
      this.direction = 1;
      nextTop = 24 - (this.iscrollInstance.x - this.startX)/this.itemWidth * -24;
      curTop = (this.iscrollInstance.x - this.startX)/this.itemWidth * -24;

      curTextStyles = {
        opacity: half ? 0 : 1 - 2 * Math.abs((this.iscrollInstance.x - this.startX)/this.itemWidth),
        left: -(this.iscrollInstance.x - this.startX)/this.itemWidth * -50 + '%',
      }
      nextTextStyles = {
        opacity: half ? Math.abs((2*this.iscrollInstance.x - 2*this.startX)/(2*this.itemWidth)) : 0,
        left: 50 - (this.iscrollInstance.x - this.startX)/this.itemWidth * -50 + '%',
      }
    } else if(this.iscrollInstance.directionX == -1){
      this.direction = -1;
      nextTop = 24 - (- this.iscrollInstance.x + this.startX)/this.itemWidth * -24;
      curTop = (- this.iscrollInstance.x + this.startX)/this.itemWidth * -24;

      curTextStyles = {
        opacity: half ? 0 : 1 - 2 * Math.abs((this.iscrollInstance.x - this.startX)/this.itemWidth),
        left: - (this.iscrollInstance.x - this.startX)/this.itemWidth * -50 + '%',
      }
      nextTextStyles = {
        opacity: half ? Math.abs((2*this.iscrollInstance.x - 2*this.startX)/(2*this.itemWidth)) : 0,
        left: - 50 + (this.iscrollInstance.x - this.startX)/this.itemWidth * 50 + '%',
      }
    }

    if(this.nextPage.block){
      this.nextPage.block.css({
        paddingTop: nextTop
      });

      if(this.nextText){
        this.nextText.css(nextTextStyles);
      }
    }

    this.currentPage.block.css({
      paddingTop: curTop
    });

    if(this.currentText){
      this.currentText.css(curTextStyles);
    }

  }

  prepareScroll(evt) {
    this.currentPageIndex = evt.currentPage.pageX;
    this.currentPage = {
      block: this.slides.eq(this.currentPageIndex),
      x: evt.currentPage.x
    }

    this.currentText = this.texts.eq(this.currentPageIndex);

    this.itemWidth = this.pages[0][0].width;

    this.nextPage = false;
    if(evt.directionX == 1){
      this.nextPage = {
        block: (this.currentPage.block.next('.item').length) ? this.currentPage.block.next('.item') : false,
        x: this.pages[this.currentPageIndex + 1] ? this.pages[this.currentPageIndex + 1][0].x : false
      }

      this.nextText = (this.currentText.next('.item-text').length) ? this.currentText.next('.item-text') : false;
    } else if(evt.directionX == -1){
      this.nextPage = {
        block: (this.currentPage.block.prev('.item').length) ? this.currentPage.block.prev('.item') : false,
        x: this.pages[this.currentPageIndex - 1] ? this.pages[this.currentPageIndex - 1][0].x : false
      }

      this.nextText = (this.currentText.prev('.item-text').length) ? this.currentText.prev('.item-text') : false;
    }
    this.startX = this.iscrollInstance.x;
  }

  onRefresh(inst) {
    this.iscrollInstance = inst;
    this.slides = $(`#iscroll-${this.props.name} .item`);
    this.texts = $(`#iscroll-${this.props.name} .item-text`);
    this.container = $(`#iscroll-${this.props.name} .iscroll-container`);
    this.pages = inst.pages;
    this.currentPageIndex = this.iscrollInstance.currentPage.pageX;
    this.container.css({
      width: 'calc(' + this.slides.length * 100 + 'vw - ' + 71 * this.slides.length +'px )'
    });
    this.slides.eq(this.currentPageIndex).addClass('active');
    this.texts.eq(this.currentPageIndex).addClass('active');
    this.bg = this.props.colors ? $(`#iscroll-${this.props.name} .bg`) : false;
    if(this.bg){
      this.bg.css({
        backgroundColor: this.props.content[this.currentPageIndex].bg
      })
    }


  }

  prepareText(html) {
    switch(this.props.name){
      case 'technologies-main':
        html = html.replace('Технология ', '');
        break;
    }
    return html;
  }

  scrollEnd(evt) {
    if(this.bg){
      this.bg.css({backgroundColor: this.props.content[evt.currentPage.pageX].bg});
    }

  }

  render() {
    const { name, noText, textHeight, colors, showPage } = this.props;
    const { pagewidth } = this.props.app;

    let content = this.props.content.slice();

    if(this.props.name == 'collections'){
      content.shift();
    }

    const options = {
      scrollX: true,
      scrollY: false,
      snap: true,
      probeType: 3,
      bounce: false,
      bounceTime: 0,
      deceleration: 0,
      momentum: false,
      click: true,
      preventDefault: false,
      eventPassthrough: true,
    }

    let styles = {};

    if(textHeight){
      styles = {
        height: textHeight
      }
    }

    return (
        <div className={`mobile-gallery mobile-gallery-${name}`} id={`iscroll-${name}`} style={{ backgroundColor: content[0].BackgroundColor}}>
          { name == 'collections' &&
            <div className="text-with-decorations" style={{ backgroundColor: content[0].BackgroundColor}}>
              <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              <div className="text" dangerouslySetInnerHTML={{ __html: this.props.content[0].Title }} />
              <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
            </div>
          }
          <ReactIScroll
            className="iscroll-wrapper b-iscroll"
            iScroll={iScroll}
            options={options}
            onRefresh={(inst) => this.onRefresh(inst)}
            onScroll={(evt) => this._handleScroll(evt)}
            onScrollStart={(evt) => this.prepareScroll(evt)}
            onScrollEnd={(evt) => this.scrollEnd(evt)}
            style={{ backgroundColor: content[0].BackgroundColor}}
          >
            <div className="iscroll-container b-iscroll-container">
              {content.map((item, index) =>
                <div className="item" key={index}>
                  <img src={getImageSize(pagewidth, item.Image.Url, 'square')} alt="" />
                </div>
              )}
            </div>
          </ReactIScroll>
          { !noText &&
            <div>
              <div className={`gallery-texts gallery-texts-${name}`} style={ {styles,backgroundColor: content[0].BackgroundColor} }>
                {content.map((item, index) =>
                  <Link key={index} to={item.URL} className="item-text">
                    <div className="title" dangerouslySetInnerHTML={{ __html: this.prepareText(item.Title) }} />
                    <div className="text" dangerouslySetInnerHTML={{ __html: item.Text }} />
                  </Link>
                )}
              </div>
              { colors ? <i className="bg" /> : null }
            </div>
          }
        </div>
    )
  }

}


function mapStateToProps(state) {
  const { app } = state;
  return {
    app,
  };
}

export default connect(mapStateToProps)(MobileGallery);
