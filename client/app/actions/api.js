import fetch from 'isomorphic-fetch';

const API = 'https://service.canoe.ru/';

export default function api(method, body = {}) {
  return new Promise((resolve, reject) => {
    fetch(`${API + method}`, {
      method: 'post',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({ ...body, ...{ form: method } }),
    }).then((response) => {
      response.json().then((json) => {
        const { form } = json;

        if (json.return && json.error) {
          const data = json.return;
          const error = { ...json.error, form };
          return resolve({ ...data, error }); // mixed response
        }

        if (json.return) {
          return resolve(json.return); // data
        }

        const { error } = json;

        if (error && error.name === 'WEB_SERVICE_ERROR') {
          const message = error.message.ru || `Неизвестная ошибка веб-сервиса при обращении к методу ${method}`;
          alert(message); // eslint-disable-line
          console.warn(error); // eslint-disable-line
        }

        return reject({ ...error, form }); // error response
      });
    });
  });
}
