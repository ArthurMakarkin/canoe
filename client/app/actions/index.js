import fetch from 'isomorphic-fetch';

import * as types from 'app/constants/Actions';
import api from './api';

const API = 'https://service.canoe.ru';

export function fetchMain() {
  return (dispatch) => {
    fetch(`${API}/page/main_page/`, {
      mode: 'cors',
      credentials: 'include',
    }).then(response => response.json())
      .then((data) => {
        dispatch({
          type: types.RECEIVE_MAIN,
          data: data.return,
        });
      });
  };
}

export function hideFooter() {
  return {
    type: types.HIDE_FOOTER,
    footer: false
  };
}

export function showFooter() {
  return {
    type: types.SHOW_FOOTER,
    footer: true
  };
}

export function receiveMain(main) {
  return {
    type: types.RECEIVE_MAIN,
    main,
  };
}

export function receiveError(error) {
  return {
    type: types.RECEIVE_ERROR,
    error,
  };
}

export function updateCatalogueView(view) {
  return {
    type: types.UPDATE_CATALOGUE_VIEW,
    view,
  };
}

export function receiveUpdatedSorting(sort) {
  return (dispatch) => {
    dispatch({
      type: types.UPDATE_CATALOGUE_SORTING,
      sort,
    });

    return Promise.resolve();
  };
}

export function updateCatalogueSorting(sort) {
  return dispatch => dispatch(
    receiveUpdatedSorting(sort)
  ).then(() => {
    dispatch(fetchProducts());
  });
}

export function receiveBasket(basket) {
  return (dispatch) => {
    if (basket.error) {
      dispatch(receiveError(basket.error));
    } else {
      dispatch(receiveError({}));
    }

    return dispatch({
      type: types.RECEIVE_BASKET,
      basket,
    });
  };
}

export function receiveProducts(products) {
  return {
    type: types.RECEIVE_PRODUCTS,
    products,
  };
}

export function receiveProduct(type, product) {
  const img = new Image();
  img.src = product.DetailPictures[0].ThumbUrl;

  return {
    type,
    product,
  };
}

export function clearPrefetchedProducts() {
  return {
    type: types.CLEAR_PREFETCHED_PRODUCTS,
  };
}

export function receiveUpdatedFilters(filters) {
  return (dispatch) => {
    dispatch({
      type: types.UPDATE_FILTERS,
      filters,
    });

    return Promise.resolve();
  };
}

export function updateFilters(filters) {
  return dispatch => dispatch(
    receiveUpdatedFilters(filters)
  ).then(() => {
    dispatch(fetchProducts());
  });
}

export function toggleFilter(value) {
  return (dispatch, getState) => {
    const filters = getState().app.activeFilters;
    const index = filters.indexOf(value);
    if (~index) {
      filters.splice(index, 1);
    } else {
      filters.push(value);
    }
    dispatch(updateFilters(filters));
  };
}

export function setPageWidth(value) {
  return {
    type: types.SET_PAGE_WIDTH,
    pagewidth: value,
  };
}

export function setWholesaleMode(value) {
  return {
    type: types.SET_WHOLESALE_MODE,
    wholesaleMode: value,
  };
}

export function receiveUser(user) {
  return (dispatch) => {
    dispatch({
      type: types.RECEIVE_USER,
      user,
    });

    return Promise.resolve();
  };
}

export function onLogin(user) {
  return (dispatch, getState) => {
    dispatch(setWholesaleMode(user.AccountTYPE === 'wholesale'));
    dispatch(receiveUser(user)).then(() => {
      dispatch(clearPrefetchedProducts());
      dispatch(fetchProducts());
      dispatch(fetchBasket());

      const product = getState().product.current;
      if (product) dispatch(fetchProduct(product.URLName));
    });
  };
}

export function logOut() {
  return (dispatch) => {
    dispatch(setWholesaleMode(false));
    dispatch(onLogin({ AccountUUID: 0 }));
  };
}

export function receiveFilters(filters) {
  return {
    type: types.RECEIVE_FILTERS,
    filters,
  };
}

// Fetches
export function fetchFilters() {
  return (dispatch) => {
    api('filters').then((data) => {
      dispatch(receiveFilters(data));
    }).catch(error => error);
  };
}

export function fetchUser(user) {
  return (dispatch) => {
    api('user_info', user).then((data) => {
      dispatch(onLogin(data));
    }).catch((error) => {
      dispatch(receiveError(error));
    });
  };
}

export function registerRetail(user) {
  return (dispatch) => {
    api('reg_retail', user).then((data) => {
      dispatch(onLogin(data));
    }).catch((error) => {
      dispatch(receiveError(error));
    });
  };
}

export function registerWholesale(user) {
  return (dispatch) => {
    api('reg_wholesale', user).then((data) => {
      dispatch(onLogin(data));
    }).catch((error) => {
      dispatch(receiveError(error));
    });
  };
}

export function fetchProducts(searchString = false) {
  return (dispatch, getState) => {
    const state = getState();
    const { ContractUUID } = state.user;
    const filter = state.app.activeFilters;
    const sort = state.app.sort;

    let params = {
      ContractUUID,
    }

    if (searchString) {
      params.searchString = searchString;
    } else {
      params.filter = filter;
    }

    params.sort = sort;

    api('products', params).then((data) => {
      dispatch(receiveProducts(data));
    }).catch(error => error);
  };
}

export function fetchProduct(URLName, prefetch) {
  return (dispatch, getState) => {
    const state = getState();
    const { ContractUUID } = state.user;
    const prefetchedProducts = state.product.prefetched;
    const product = prefetchedProducts.find(p => p.URLName === URLName);

    if (product) {
      if (!prefetch) dispatch(receiveProduct(types.RECEIVE_PRODUCT, product));
      return;
    }

    const actionType = prefetch ? types.PREFETCH_PRODUCT : types.RECEIVE_PRODUCT;

    api('product', {
      ContractUUID,
      URLName,
    }).then((data) => {
      dispatch(receiveProduct(actionType, data));
    }).catch(error => error);
  };
}

export function fetchMaterial(id) {
  return (dispatch) => {
    fetch(`https://service.canoe.ru/page/materials/${id}`)
      .then(response => response.json())
      .then((material) => {
        dispatch({ type: types.RECEIVE_MATERIAL, material: material.return, id });
      });
  };
}

export function fetchMaterials() {
  return (dispatch, getState) => {
    if (getState().materials.length === 0) {
      fetch('https://service.canoe.ru/pages/materials')
        .then(response => response.json())
        .then((materials) => {
          dispatch({ type: types.RECEIVE_MATERIALS, materials: materials.return });
        });
    }
  };
}

export function fetchBasket() {
  return (dispatch, getState) => {
    const state = getState();
    const { AccountUUID, ContractUUID } = state.user;

    api('basket', {
      ContractUUID,
      AccountUUID,
    }).then((data) => {
      dispatch(receiveBasket(data));
    }).catch(error => error);
  };
}

export function addBasket(ProductUUID, Quantity) {
  return (dispatch, getState) => {
    const state = getState();
    const AccountUUID = state.user.AccountUUID;
    const ContractUUID = state.user.ContractUUID;

    api('add_basket', {
      ContractUUID,
      AccountUUID,
      add_basket: [{
        ProductUUID,
        Quantity,
      }],
    }).then((data) => {
      dispatch(receiveBasket(data));
    }).catch(error => error);
  };
}

export function deleteBasket(ProductUUID, Quantity) {
  return (dispatch, getState) => {
    const state = getState();
    const AccountUUID = state.user.AccountUUID;
    const ContractUUID = state.user.ContractUUID;

    api('del_basket', {
      ContractUUID,
      AccountUUID,
      del_basket: [{
        ProductUUID,
        Quantity,
      }],
    }).then((data) => {
      dispatch(receiveBasket(data));
    }).catch(error => error);
  };
}

export function updateBasket(products, Coupon) {
  return (dispatch, getState) => {
    if (!products.length) return;

    const state = getState();
    const AccountUUID = state.user.AccountUUID;
    const ContractUUID = state.user.ContractUUID;

    api('update_basket', {
      ContractUUID,
      AccountUUID,
      Coupon,
      update_basket: products,
    }).then((data) => {
      dispatch(receiveBasket(data));
    }).catch(error => error);
  };
}
