 export const DEFAULT = ['Season_1c421837-fdea-11e1-99a4-00259034376e'];
//export const DEFAULT = ['Season_1c421836-fdea-11e1-99a4-00259034376e'];


export const MAIN_PAGE = ['Segment_7e5e529a-7772-11e6-ef83-02000a452867'];

export const ALL = ['Gender_male', 'Gender_female', 'Gender_boys', 'Gender_girls'];
export const MEN = ['Gender_male'];
export const WOMEN = ['Gender_female'];
export const BOYS = ['Gender_boys'];
export const GIRLS = ['Gender_girls'];
export const CHILDREN = ['Gender_boys', 'Gender_girls'];

export const H2DRY = ['Technology_00b372d4-7476-11e6-078f-02000a452867'];
export const TEFLON = ['Technology_c0afaa22-7475-11e6-078f-02000a452867'];
export const THREE_D_TUBE = ['Technology_0807b414-7476-11e6-078f-02000a452867'];
export const HIGH_TWIST = ['Technology_9e6e5d78-7475-11e6-078f-02000a452867'];
export const NANO_SILVER = ['Technology_3abea036-840e-11e7-989d-02000a452867'];

export const PREMIUM = ['Style_131b445e-548f-11e5-8934-080027002424'];
export const NEO_CLASSIC = ['Style_6e5f3ab6-ac35-11e0-b178-00259034376f'];
export const STREET_FASHION = ['Style_6e5f3ab7-ac35-11e0-b178-00259034376f'];
export const TEENS = ['Style_6e5f3ab8-ac35-11e0-b178-00259034376f'];
export const WINTER = ['Season_1c421837-fdea-11e1-99a4-00259034376e'];
export const SUMMER = ['Season_1c421836-fdea-11e1-99a4-00259034376e'];
