export default JSON.parse(`{
        "PageContent": [
            {
                "block_name": "Materials_banner",
                "contents": [
                    {
                        "Title": "альпака",
                        "Text": "Самая ценная шерсть c комфортным температурным режимом",
                        "Image": {
                            "Url": "https:\/\/service.canoe.ru\/image\/20161008%2Falpaka_banner.jpg",
                            "ThumbUrl": ""
                        },
                        "URL": ""
                    }
                ]
            },
            {
                "block_name": "Materials_header",
                "contents": [
                    {
                        "Title": "",
                        "Text": "Альпака является одним из видов южноамериканских верблюдов, обитающих на высокогорных плато на высоте 4000-4700 м.",
                        "Image": {
                            "Url": "",
                            "ThumbUrl": ""
                        },
                        "URL": ""
                    }
                ]
            },
            {
                "block_name": "Materials_picture2cols",
                "contents": [
                    {
                        "Title": "Согревает от холода",
                        "Text": "Шерсть альпаки приспособлена к экстремальному климату и резким перепадам температур - она легкая, тонкая и настолько плотная, что не пропускает воду. ",
                        "Image": {
                            "Url": "https:\/\/service.canoe.ru\/image\/20161008%2FAURA-3446848.jpg",
                            "ThumbUrl": "https:\/\/service.canoe.ru\/image_thumb\/520\/520\/20161008%2FAURA-3446848.jpg"
                        },
                        "URL": "\/catalogue\/product\/snood_aura_white_3449930"
                    },
                    {
                        "Title": "Терморегуляция",
                        "Text": "Шерсть альпаки приспособлена к экстремальному климату и резким перепадам температур - она легкая, тонкая и настолько плотная, что не пропускает воду. ",
                        "Image": {
                            "Url": "https:\/\/service.canoe.ru\/image\/20161008%2Fmilan_3440637.jpg",
                            "ThumbUrl": "https:\/\/service.canoe.ru\/image_thumb\/520\/520\/20161008%2Fmilan_3440637.jpg"
                        },
                        "URL": "\/catalogue\/product\/beanie_milan_yellow_3440633"
                    }
                ]
            },
            {
                "block_name": "Materials_shop_banner",
                "contents": [
                    {
                        "Title": "",
                        "Text": "",
                        "Image": {
                            "Url": "https:\/\/service.canoe.ru\/image\/20161008%2Falpaka_shop_banner.jpg",
                            "ThumbUrl": ""
                        },
                        "URL": "\/catalogue\/"
                    }
                ]
            },
            {
                "block_name": "Materials_properties",
                "contents": [
                    {
                        "Title": "Какими свойствами обладает головной убор",
                        "Text": "Головной убор из альпаки имеет уникальные свойства регулировать температуру тела — в шапке комфортно в холод и не жарко в жару. Головной убор очень легкий и при этом чрезвычайно прочный, не растягивается и не дает усадки.",
                        "Image": {
                            "Url": "https:\/\/service.canoe.ru\/image\/20160907%2Falpaka_square.jpg",
                            "ThumbUrl": ""
                        },
                        "URL": ""
                    }
                ]
            },
            {
                "block_name": "Materials_end",
                "contents": [
                    {
                        "Title": "Какую пряжу мы используем",
                        "Text": "Мы работаем с изысканной смесовой пряжей из альпаки, шерсти и акрила, которая придает великолепные ощущения комфорта, радости и удовлетворения.",
                        "Image": {
                            "Url": "https:\/\/service.canoe.ru\/image\/20161008%2Falpaka_banner.jpg",
                            "ThumbUrl": ""
                        },
                        "URL": ""
                    }
                ]
            },
            {
                "block_name": "Materials_catalogue",
                "contents": [
                    {
                        "Title": "Шапки из шерсти альпаки",
                        "Text": "",
                        "Image": {
                            "Url": "",
                            "ThumbUrl": ""
                        },
                        "URL": "",
                        "CatalogFilterValue": "Materials_2e4cd9bc-7474-11e6-078f-02000a452867"
                    }
                ]
            }
        ]
    }`);
