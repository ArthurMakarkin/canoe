import React, { Component } from 'react';

import classNames from 'classnames';

import MediaQuery from 'react-responsive';

import api from 'app/actions/api';

export default class Contacts extends Component {
  state = {
    retailSelected: true
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const form = event.target;
    const button = form.elements.button;

    button.setAttribute('disabled', 'disabled');

    api('feedback', {
      fullname: form.elements.fullname.value,
      email: form.elements.email.value,
      subject: form.elements.customer_type.value,
      message: form.elements.message.value,
    }).then(() => {
      button.textContent = 'Отправлено';
      button.classList.add('success');
    }).catch((error) => {
      console.log(error); // eslint-disable-line
      button.removeAttribute('disabled');
    });
  }

  toggleContacts(isRetail) {
    let state = this.state;
    state.retailSelected = isRetail;
    this.setState(state);

  }

  render() {
    return (
      <section className="page-content contacts-page">
        <div className="content-wrapper">
          <div className="banner-with-centered-content full-width header-banner" style={{ backgroundImage: 'url(//media.canoe.ru/images/patterns/pattern-01.jpg)' }}>
            <MediaQuery query='(max-width: 768px)'>
              <div className="content">
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                <div className="text with-br">Прокладывая курс<br />в неизвестные воды</div>
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              </div>
            </MediaQuery>
            <MediaQuery query='(min-width: 769px)'>
              <div className="content">
                <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
                <div className="text">Прокладывая курс в неизвестные воды</div>
                <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              </div>
            </MediaQuery>
          </div>

          <form className="form full-width" onSubmit={this.handleSubmit}>
            <div className="content-wrapper">
              <h2>Мы рады ответить на ваши вопросы или получить ваши отзывы.<br />Пожалуйста, заполните контактную форму.</h2>
              <div className="inputs two-columns-inputs">
                <div className="column">
                  <div className="input-group">
                    <label className="label" htmlFor="#">Имя</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" defaultValue={''} name="fullname" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                  <div className="input-group">
                    <label className="label" htmlFor="#">Электронная почта</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" defaultValue={''} name="email" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                  <div className="input-group">
                    <label className="label" htmlFor="#">Покупатель</label>
                    <div className="input-wrapper">
                      <div className="select-wrapper">
                        <select name="customer_type" className="select">
                          <option>Опт</option>
                          <option>Розница</option>
                        </select>
                      </div>
                      {/*<input className="text-input" type="text" defaultValue={''} name="subject" />*/}
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                </div>
                <div className="column">
                  <div className="input-group">
                    <label className="label" htmlFor="#">Сообщение</label>
                    <div className="input-wrapper">
                      <textarea className="textarea" type="text" defaultValue={''} name="message" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="buttons">
                <button className="round-button" name="button" type="submit">Отправить</button>
              </div>
            </div>
          </form>

          <div className="support">
            <div className="title">Служба поддержки</div>
            <div className="decorations">
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            </div>
            <div className="text">Мы делаем все, чтобы Вам было легко и удобно <br />оформить заказ на сайте. Вы можете уточнить условия оплаты и доставки<br />у наших консультантов по телефону.</div>

            <MediaQuery query='(max-width: 768px)'>
              <div className="mobile-contacts-switcher">
{/*                <div className="btns">
                  <div onClick={() => this.toggleContacts(true)} className={classNames("btn", {
                    "active": this.state.retailSelected
                  })}>Розница</div>
                  <div onClick={() => this.toggleContacts(false)} className={classNames("btn", {
                    "active": !this.state.retailSelected
                  })}>Опт</div>
                </div>
*/}
                <div className="info active">
                  <p className="phone"><a href="tel:+74952150701">+7 (495) 215-07-01</a></p>
                  <p>по будням с 10:00 до 18:00<br /><em>(бесплатно по России)</em></p>
                </div>
              </div>
            </MediaQuery>
          </div>

          <div className="support-info full-width">
{/*            <div className="support-block retail">
              <div className="title">Для розничных<br />покупателей</div>
              <div className="divider" />
              <div className="phone">8 (800) 775-77-37</div>
              <div className="info">
                <em>(бесплатно по России)</em><br />по будням 9:00 до 18:00
              </div>
            </div>
*/}
            <div className="support-block wholesale">
              <div className="title">Телефон для связи</div>

              <div className="divider" />
              <div className="phone">+7 (495) 215-07-01</div>
              <div className="info">
                <em>(бесплатно по России)</em><br />по будням с 10:00 до 18:00
              </div>
            </div>
          </div>

          <p className="mobile-address-title">Юридический и фактический адрес</p>

          <div className="map full-width">
            <iframe className="map-frame" src="https://www.google.com/maps/embed/v1/place?q=%D1%83%D0%BB.%20%D0%9C%D0%B0%D0%BB%D0%B0%D1%8F%20%D0%9A%D0%B0%D0%BB%D1%83%D0%B6%D1%81%D0%BA%D0%B0%D1%8F%2C%2015%2C%20%D0%BA%D0%BE%D1%80%D0%BF%D1%83%D1%81%2029&key=AIzaSyAUigdJZ2aAYJ0mHs4JYG-97OZA0dUSkCE" />
          </div>

          <div className="address">
            <div className="title">ООО «Каное»</div>
            <div className="subtitle">Юридический и фактический адрес</div>
            <div className="text">119071, г. Москва<br />ул. Малая Калужская, 15, корпус 29<br />тел./факс: +7 (495) 215-07-01<br />e-mail: <a href="mailto:c@noe.ru">c@noe.ru</a></div>
            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
          </div>
        </div>
      </section>
    );
  }
}
