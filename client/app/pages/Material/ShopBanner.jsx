import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';


const MaterialShopBanner = ({
  BackgroundColor,
  Image,
  URL,
}) => (
  <div className="b-shop-banner full-width" style={{ backgroundColor: BackgroundColor || '#D7CDD6' }}>
    <div className="content-wrapper">
      <div className="b-shop-banner__content" style={{ backgroundImage: `url(${Image.Url})` }}>
        <Link className="b-shop-banner__title" to={URL}>Коллекция</Link>
      </div>
    </div>
  </div>
);

MaterialShopBanner.propTypes = {
  BackgroundColor: PropTypes.string,
  Image: PropTypes.shape({
    Url: PropTypes.string,
    ThumbUr: PropTypes.string,
  }),
  URL: PropTypes.string,
};

export default MaterialShopBanner;
