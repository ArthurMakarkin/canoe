import React from 'react';
import PropTypes from 'prop-types';

const MaterialHeader = ({
  Text,
}) => (
  <div className="b-material-header full-width">
    <div className="text-with-decorations">
      <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
      <div className="subtext">{Text}</div>
    </div>
  </div>
);

MaterialHeader.propTypes = {
  Text: PropTypes.string,
};

export default MaterialHeader;
