import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';


const MaterialPicture2Cols = ({
  index,
  URL,
  Image,
  Title,
  Text,
}) => (
  <div className={`b-twocols__side b-twocols__side-${index + 1}`} key={index}>
    <Link className="b-twocols__side__image" to={URL} style={{ backgroundImage: `url(${Image.ThumbUrl})` }} />
    <div className="b-twocols__side__content">
      <div className="b-twocols__side__title">{Title}</div>
      <div className="b-twocols__side__text">{Text}</div>
    </div>
  </div>
);

MaterialPicture2Cols.propTypes = {
  Title: PropTypes.string,
  Text: PropTypes.string,
};

export default MaterialPicture2Cols;
