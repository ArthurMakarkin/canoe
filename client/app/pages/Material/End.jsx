import React from 'react';
import PropTypes from 'prop-types';


const MaterialEnd = ({
  Image,
  Title,
  Text,
}) => (
  <div className="b-end-banner full-width" style={{ backgroundImage: `url(${Image.Url})` }}>
    <div className="text-with-decorations b-end-banner__content">
      <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
      <div className="text">{Title}</div>
      <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
      <div className="subtext">{Text}</div>
    </div>
  </div>
);

MaterialEnd.propTypes = {
  Image: PropTypes.shape({
    Url: PropTypes.string,
    ThumbUr: PropTypes.string,
  }),
  Title: PropTypes.string,
  Text: PropTypes.string,
};

export default MaterialEnd;
