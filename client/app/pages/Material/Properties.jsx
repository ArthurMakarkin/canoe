import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';


const MaterialProperties = ({
  BackgroundColor,
  Image,
  Title,
  Text,
}) => (
  <div className="b-with-media full-width" style={{ backgroundColor: BackgroundColor || '#80717F' }}>
    <div className="content-wrapper">
      <div className="b-with-media__media" style={{ backgroundImage: `url(${Image.Url})` }} />
      <div className="b-with-media__content">
        <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
        <div className="b-with-media__content__title">{Title}</div>
        <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
        <div className="b-with-media__content__text">{Text}</div>
        <Link className="b-with-media__content__link" to="/catalogue">Перейти в магазин</Link>
      </div>
    </div>
  </div>
);

MaterialProperties.propTypes = {
  BackgroundColor: PropTypes.string,
  Image: PropTypes.shape({
    Url: PropTypes.string,
    ThumbUr: PropTypes.string,
  }),
  Title: PropTypes.string,
  Text: PropTypes.string,
};

export default MaterialProperties;
