import React from 'react';
import PropTypes from 'prop-types';

const MaterialBanner = ({
  Title,
  Text,
  Image,
}) => (
  <div className="b-material-header-banner banner-with-centered-content full-width" style={{ backgroundImage: `url(${Image.Url})` }}>
    <div className="content">
      <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
      <div className="title">{Title}</div>
      <div className="subtitle">{Text}</div>
    </div>
  </div>
);

MaterialBanner.propTypes = {
  Title: PropTypes.string,
  Text: PropTypes.string,
};

export default MaterialBanner;
