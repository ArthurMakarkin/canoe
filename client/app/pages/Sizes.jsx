import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

class Sizes extends Component {
  render() {
    return (
      <section className="page-content">
        <div className="sizes">
          <div
            className="sizes__header"
          >
            <div
              className="sizes__header__content-wrapper content-wrapper"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/sizes/sizes-header.svg)',
              }}
            >
              <div className="sizes__header__content">
                <div className="sizes__header__title">Посадка</div>
                <div className="sizes__header__list">
                  <div className="sizes__header__list__item">1. Шапка по голове</div>
                  <div className="sizes__header__list__item">2. Шапка средней длины</div>
                  <div className="sizes__header__list__item">3. Шапка удлиненная</div>
                  <div className="sizes__header__list__item">4. Снуд малый</div>
                  <div className="sizes__header__list__item">5. Снуд длинный</div>
                  <div className="sizes__header__list__item">6. Капюшон</div>
                </div>
              </div>
            </div>
          </div>
          <div className="sizes__content">
            <div className="sizes__content__title">Размеры головных уборов</div>
            <svg className="sizes__content__decoration decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
            <div className="sizes__content__text">
              Большинство наших головных уборов поставляются в самых популярных
              размерах: мужские 57—59 см, женские 55—57 см, детские 48—54 см.
              <br /><br />
              <span className="italic">
                Существуют универсальные модели, размер которых можно регулировать.<br />
                Трикотажные головные уборы имеют великолепную растяжимость.<br />
                Хлопковые головные уборы оснащены регулируемым замком.<br />
                В панамах и кепках без замков – обязательно вставлена резинка<br />
                для идеальной посадки на голове.
              </span>
            </div>
            <table className="sizes__content__table">
              <thead>
                <tr>
                  <th width={130}>Размер</th>
                  <th width={190}>Охват головы</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>XS</td>
                  <td className="size-metric">48—54</td>
                </tr>
                <tr>
                  <td>S</td>
                  <td className="size-metric">54—55</td>
                </tr>
                <tr>
                  <td>M</td>
                  <td className="size-metric">56—57</td>
                </tr>
                <tr>
                  <td>L</td>
                  <td className="size-metric">58—59</td>
                </tr>
              </tbody>
            </table>
            {
              this.props.product.current ? (
                <Link to={this.props.product.current.URL} className="sizes__back">
                  <img src="//media.canoe.ru/images/sizes/arrow.svg" alt="Вернуться к товару" />
                  <span>Вернуться в карточку товара</span>
                </Link>
              ) : null
            }
          </div>
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  const { product } = state;
  return {
    product,
  };
}

export default connect(mapStateToProps)(Sizes);
