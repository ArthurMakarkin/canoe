import React, { Component } from 'react';
import { Link } from 'react-router';

import classNames from 'classnames';
import MediaQuery from 'react-responsive';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';
import loadImage from 'blueimp-load-image';

import MobileDetect from 'mobile-detect';

import {ScrollToTopOnMount, SectionsContainer, Section} from 'react-fullpage';

const $ = window.jQuery;

export default class Contest extends Component {
  state = {
    step: 6,
    rulesSlide: 0,
    isConfirmed: false,
    initialAutoLoad: true,
    selectedFilter: 0,
    readyForDownload: false,
    isUIWebView: false,
    filters: [
      {
        preview: '/assets/images/contest/previews/1.jpg',
        filter: '/assets/images/contest/filters/1.png',
      },
      {
        preview: '/assets/images/contest/previews/2.jpg',
        filter: '/assets/images/contest/filters/2.png',
      },
      {
        preview: '/assets/images/contest/previews/3.jpg',
        filter: '/assets/images/contest/filters/3.png',
      },
      {
        preview: '/assets/images/contest/previews/4.jpg',
        filter: '/assets/images/contest/filters/4.png',
      },
      {
        preview: '/assets/images/contest/previews/5.jpg',
        filter: '/assets/images/contest/filters/5.png',
      },
      {
        preview: '/assets/images/contest/previews/6.jpg',
        filter: '/assets/images/contest/filters/6.png',
      },
      {
        preview: '/assets/images/contest/previews/7.jpg',
        filter: '/assets/images/contest/filters/7.png',
      },
      {
        preview: '/assets/images/contest/previews/8.jpg',
        filter: '/assets/images/contest/filters/8.png',
      },
      {
        preview: '/assets/images/contest/previews/9.jpg',
        filter: '/assets/images/contest/filters/9.png',
      },
      {
        preview: '/assets/images/contest/previews/10.jpg',
        filter: '/assets/images/contest/filters/10.png',
      },
      {
        preview: '/assets/images/contest/previews/11.jpg',
        filter: '/assets/images/contest/filters/11.png',
      },
      {
        preview: '/assets/images/contest/previews/12.jpg',
        filter: '/assets/images/contest/filters/12.png',
      },
      {
        preview: '/assets/images/contest/previews/13.jpg',
        filter: '/assets/images/contest/filters/13.png',
      },
      {
        preview: '/assets/images/contest/previews/14.jpg',
        filter: '/assets/images/contest/filters/14.png',
      },
    ]
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.step == 3 && this.state.initialAutoLoad){
      this.selectPhoto();
    }
  }

  componentWillMount(){
    window.location.hash = '';
    const md = new MobileDetect(window.navigator.userAgent);
    this.isMobile = md.mobile();
    var standalone = window.navigator.standalone,
    userAgent = window.navigator.userAgent.toLowerCase(),
    safari = /safari/.test( userAgent ),
    ios = /iphone|ipod|ipad/.test( userAgent );

    if(ios && !standalone && !safari){
      let state = this.state;
      state.isUIWebView = true;
      this.setState(state);
    }
  }

  closeWarning() {
    let state = this.state;
    state.isUIWebView = false;
    this.setState(state);
  }

  onRefreshFiltersList(inst) {
    const scroller = $(inst.scroller);
  }

  onRefreshFiltersListMobile(inst) {
    const scroller = $(inst.scroller);
    scroller.width(scroller.find(".filter").length * 90);
  }

  nextStep(step){
    let state = this.state;
    state.step = step ? step : state.step + 1;
    this.setState(state);
  }

  prevStep(step){
    let state = this.state;
    state.step = step ? step : state.step - 1;
    state.rulesSlide = 0;
    state.isConfirmed = false;
    state.initialAutoLoad = true;
    state.selectedFilter = 0;
    state.readyForDownload = false;
    this.setState(state);
  }

  toggleConfirmation(){
    let state = this.state;
    state.isConfirmed = !state.isConfirmed;
    this.setState(state);
  }

  renderIntro() {
    return (
      <div className="contest-intro">
        <p className="title">Получи шанс<br />выиграть стильный<br />головной убор от Canoe</p>
        <p className="main-button" onClick={() => this.nextStep()}>Создать арт</p>
      </div>
    );
  }

  renderRules() {
    const fullPageOptions = {
      arrowNavigation: true,
      delay: 500,
      navigation: false,
      scrollBar: false,
      anchors: ['slide1', 'slide2', 'slide3'],
      scrollCallback: (states) => this.setState({rulesSlide: states.activeSection})
    };

    const slide1 = (
      <div className="slide slide-first">
        <p className="title">Правила конкурса</p>
        <p className="slide-num">01</p>
        <div className="text">
          <p>загрузи фото, наложи фильтр, <br />поделись результатом на своей странице<br />укажи <strong>#canoe_artlovers</strong> в тексте поста</p>
        </div>
      </div>
    )

    const slide2 = (
      <div className="slide">
        <p className="title">Правила конкурса</p>
        <p className="slide-num">02</p>
        <div className="text">
          <p>Требования к фото:</p>
          <p>— на фото должен (-на) быть ты</p>
          <p>— фото не меньше 1000*1000</p>
          <p>— формат JPG/PNG</p>
          <p>— возрастной рейтинг 12+</p>
        </div>
      </div>
    )

    const slide3 = (
      <div className="slide">
        <p className="title">Правила конкурса</p>
        <p className="slide-num">03</p>
        <div className="text">
          <p>Наше жюри выберет 3 самые<br />интересные работы и подарит<br />стильные головные уборы</p>
          <p><a className="main-button" href="http://media.canoe.ru/downloads/contest-rules.pdf" target="_blank">полные правила конкурса</a></p>
        </div>
        <div className="confirm">
          <label onClick={() => this.toggleConfirmation()} className={classNames({'checked': this.state.isConfirmed})}>С правилами согласен</label><br />
          <div className={classNames("main-button", {'disabled': !this.state.isConfirmed})} onClick={this.state.isConfirmed ? (() => this.nextStep()) : null}>Загрузить фото</div>
        </div>
      </div>
    )

    return (
      <div>
        <ScrollToTopOnMount />
        <div className="navigation">
          <a href="#slide1" className={classNames({'active': this.state.rulesSlide == 0})} />
          <a href="#slide2" className={classNames({'active': this.state.rulesSlide == 1})} />
          <a href="#slide3" className={classNames({'active': this.state.rulesSlide == 2})} />
        </div>
        <SectionsContainer {...fullPageOptions}>
          <Section>{slide1}</Section>
          <Section>{slide2}</Section>
          <Section>{slide3}</Section>
        </SectionsContainer>
      </div>
    );
  }

  onSlideChangeStart(name, props, state, newState) {
    // return false;
  }

  onSlideChangeEnd(name, props, state, newState) {
    let oldState = this.state;
    oldState.rulesSlide = newState.activeSlide
    this.setState(oldState);
  }

  drawImageProp(ctx, img, x, y, w, h, isLarge) {
      if (arguments.length === 2) {
          x = y = 0;
          w = ctx.canvas.width;
          h = ctx.canvas.height;
      }

      let offsetX = 0.5;
      let offsetY = 0.5;

      if (offsetX < 0) offsetX = 0;
      if (offsetY < 0) offsetY = 0;
      if (offsetX > 1) offsetX = 1;
      if (offsetY > 1) offsetY = 1;

      var iw = img.width,
          ih = img.height,
          r = Math.min(w / iw, h / ih),
          nw = iw * r,   // new prop. width
          nh = ih * r,   // new prop. height
          cx, cy, cw, ch, ar = 1;


      if (nw < w) ar = w / nw;
      if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;  // updated
      nw *= ar;
      nh *= ar;

      cw = iw / (nw / w);
      ch = ih / (nh / h);

      cx = (iw - cw) * offsetX;
      cy = (ih - ch) * offsetY;

      if (cx < 0) cx = 0;
      if (cy < 0) cy = 0;
      if (cw > iw) cw = iw;
      if (ch > ih) ch = ih;

      ctx.drawImage(img, cx, cy, cw, ch,  x, y, w, h);

      let d;
      if(isLarge){
        this.largePixels = ctx.getImageData(0, 0, w, h);
        d = this.largePixels.data;
      } else {
        this.pixels = ctx.getImageData(0, 0, w, h);
        d = this.pixels.data;
      }

      for (let i = 0; i < d.length; i += 4) {
        let r = d[i];
        let g = d[i+1];
        let b = d[i+2];
        let v = 0.2126*r + 0.7152*g + 0.0722*b;
        d[i] = d[i+1] = d[i+2] = v
      }

      if(isLarge){
        this.setFilter(this.state.filters[0].filter, this.state.selectedFilter, true);
      }
  }

  selectPhoto() {
    const fileInput = document.getElementById('fileInput');
    this.canvas = document.getElementById('canvas');
    this.largeCanvas = document.getElementById('largeCanvas');
    let img;
    const canvasWidth = this.canvas.width;
    const canvasHeight = this.canvas.width;
    const largeCanvasWidth = 1000;
    const largeCanvasHeight = 1000;
    this.ctx = this.canvas.getContext('2d');
    this.largeCtx = this.largeCanvas.getContext('2d');

    const handleImage = (e) => {
      var that = this;
      var loadingImage = loadImage(
          e.target.files[0],
          function (img) {
            if(img.type === "error") {
              console.log("Error loading image ");
            } else {
              that.drawImageProp(that.ctx, img, 0, 0, canvasWidth, canvasHeight);
              that.drawImageProp(that.largeCtx, img, 0, 0, largeCanvasWidth, largeCanvasHeight, true);
            }
          },
          {
            orientation: true
          }
      );
    }

    fileInput.removeEventListener('change', handleImage);
    fileInput.addEventListener('change', handleImage, false);

    fileInput.click();
  }

  setFilter(url, index, isFirstImage){
    let img = new Image();
    img.onload = () => {
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.ctx.putImageData(this.pixels, 0, 0);
      this.ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
      this.largeCtx.clearRect(0, 0, this.largeCanvas.width, this.largeCanvas.height);
      this.largeCtx.putImageData(this.largePixels, 0, 0);
      this.largeCtx.drawImage(img, 0, 0, this.largeCanvas.width, this.largeCanvas.height);
      let state = this.state;
      state.selectedFilter = index;
      state.initialAutoLoad = false;
      state.readyForDownload = true;
      this.setState(state);
    }
    img.src = url;
  }

  downloadImage() {
    let link = document.getElementById('downloadLink')
    link.href = this.largeCanvas.toDataURL();
    link.download = 'canoe_contest_photo.png';
    if(this.isMobile){
      this.nextStep();
    } else {
      this.nextStep(5);
    }
  }

  renderMain() {
    const options = {
      scrollX: false,
      scrollY: true,
      mouseWheel: true,
      scrollbars: true,
      interactiveScrollbars: true,
      // click: true,
      // preventDefault: false,
      // eventPassthrough: true,
    }

    const mobileOptions = {
      scrollX: true,
      scrollY: false,
      mouseWheel: true,
      scrollbars: false,
      // interactiveScrollbars: true,
      click: true,
      preventDefault: false,
      eventPassthrough: true,
    }

    $(window).scrollTop(0);



    return (
      <div className="contest-main">
        <p className="title">Выбери арт-фильтр</p>
        <svg className="arrow svg-arrow-left" onClick={() => this.prevStep()}><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>

        <div className="container">
          <div className="photo">
            <MediaQuery query='(max-width: 768px)'>
              <canvas id="canvas" width="320" height="320" />
            </MediaQuery>
            <MediaQuery query='(min-width: 769px)'>
              <canvas id="canvas" width="410" height="410" />
            </MediaQuery>
            <canvas id="largeCanvas" width="1000" height="1000" />
          </div>
          <div className="filters-container">
            <MediaQuery query='(max-width: 768px)'>
              <ReactIScroll
                className="b-iscroll"
                onRefresh={(inst) => this.onRefreshFiltersListMobile(inst)}
                options={mobileOptions}
                iScroll={iScroll}>
                <div className="filters b-iscroll-container">
                  { this.state.filters.map((filter, index) =>
                    <div key={index} className={classNames("filter", {
                      'active': this.state.selectedFilter == index
                    })}>
                      <img onClick={() => this.setFilter(filter.filter, index)} src={filter.preview} />
                    </div>
                  ) }
                </div>
              </ReactIScroll>
            </MediaQuery>
            <MediaQuery query='(min-width: 769px)'>
              <ReactIScroll
                className="b-iscroll"
                onRefresh={(inst) => this.onRefreshFiltersList(inst)}
                options={options}
                iScroll={iScroll}>
                <div className="filters">
                  { this.state.filters.map((filter, index) =>
                    <div key={index} className={classNames("filter", {
                      'active': this.state.selectedFilter == index
                    })}>
                      <img onClick={() => this.setFilter(filter.filter, index)} src={filter.preview} />
                    </div>
                  ) }
                </div>
              </ReactIScroll>
            </MediaQuery>
          </div>
          <div className="buttons">
            <input id="fileInput" type="file" accept="image/*" className="hidden" />
            <div className="main-button" onClick={() => this.selectPhoto()}>Другое фото</div>
            <a href="#" target="_blank" id="downloadLink" className={classNames("main-button", {
              "disabled": !this.state.readyForDownload
            })} onClick={this.state.readyForDownload ? () => this.downloadImage() : null}>Скачать</a>
          </div>
        </div>

      </div>
    );
  }

  renderPreload() {
    $(window).scrollTop(0);

    window.location.hash = '';

    return (
      <div className="contest-success">
        <div className="text">
          <p>фотография загружается в соседнем окне / вкладке</p>
          <p>для сохранения нажмите на фото пока не откроется меню сохранения фото</p>
        </div>
        <div className="main-button" onClick={() => this.nextStep()}>Далее</div>
      </div>
    );
  }

  renderSuccess() {
    $(window).scrollTop(0);

    return (
      <div className="contest-success">
        <div className="text">
          <p><strong>Хочешь аксессуар от Canoe?</strong><br />Поделись своим арт-фото<br />с хэштегом <strong>#canoe_artlovers</strong><br />и жди результатов конкурса.</p>
          <p>Авторам 3-х самых интересных фото<br />Canoe подарит стильные головные уборы.</p>
        </div>
        <div className="sharing">
          <p className="share-title">Расскажи друзьям</p>
          <ul>
            <li>
              <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//canoe.ru/contest" target="_blank">
                <svg><use xlinkHref="/assets/images/sprite.svg#icon-fb" /></svg>
              </a>
            </li>
            <li>
              <a href="https://vk.com/share.php?url=http%3A//canoe.ru/contest" target="_blank">
                <svg><use xlinkHref="/assets/images/sprite.svg#icon-vk" /></svg>
              </a>
            </li>
          </ul>
        </div>
        <div className="main-button" onClick={() => this.prevStep(2)}>Создать заново</div>
      </div>
    );
  }

  renderEnd() {
    $(window).scrollTop(0);

    return (
      <div className="contest-intro">
        <p className="title">Конкурс завершен</p>
        <p className="text">Следите за нашими страницами в социальных сетях, скоро будут объявлены победители</p>
      </div>
    );
  }

  render() {

    let step;
    switch (this.state.step) {
      case 1:
        step = this.renderIntro();
        break;
      case 2:
        step = this.renderRules();
        break;
      case 3:
        step = this.renderMain();
        break;
      case 4:
        step = this.renderPreload();
        break;
      case 5:
        step = this.renderSuccess();
        break;
      case 6:
        step = this.renderEnd();
        break;
      default:
        break;
    }

    let styles = null;

    switch(this.state.step){
      case 1:
        styles = {
          backgroundImage: 'url("/assets/images/contest/bg.jpg")'
        }
        break;

      case 2:
        styles = {
          display: 'block'
        }
        break;

      case 6:
        styles = {
          backgroundImage: 'url("/assets/images/contest/bg.jpg")'
        }
        break;

      default:
        styles = null
        break;
    }

    return (
      <section className={classNames("page-content contest-page", {
        'filter-slide': this.state.step == 3,
        'success-slide': this.state.step == 5,
      })} style={styles}>
        <div className="content-wrapper">
          { this.state.isUIWebView && this.state.step == 1 &&
            <div className="contest-info-popup">
              <svg className="icon svg-icon-cross" onClick={() => this.closeWarning()}><use xlinkHref="/assets/images/sprite.svg#icon-cross-mobile" /></svg>
              <div className="in">
                <svg className="icon svg-icon-warning"><use xlinkHref="/assets/images/sprite.svg#icon-warning" /></svg>
                <p>Для стабильной работы<br />приложения, откройте<br />страницу canoe.ru<br />в обычном браузере</p>
              </div>

            </div>
          }
          {step}
        </div>
      </section>
    );
  }
}
