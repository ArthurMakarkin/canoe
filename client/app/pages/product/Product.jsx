import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import classNames from 'classnames';
import Scroll from 'react-scroll';
import _ from 'lodash';
import Slider from 'react-slick';

import Swipeable from 'react-swipeable'

import MediaQuery from 'react-responsive';

import Preloader from 'app/components/Preloader';
import SimilarProducts from 'app/components/SimilarProducts';
import TechnologiesBanner from 'app/components/TechnologiesBanner';
import TechnologiesMobileBanner from 'app/components/TechnologiesMobileBanner';

import NumberInput from 'app/components/NumberInput';

import OtherColors from 'app/pages/product/Blocks/OtherColors';
import WholesaleOrder from 'app/pages/product/Blocks/WholesaleOrder';
import WholesaleOrderMobile from 'app/pages/product/Blocks/WholesaleOrderMobile';

import * as actions from 'app/actions';

const $ = window.jQuery;

export class Product extends Component {
  state = {
    sliderPosition: 0,
    zoomedPhoto: '',
    zoomedIndex: 0,
    descriptionExpanded: false,
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log(this.state.sliderPosition)
  //   console.log(nextState.sliderPosition)
  //   // return !_.isEqual(this.props.product, nextProps.product) || this.state.sliderPosition != nextState.sliderPosition;
  //   return true
  // }

  addToBasket(id) {
    if (document.querySelector('.buy-button')) {
      document.querySelector('.buy-button').classList.add('clicked');
      document.querySelector('.buy-button .text').innerHTML = 'В корзине';
    }

    this.props.actions.addBasket(
      id,
      document.getElementById(`quantity-${id}`).value
    );
  }

  sliderSwitched(oldIndex, newIndex) {
    let state = this.state;
    state.sliderPosition = newIndex;
    this.setState(state);
  }

  zoomPhoto(url, index) {
    let state = this.state;
    state.zoomedIndex = index;
    state.zoomedPhoto = url;
    this.setState(state);
  }

  switchPhoto(dir) {
    let state = this.state;
    state.zoomedIndex = state.zoomedIndex + dir;
    if (state.zoomedIndex < 0) {
      state.zoomedIndex = this.props.product.current.DetailPictures.length - 1;
    } else if (state.zoomedIndex > this.props.product.current.DetailPictures.length - 1){
      state.zoomedIndex = 0;
    }
    state.zoomedPhoto = $('.slick-track .photo:not(.slick-cloned) img').eq(state.zoomedIndex).data('img');
    this.setState(state);
  }

  expand(){
    let state = this.state;
    state.descriptionExpanded = true;
    this.setState(state);
  }

  render() {
    const { app, basket, user } = this.props;
    const product = this.props.product.current;
    const { fetchProduct, deleteBasket } = this.props.actions;
    const isInBasket = basket.BasketList && product ? basket.BasketList.find(p => p.Product.Id === product.Id) : false;

    let technologies = [];
    if (product) {
      const { Materials, Features, Technologies, Temperature } = product;
      if (Materials.length || Features.length || Technologies.length || Temperature) {
        technologies = Materials.concat(Features, Technologies, Temperature).filter(t => t !== undefined);
      }

      const collapsedContent = $('.collapsed-content');
      if(collapsedContent.find('.text').height() <= 80){
        collapsedContent.addClass('expanded');
        collapsedContent.siblings('.expand-btn-container').addClass('hidden');
      }
    }


    const leftArrow = <button type="button" className="slick-arrow slick-prev"><svg className="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" strokeMiterlimit="10"><path d="M7.707.354l-7 7 7 7M.707 7.354h18" vectorEffect="non-scaling-stroke" /></g></svg></button>;

    const rightArrow = <button type="button" className="slick-arrow slick-next"><svg className="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" strokeMiterlimit="10"><path d="M11 14.354l7-7-7-7M18 7.354H0" vectorEffect="non-scaling-stroke" /></g></svg></button>;


    const sliderSettings = {
      dots: true,
      fade: true,
      prevArrow: leftArrow,
      nextArrow: rightArrow,
      swipe: true,
      draggable: false,
      touchMove: false,
      fade: false,
    };

    return (
      <section className={classNames("page-content", "product-page", {
        "zoomed-photo-visible": this.state.zoomedPhoto != ''
      })}>
        {product ? (
          <div className="content-wrapper">
            <Link className="back-link product-back-link" to="/catalogue">
              <svg className="arrow svg-arrow-left"><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>
              <span className="text">Каталог</span>
            </Link>
            <div className="product-card">
              <div className="photos-slider">
                <Slider beforeChange={(oldIndex, newIndex) => this.sliderSwitched(oldIndex, newIndex)} {...sliderSettings}>
                  {product.DetailPictures.map((photo, index) =>
                    <div className="photo" key={index}>
                      <img onClick={() => this.zoomPhoto(`${photo.Url}`, index)} src={`${photo.ThumbUrl}`} data-img={photo.Url} alt="" />
                    </div>
                  )}
                </Slider>
                <i className="slider-position" style={{
                  width: 100 / product.DetailPictures.length + '%',
                  left: this.state.sliderPosition * 100 / product.DetailPictures.length + '%',
                }} />
                {(product.Discount && product.Discount.Value > 0) ? <span className="discount-value">-{product.Discount.Value}%</span> : null}
              </div>

              <div className="product-info">
                <div className="product-heading">
                  <h1 className="name">{product.Name}</h1>
                  <h2 className="vendor-code"><span className="prefix">Арт.</span> {product.Article}</h2>
                </div>

                <div className="product-params">
                  <MediaQuery query='(max-width: 768px)'>
                    {/*Другие цвета мобилка*/}
                    {!app.wholesaleMode && product.OtherColors.length ? (
                      <OtherColors colors={product.OtherColors} leftArrow={leftArrow} rightArrow={rightArrow} />
                    ) : null}

                    {/*Цены мобилка*/}
                    {!app.wholesaleMode ? (
                      <div className="params-row">
                        <div className="param-label">Цена</div>
                        <div className="param-content">
                          <div className="price">
                            {(product.Discount && product.Discount.Value > 0) ? <span className="sum old-sum">{product.Discount.PriceOld}</span> : null}
                            <span className="sum">{product.Price}</span>
                            {' '}
                            <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                          </div>
                        </div>
                      </div>
                    ) : null}

                    {
                      /*user.AccountTYPE === 'retail' && product.OtherSizes.length ? (*/
                      !app.wholesaleMode ? (
                      <div className="params-row">
                        <div className="param-label">Размер</div>
                        <div className="param-content">
                          <div className="sizes">
                            <Link className="size-selector simple" to="/sizes">Таблица размеров</Link>
                            <span className="size-selector current">{product.Size}</span>
                            { product.OtherSizes.length ? product.OtherSizes.map((size, index) =>
                              <Link
                                className="size-selector"
                                to={`/catalogue/product/${size.URLName}`}
                                key={index}
                                onMouseEnter={() => fetchProduct(size.URLName, true)}
                              >
                                {size.Size}
                              </Link>
                            ) : null}
                          </div>
                        </div>
                      </div>
                    ) : null}

                    {!app.wholesaleMode ? (
                      <div className="params-row">
                        <div className="param-label" />
                        <div className="param-content controls">
                          <NumberInput id={`quantity-${product.Id}`} />
                          <button
                            className={classNames({
                              'main-button': true,
                              'buy-button': true,
                              clicked: isInBasket,
                            })}
                            onClick={isInBasket ? null : () => { this.addToBasket(product.Id); }}
                          >
                            <svg className="icon svg-icon-basket-add">
                              <use xlinkHref="/assets/images/sprite.svg#icon-basket-add" />
                            </svg>
                            <span className="text">{isInBasket ? 'В корзине' : 'Купить'}</span>
                          </button>
                        </div>
                      </div>
                    ) : null}
                  </MediaQuery>

                  <div className="params-row">
                    <div className="param-label">Состав</div>
                    <div className="param-content">
                      <p>
                        {product.Consist.map((component, index) =>
                          <span key={index}>
                            <span>{index === 0 ? '' : ', '}</span>
                            <span>{component.Procent}% {component.Material.Name.ru}</span>
                          </span>
                        )}
                      </p>
                      {technologies.length ? (
                        <Scroll.Link to="technologies-banner" smooth offset={-70} duration={1000}><span className="pseudo-link">Подробнее о материалах и технологиях</span></Scroll.Link>
                      ) : null}
                    </div>
                  </div>

                  <MediaQuery query='(max-width: 768px)' className="columns">
                    <div className="params-row">
                      <div className="param-label">Стиль</div>
                      <div className="param-content">{product.Type.Name.ru}</div>
                    </div>
                    <div className="params-row">
                      <div className="param-label">Пол</div>
                      <div className="param-content">{product.Gender.ru}</div>
                    </div>

                    <div className="params-row">
                      <div className="param-label">Цвет</div>
                      <div className="param-content">{product.Color.Name.ru}</div>
                    </div>
                  </MediaQuery>


                  <MediaQuery query='(max-width: 768px)' className="params-row">
                    <div className="param-label">Описание</div>
                    <div className="param-content">
                      <div className={classNames("collapsed-content", {
                        'expanded': this.state.descriptionExpanded
                      })}>
                        <p className="text">{product.Description.ru}</p>
                      </div>
                      { !this.state.descriptionExpanded &&
                        <div className="expand-btn-container">
                          <span onClick={() => this.expand()} className="expand-btn"></span>
                          <i className="gradient" />
                        </div>
                      }
                    </div>
                  </MediaQuery>

                  <MediaQuery query='(min-width: 769px)' className="params-row">
                    <div className="param-label">Описание</div>
                    <div className="param-content">
                      <div className="long-text">
                        <p className="text">{product.Description.ru}</p>
                        <span className="pseudo-link js-toggle-long-text">
                          <span className="more-link">Читать дальше</span>
                          <span className="less-link">Свернуть</span>
                        </span>
                      </div>
                    </div>
                  </MediaQuery>

                  {/* app.wholesaleMode ? (
                    <div className="params-row">
                      <div className="param-label">Розничная<br />цена</div>
                      <div className="param-content">
                        <div className="price retail-price">
                          <span className="sum"></span>
                          {' '}
                          <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                        </div>
                      </div>
                    </div>
                  ) : null */}

                  {app.wholesaleMode ? (
                    <div className="prices">
                      <div className="params-row">
                        <div className="param-label">Розница</div>
                        <div className="param-content">
                          <div className="price retail-price">
                            {(product.Discount && product.Discount.Value > 0) ? <span className="sum old-sum">{product.Discount.PriceOld}</span> : null}
                            <span className="sum">{product.PriceRetail}</span>
                            {' '}
                            <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                          </div>
                        </div>
                      </div>

                      <div className="params-row">
                        <div className="param-label">Оптовая цена</div>
                        <div className="param-content">
                          {user.AccountTYPE === 'wholesale' ? (
                            <div className="price wholesale-price">
                              <span className="sum">{product.Price}</span>
                              {' '}
                              <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                            </div>
                          ) : (
                            <div
                              className="price not-authorized"
                              style={{
                                fontSize: '11px',
                                fontWeight: 700,
                                textTransform: 'uppercase',
                                lineHeight: 1.875,
                              }}
                            >
                              Чтобы узнать цену, необходимо<br />войти в B2B портал
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  ) : null}

                  {/*Другие цвета и техническое описание десктоп*/}
                  <MediaQuery query='(min-width: 769px)'>
                    {!app.wholesaleMode && product.OtherColors.length ? (
                      <OtherColors labelBr={true} colors={product.OtherColors} leftArrow={leftArrow} rightArrow={rightArrow} />
                    ) : null}

                    <div className="params-row">
                      <div className="collapsed-block">
                        <div className={classNames({
                          'collapsed-title disabled': true,
                          'js-collapsed-toggler': false,
                          'active': true,
                        })}
                        >
                          {/*active: app.wholesaleMode,*/}

                          <span className="text">Техническое описание</span>
                          <svg className="arrow svg-arrow-down"><use xlinkHref="/assets/images/sprite.svg#arrow-down" /></svg>
                        </div>
                        <div className="collapsed-content">
                          <div className="params-row">
                            <div className="param-label">Стиль</div>
                            <div className="param-content">{product.Type.Name.ru}</div>
                          </div>
                          <div className="params-row">
                            <div className="param-label">Пол</div>
                            <div className="param-content">{product.Gender.ru}</div>
                          </div>

                          <div className="params-row">
                            <div className="param-label">Размер</div>
                            <div className="param-content">
                              {product.Size}
                              <Link
                                to="/sizes"
                                style={{
                                  marginLeft: '10px',
                                  textDecoration: 'underline',
                                  verticalAlign: 'middle',
                                  fontSize: '12px',
                                }}
                              >
                                Таблица размеров и стилей
                              </Link>
                            </div>
                          </div>

                          <div className="params-row">
                            <div className="param-label">Цвет</div>
                            <div className="param-content">{product.Color.Name.ru}</div>
                          </div>
                        </div>
                      </div>
                    </div>

                    {/*Цены десктоп*/}
                    {!app.wholesaleMode ? (
                      <div className="params-row">
                        <div className="param-label">Розничная<br />цена</div>
                        <div className="param-content">
                          <div className="price">
                            {(product.Discount && product.Discount.Value > 0) ? <span className="sum old-sum">{product.Discount.PriceOld}</span> : null}
                            <span className="sum">{product.Price}</span>
                            {' '}
                            <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                          </div>
                        </div>
                      </div>
                    ) : null}

                    {!app.wholesaleMode ? (
                      <div className="params-row">
                        <div className="param-label">Оптовая<br />цена</div>
                        <div className="param-content">
                          <div
                            className="price"
                            style={{
                              fontSize: '11px',
                              fontWeight: 700,
                              textTransform: 'uppercase',
                              lineHeight: 1.875,
                            }}
                          >
                            Чтобы узнать цену, необходимо<br />войти в B2B портал
                          </div>
                        </div>
                      </div>
                    ) : null}

                    {/*Другие размеры десктоп*/}
                    {user.AccountTYPE === 'retail' && product.OtherSizes.length ? (
                      <div className="params-row">
                        <div className="param-label">Другие размеры</div>
                        <div className="param-content">
                          <div className="sizes">
                            {product.OtherSizes.map((size, index) =>
                              <Link
                                className="size-selector"
                                to={`/catalogue/product/${size.URLName}`}
                                key={index}
                                onMouseEnter={() => fetchProduct(size.URLName, true)}
                              >
                                {size.Size}
                              </Link>
                            )}
                          </div>
                        </div>
                      </div>
                    ) : null}

                    {!app.wholesaleMode ? (
                      <div className="params-row">
                        <div className="param-label" />
                        <div className="param-content controls">
                          <NumberInput id={`quantity-${product.Id}`} />
                          <button
                            className={classNames({
                              'main-button': true,
                              'buy-button': true,
                              clicked: isInBasket,
                            })}
                            onClick={isInBasket ? null : () => { this.addToBasket(product.Id); }}
                          >
                            <svg className="icon svg-icon-basket-add">
                              <use xlinkHref="/assets/images/sprite.svg#icon-basket-add" />
                            </svg>
                            <span className="text">{isInBasket ? 'В корзине' : 'Купить'}</span>
                          </button>
                        </div>
                      </div>
                    ) : null}
                  </MediaQuery>
                </div>
              </div>
            </div>

            {user.AccountTYPE === 'wholesale' ? (
              <div className="wholesale-order-block">
                <MediaQuery query='(min-width: 769px)'>
                  <WholesaleOrder
                    product={product}
                    basket={basket}
                    addToBasket={(id) => this.addToBasket(id)}
                    fetchProduct={(url) => this.fetchProduct(url, true)}
                    deleteBasket={(id) => deleteBasket(id)}
                  />
                </MediaQuery>
                <MediaQuery query='(max-width: 768px)'>
                  <WholesaleOrderMobile
                    product={product}
                    basket={basket}
                    addToBasket={(id) => this.addToBasket(id)}
                    fetchProduct={(url) => this.fetchProduct(url, true)}
                    deleteBasket={(id) => deleteBasket(id)}
                  />
                </MediaQuery>
              </div>
            ) : null}

            {technologies.length ? (
              <MediaQuery query='(max-width: 768px)'>
                <TechnologiesMobileBanner technologies={technologies} />
              </MediaQuery>
            ) : null}

            {technologies.length ? (
              <MediaQuery query='(min-width: 769px)'>
                <TechnologiesBanner technologies={technologies} />
              </MediaQuery>
            ) : null}

            <div className="banner-with-centered-content full-width darkened" style={{ backgroundImage: 'url(//media.canoe.ru/images/backgrounds/sheeps.jpg)' }}>
              <div className="content">
                <svg className="decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
                <div className="text">Мы постоянно ищем самые умные в мире<br />способы обработки натуральных материалов.</div>
                <svg className="decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              </div>
            </div>

            <SimilarProducts list={product.Combinations} />
          </div>
        ) : <Preloader />}


        <div className="zoomed-photo">
          <Swipeable onSwipedLeft={() => this.switchPhoto(1)} onSwipedRight={() => this.switchPhoto(-1)}>
            <div className="photo" onClick={() => this.zoomPhoto('')} style={{
              backgroundImage: `url("${this.state.zoomedPhoto}")`
            }}></div>
          </Swipeable>
          <MediaQuery query='(max-width: 768px)'>
            <svg className="icon svg-icon-cross" onClick={() => this.zoomPhoto('')}><use xlinkHref="/assets/images/sprite.svg#icon-cross" /></svg>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <button onClick={() => this.switchPhoto(-1)} className="arrow arrow-left">
              <svg className="arrow-icon svg-arrow-left">
                <use xlinkHref="/assets/images/sprite.svg#arrow-left" />
              </svg>
            </button>
            <button onClick={() => this.switchPhoto(1)} className="arrow arrow-right">
              <svg className="arrow-icon svg-arrow-right">
                <use xlinkHref="/assets/images/sprite.svg#arrow-right" />
              </svg>
            </button>
          </MediaQuery>
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  const { app, basket, user, product } = state;
  return {
    app,
    basket,
    user,
    product,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);
