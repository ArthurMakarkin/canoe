import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

import NumberInput from 'app/components/NumberInput';

export default class WholesaleOrder extends Component {
  static propTypes = {
    addToBasket: PropTypes.func,
    fetchProduct: PropTypes.func,
    deleteBasket: PropTypes.func,
    basket: PropTypes.object,
    product: PropTypes.object,
  }

  render() {
    const { basket, product, addToBasket, fetchProduct, deleteBasket } = this.props;

    return (
      <div className="collapsed-block">
        <div className="collapsed-title active js-collapsed-toggler">
          <span className="text">Цвета и количество</span>
          <svg className="arrow svg-arrow-down"><use xlinkHref="/assets/images/sprite.svg#arrow-down" /></svg>
        </div>

        <div className="collapsed-content">
          <table className="table striped-table">
            <thead className="table-head">
              <tr className="row">
                <td className="cell" />
                <td className="cell">Артикул</td>
                <td className="cell">Цвет</td>
                <td className="cell">Размер</td>
                <td className="cell">Наличие</td>
                <td className="cell">Количество</td>
                <td className="cell" />
              </tr>
            </thead>

            <tbody className="table-body">
              {[product].concat(product.OtherColors, product.OtherSizes).map((color, index) => {
                const inBasket = basket.BasketList ? basket.BasketList.find(p => p.Product.Id === color.Id) : false;
                const i = index;

                return (
                  <tr key={i} className="row">
                    <td className="cell image-cell">
                      <Link
                        className="image"
                        to={`/catalogue/product/${color.URLName}`}
                        onMouseEnter={() => fetchProduct(color.URLName)}
                      >
                        <img src={color.Img.ThumbUrl} alt="" />
                      </Link>
                    </td>
                    <td className="cell">
                      <span className="vendor-code">{color.Article}</span>
                    </td>
                    <td className="cell">
                      <span className="color">{color.Color.Name.ru}</span>
                    </td>
                    <td className="cell">
                      <span className="size">{color.Size}</span>
                    </td>
                    <td className="cell">
                      <span className="in-stock">
                        {color.Quantity}
                        {' '}
                        {color.Unit.Name.ru}
                      </span>
                    </td>
                    <td className="cell">
                      {inBasket ? (
                        <div>
                          <span className="quantity">{inBasket.Quantity}</span>
                          <button
                            className="button-remove"
                            onClick={() => deleteBasket(color.Id)}
                          >
                            <svg className="icon svg-icon-cross">
                              <use xlinkHref="/assets/images/sprite.svg#icon-cross" />
                            </svg>
                          </button>
                        </div>
                      ) : (
                        <NumberInput id={`quantity-${color.Id}`} maxValue={color.Quantity} />
                      )}
                    </td>
                    <td className={classNames("cell", "button-cell", {
                      "in-basket": inBasket
                    })}>
                      <button
                        className={classNames("in-basket-btn", {
                          'main-button': true,
                        })}
                        disabled
                      >
                        <svg className="icon svg-icon-input-check">
                          <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                        </svg>
                        <span className="text">В корзине</span>
                      </button>
                      <button
                        className={classNames("not-in-basket-btn", {
                          'main-button': true,
                        })}
                        onClick={() => addToBasket(color.Id)}
                      >
                        <svg className="icon svg-icon-basket-add">
                          <use xlinkHref="/assets/images/sprite.svg#icon-basket-add" />
                        </svg>
                        <span className="text">Купить</span>
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}



