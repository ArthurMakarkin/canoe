import React, { Component } from 'react';
import { Link } from 'react-router';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';

import * as actions from 'app/actions';

const $ = window.jQuery;

export class OtherColors extends Component {

  onRefresh(inst) {
    this.instance = inst;
    $(".colors-list .b-iscroll-container").width($(".colors-list .b-iscroll-container .color").length * 96);
  }

  next(){
    this.instance.next();
  }

  prev(){
    this.instance.prev();
  }

  render() {
    const { colors, labelBr } = this.props;
    const { fetchProduct } = actions;

    const options = {
      scrollX: true,
      scrollY: false,
      snap: '.color',
      // probeType: 3,
      // bounce: false,
      // bounceTime: 0,
      // deceleration: 0,
      // momentum: false,
      // mouseWheel: true,
      click: true,
      preventDefault: false,
      eventPassthrough: true,
    }


    return (
      <div className="params-row">
        <div className="param-label">Другие{labelBr ? <br /> : ' '}цвета</div>
        <div className="param-content">
          <div className="colors-list">
            <ReactIScroll
              className="b-iscroll"
              onRefresh={(inst) => this.onRefresh(inst)}
              options={options}
              iScroll={iScroll}>
              <div className="b-iscroll-container">
                {colors.map((color, index) =>
                  <Link
                    className="color"
                    to={`/catalogue/product/${color.URLName}`}
                    key={index}
                    onMouseEnter={() => fetchProduct(color.URLName, true)}
                  >
                    <img src={color.Img.ThumbUrl} alt="" />
                  </Link>
                )}
              </div>
            </ReactIScroll>
            {colors.length >= 5 ? (
              <button type="button" onClick={() => this.prev()} className="slick-arrow slick-prev"><svg className="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" strokeMiterlimit="10"><path d="M7.707.354l-7 7 7 7M.707 7.354h18" vectorEffect="non-scaling-stroke" /></g></svg></button>
            ) : null}
            {colors.length >= 5 ? (
              <button type="button" onClick={() => this.next()} className="slick-arrow slick-next"><svg className="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" strokeMiterlimit="10"><path d="M11 14.354l7-7-7-7M18 7.354H0" vectorEffect="non-scaling-stroke" /></g></svg></button>
            ) : null}
          </div>
        </div>
      </div>
    )
  }
}

export default OtherColors;

