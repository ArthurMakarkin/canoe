import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

import NumberInput from 'app/components/NumberInput';

export default class WholesaleOrder extends Component {
  static propTypes = {
    addToBasket: PropTypes.func,
    fetchProduct: PropTypes.func,
    deleteBasket: PropTypes.func,
    basket: PropTypes.object,
    product: PropTypes.object,
  }

  render() {
    const { basket, product, addToBasket, fetchProduct, deleteBasket } = this.props;

    return (
      <div className="collapsed-block">
        <div className="collapsed-title active">
          <span className="text">Другие цвета</span>
        </div>

        <div className="mobile-products">
          {[product].concat(product.OtherColors, product.OtherSizes).map((color, index) => {
            const inBasket = basket.BasketList ? basket.BasketList.find(p => p.Product.Id === color.Id) : false;

            return (
              <div className="mobile-product">
                <Link
                  className="image"
                  to={`/catalogue/product/${color.URLName}`}
                  onMouseEnter={() => fetchProduct(color.URLName)}
                >
                  <img src={color.Img.ThumbUrl} alt="" />
                </Link>
                <div className="info">
                  <span className="vendor-code">{color.Article}</span>
                  <span className="color">{color.Color.Name.ru}</span>
                  <span className="size">{color.Size}</span>
                </div>
                <div className={classNames("quantity", {
                  "in-basket": inBasket
                })}>
                  {inBasket ? (
                    <div>
                      <span className="count">{inBasket.Quantity}</span>
                      <button
                        className="button-remove"
                        onClick={() => deleteBasket(color.Id)}
                      >
                        <svg className="icon svg-icon-cross">
                          <use xlinkHref="/assets/images/sprite.svg#icon-cross" />
                        </svg>
                      </button>
                    </div>
                  ) : (
                    <div>
                      <NumberInput id={`quantity-${color.Id}`} maxValue={color.Quantity} />
                      <p className="availability">Доступно {color.Quantity}{' '}{color.Unit.Name.ru}</p>
                    </div>
                  )}
                </div>
                <div className={classNames("btns", {
                  "in-basket": inBasket
                })}>
                  <button
                    className={classNames("not-in-basket-btn", {
                      'main-button': true,
                    })}
                    onClick={() => addToBasket(color.Id)}
                  >
                    <svg className="icon svg-icon-basket-add">
                      <use xlinkHref="/assets/images/sprite.svg#icon-basket-add" />
                    </svg>
                  </button>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}



