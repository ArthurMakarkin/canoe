import React, { Component } from 'react';
import { Link } from 'react-router';
import MediaQuery from 'react-responsive';

export default class NotFound extends Component {
  render() {
    return (
      <div className="not-found-page">
        <MediaQuery query='(max-width: 768px)'>
          <div className="container">
            <svg className="svg-logo-feather">
              <use xlinkHref="/assets/images/sprite.svg#logo-feather" />
            </svg>
            <div className="t-404">Такой страницы<br />не существует</div>

            <Link className="main-button" to="/">Вернуться на главную</Link>
          </div>
        </MediaQuery>
        <MediaQuery query='(min-width: 769px)'>
          <div>
            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            <div className="title">404</div>
            <div className="subtitle">Этой страницы не существует</div>
            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
          </div>
        </MediaQuery>
      </div>
    );
  }
}
