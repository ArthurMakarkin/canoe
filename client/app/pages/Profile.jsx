import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Preloader from 'app/components/Preloader';

import * as actions from 'app/actions';

import api from 'app/actions/api';

export class Profile extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const form = event.target;
    const button = form.elements.button;

    const { user } = this.props;
    const userInfo = {};
    const contactInfo = {};

    contactInfo.FirstName = form.elements.FirstName.value;
    contactInfo.LastName = form.elements.LastName.value;
    contactInfo.Patronymic = '';
    contactInfo.Phone = form.elements.Phone.value;
    contactInfo.Email = form.elements.Email.value;

    userInfo.AccountUUID = user.AccountUUID;
    userInfo.password = form.elements.password.value;
    userInfo.newPassword = form.elements.newPassword.value;
    userInfo.newPasswordConfirm = form.elements.newPasswordConfirm.value;

    userInfo.ContactInformation = contactInfo;

    if (user.AccountTYPE === 'retail') {
      userInfo.PersonInformation = user.PersonInformation;
    }

    if (user.AccountTYPE === 'wholesale') {
      const companyInfo = {};
      companyInfo.UUID = user.CompanyInformation.UUID;
      companyInfo.Name = form.elements.CompanyName.value;
      companyInfo.INN = form.elements.CompanyINN.value;
      companyInfo.Phone = form.elements.CompanyPhone.value;
      userInfo.CompanyInformation = companyInfo;
    }

    button.setAttribute('disabled', 'disabled');

    api('update_profile', userInfo).then((data) => {
      this.props.actions.onLogin(data);
      button.textContent = 'Успех';
      button.classList.add('success');
    }).catch((error) => {
      console.log(error); // eslint-disable-line
      button.removeAttribute('disabled');
    });
  }

  render() {
    const { user } = this.props;
    const type = user.AccountTYPE;

    return (
      <section className="page-content">
        <div className="content-wrapper">
          {!type ? (
            <Preloader />
          ) : null}
          {type === 'retail' ? (
            <form className="form" name="update_profile" onSubmit={this.handleSubmit}>
              <h2>Персональные данные</h2>
              <div className="inputs">
                <div className="input-group">
                  <label className="label" htmlFor="#">Имя</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.ContactInformation.FirstName} name="FirstName" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Фамилия</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.ContactInformation.LastName} name="LastName" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Номер телефона</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.ContactInformation.Phone} name="Phone" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <h2>Изменение почты и пароля</h2>
                <div className="input-group">
                  <label className="label" htmlFor="#">Электронная почта</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.ContactInformation.Email} name="Email" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Старый пароль</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="password" name="password" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Новый пароль</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="password" name="newPassword" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Повторите новый пароль</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="password" name="newPasswordConfirm" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
              </div>
              <div className="buttons">
                <button className="round-button" name="button" type="submit">Обновить</button>
              </div>
            </form>
          ) : null}

          {type === 'wholesale' ? (
            <form className="form" name="update_profile" onSubmit={this.handleSubmit}>
              <h2>Персональные данные</h2>
              <div className="inputs two-columns-inputs">
                <div className="column">
                  <div className="input-group">
                    <label className="label" htmlFor="#">Название компании</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" defaultValue={user.CompanyInformation.Name} name="CompanyName" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                  <div className="input-group">
                    <label className="label" htmlFor="#">ИНН</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" defaultValue={user.CompanyInformation.INN} name="CompanyINN" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                  <div className="input-group">
                    <label className="label" htmlFor="#">Телефон компании</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" defaultValue={user.CompanyInformation.Phone} name="CompanyPhone" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                </div>
                <div className="column">
                  <div className="input-group">
                    <label className="label" htmlFor="#">Имя</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" defaultValue={user.ContactInformation.FirstName} name="FirstName" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                  <div className="input-group">
                    <label className="label" htmlFor="#">Фамилия</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" defaultValue={user.ContactInformation.LastName} name="LastName" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                  <div className="input-group">
                    <label className="label" htmlFor="#">Контактный телефон</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" defaultValue={user.ContactInformation.Phone} name="Phone" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                </div>
                <h2>Изменение почты и пароля</h2>
                <div className="column">
                  <div className="input-group">
                    <label className="label" htmlFor="#">Электронная почта</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" defaultValue={user.ContactInformation.Email} name="Email" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                  <div className="input-group">
                    <label className="label" htmlFor="#">Старый пароль</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="password" name="password" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                </div>
                <div className="column">
                  <div className="input-group">
                    <label className="label" htmlFor="#">Новый пароль</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="password" name="newPassword" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                  <div className="input-group">
                    <label className="label" htmlFor="#">Повторите новый пароль</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="password" name="newPasswordConfirm" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
              <div className="buttons">
                <button className="round-button" name="button" type="submit">Обновить</button>
              </div>
            </form>
          ) : null}
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state;
  return {
    user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
