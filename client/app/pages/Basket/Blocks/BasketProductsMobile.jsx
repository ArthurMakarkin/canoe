import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

import NumberInput from 'app/components/NumberInput';

export default class BasketProductsMobile extends Component {
  static propTypes = {
    basket: PropTypes.object,
    coupon: PropTypes.string,
    couponChange: PropTypes.func,
    errors: PropTypes.object,
    wholesale: PropTypes.bool,
    deleteBasket: PropTypes.func,
    handleUpdate: PropTypes.func,
    sumError: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    basketErrors: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.bool,
    ]),
    nextStep: PropTypes.func,
    prevStep: PropTypes.func,
  }

  render() {
    const { basket, coupon, deleteBasket, errors, wholesale, handleUpdate, sumError, basketErrors, nextStep, prevStep, couponChange } = this.props;

    return (
      <div className="mobile-products mobile-basket">
        <div className="products">
          {basket.BasketList.map((product, index) => (
            <div className="mobile-product" key={index}>
              <Link className="image" to={`/catalogue/product/${product.Product.URLName}`}>
                <img src={product.Product.Img.ThumbUrl} alt="" />
              </Link>
              <div className="info">
                <span className="name">{product.Product.Name}</span>
                <span className="vendor-code">Арт. {product.Product.Article}</span>
                <span className="color">{product.Product.Color.Name.ru}</span>
                <span className="size">{product.Product.Size}</span>
              </div>
              <div className="price">
                <span className="sum">{product.Summ}</span>
                {' '}
                <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
              </div>
              <div className="quantity">
                <button
                  className="button-remove"
                  onClick={() => { deleteBasket(product.Product.Id); }}
                >
                  <svg className="icon svg-icon-cross"><use xlinkHref="/assets/images/sprite.svg#icon-cross" /></svg>
                </button>
                <NumberInput id={`q-${product.Product.Id}`} value={product.Quantity} maxValue={product.Quantity !== product.RequestQuantity ? product.Quantity : 999} onChangeCallback={() => { handleUpdate(); }} />
                {/*<p className="availability">Доступно {product.Product.Quantity}{' '}{product.Product.Unit.Name.ru}</p>*/}
              </div>
            </div>
          ))}
        </div>
        <div className="basket-info">
          <label>Промокод</label>
          <input className="coupon-input" placeholder="Промокод" type="text" onChange={(event) => {
            couponChange(event.target.value);
          }} defaultValue={coupon} name="Coupon"/>
        </div>
        {basket.TotalDiscount > 0 ? (
          <div className="basket-info">
            <label>Скидка</label>
            <div className="price">
              <span className="sum">{basket.TotalDiscount}</span>
              {' '}
              <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
            </div>
          </div>
        ) : null}
        {wholesale ? (
          <div className="basket-info">
            <label>Вес</label>
            <div className="price">
              <span className="sum">{basket.TotalWeight}</span>
              {' '}
              <span className="currency">кг</span>
            </div>
          </div>
        ) : null}
        {wholesale ? (
          <div className="g-info">
            <label>Объём</label>
            <div className="price">
              <span className="sum">{basket.TotalVolume}</span>
              {' '}
              <span className="currency">м³</span>
            </div>
          </div>
        ) : null}

        <div className="basket-info">
          <label className="price-label">Общая стоимость</label>
          <div className={classNames({
            price: true,
            'full-price': true,
            error: sumError,
          })}
          >
            <span className="sum">{basket.TotalSumm}</span>
            {' '}
            <span className="currency currency-big"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
          </div>
        </div>

        {basketErrors ? (
          <div className="basket-info">
            <div className="errors">
              {errors.data.map((error, index) => (
                <div className="error" key={index}>{error.caption.ru}</div>
              ))}
            </div>
          </div>
        ) : null}
        <div className="basket-info">
          <button
            className="main-button dark"
            onClick={nextStep}
            disabled={sumError || basket.BasketList.length === 0}
          >Продолжить</button>
        </div>
      </div>
    );
  }
}
