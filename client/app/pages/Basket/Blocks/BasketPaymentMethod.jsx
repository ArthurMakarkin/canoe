import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

export default class BasketPaymentMethod extends Component {
  static propTypes = {
    basket: PropTypes.object,
    regionDeliveryOptions: PropTypes.array,
    deliveryUUID: PropTypes.string,
    TotalSumm: PropTypes.number,
    sumError: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    basketErrors: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.bool,
    ]),
    wholesale: PropTypes.bool,
    paymentOptions: PropTypes.array,
    paymentError: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    errors: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    method: PropTypes.string,
    handleMethod: PropTypes.func,
    AccountUUID: PropTypes.string,
    orderNumber: PropTypes.number,
    orderUUID: PropTypes.string,
    handlePayment: PropTypes.func,
    prevStep: PropTypes.func,
  }

  render() {
    const { basket, regionDeliveryOptions, deliveryUUID, TotalSumm, sumError, basketErrors, wholesale, paymentOptions, paymentError, method, handleMethod, AccountUUID, orderNumber, orderUUID, errors, handlePayment, prevStep } = this.props;

    return (
      <table className="table order-table">
        <thead className="table-head">
          <tr className="row">
            <td className="cell" />
            <td className="cell">Название</td>
            <td className="cell">Артикул</td>
            <td className="cell">Цвет</td>
            <td className="cell">Размер</td>
            <td className="cell">Цена</td>
            <td className="cell">Количество</td>
          </tr>
        </thead>
        <tbody className="table-body">
          {basket.BasketList.map((product, index) => (
            <tr className="row" key={index}>
              <td className="cell image-cell">
                <Link className="image" to={`/catalogue/product/${product.Product.URLName}`}>
                  <img src={product.Product.Img.ThumbUrl} alt="" />
                </Link>
              </td>
              <td className="cell">
                <div className="name">{product.Product.Name}</div>
              </td>
              <td className="cell">
                <div className="vendor-code">{product.Product.Article}</div>
              </td>
              <td className="cell">
                <div className="color">{product.Product.Color.Name.ru}</div>
              </td>
              <td className="cell">
                <div className="size">{product.Product.Size}</div>
              </td>
              <td className="cell">
                <div className="price">
                  <span className="sum">{product.Price}</span>
                  {' '}
                  <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                </div>
              </td>
              <td className="cell">
                <div className="quantity">{product.Quantity}</div>
              </td>
            </tr>
          ))}
          <tr className="basket-info">
            <td className="cell" colSpan="3">
              <Link className="back-link" onClick={prevStep}>
                <svg className="arrow svg-arrow-left"><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>
                <span className="text">Назад</span>
              </Link>
            </td>

            {basket.TotalDiscount > 0 ? (
              <td className="cell" colSpan="2">
                <div className="price-label">Скидка</div>
              </td>
              ) : null
             }

            {basket.TotalDiscount > 0 ? (
              <td className="cell">
                <div className="price">
                  <span className="sum">{basket.TotalDiscount}</span>
                  {' '}
                  <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                </div>
              </td>
              ) : null
            }

            <td className="cell" colSpan="2">
              {/* <Link className="link" to="/discounts">О системе скидок</Link> */}
            </td>
          </tr>

          <tr className="basket-info">
            <td className="cell" colSpan="3" />
            <td className="cell" colSpan="2">
              <div className="price-label">Стоимость заказа</div>
            </td>
            <td className="cell">
              <div className={classNames({
                price: true,
                'price': true,
                error: sumError,
              })}
              >
                <span className="sum">
                  {basket.BasketList.reduce((a,b) => a + b.Summ, 0)}
                </span>
                {' '}
                <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
              </div>
            </td>
            <td className="cell" colSpan="2" />
          </tr>

          <tr className="basket-info">
            <td className="cell" colSpan="3" />
            <td className="cell" colSpan="2">
              <div className="price-label">Стоимость доставки</div>
            </td>
            <td className="cell">
              <div className={classNames({
                price: true,
                'full-price': true,
                error: sumError,
              })}
              >
                <span className="sum">
                  {deliveryUUID ? regionDeliveryOptions.find(o => o.UUID === deliveryUUID).Cost : null}
                </span>
                {' '}
                <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
              </div>
            </td>
            <td className="cell" colSpan="2" />
          </tr>

          <tr className="basket-info">
            <td className="cell" colSpan="3" />
            <td className="cell" colSpan="2">
              <div className="price-label">Общая стоимость</div>
            </td>
            <td className="cell">
              <div className={classNames({
                price: true,
                'full-price': true,
                error: sumError,
              })}
              >
                <span className="sum">{TotalSumm}</span>
                {' '}
                <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
              </div>
            </td>
            <td className="cell" colSpan="2" />
          </tr>
          {wholesale ? (
            <tr className="basket-info">
              <td className="cell" colSpan="3" />
              <td className="cell" colSpan="2">
                <div className="price-label">Вес</div>
              </td>
              <td className="cell">
                <div className="price full-price">
                  <span className="sum">{basket.TotalWeight}</span>
                  {' '}
                  <span className="currency">кг</span>
                </div>
              </td>
              <td className="cell" colSpan="2" />
            </tr>
          ) : null}
          {wholesale ? (
            <tr className="basket-info">
              <td className="cell" colSpan="3" />
              <td className="cell" colSpan="2">
                <div className="price-label">Объём</div>
              </td>
              <td className="cell">
                <div className="price full-price">
                  <span className="sum">{basket.TotalVolume}</span>
                  {' '}
                  <span className="currency">м³</span>
                </div>
              </td>
              <td className="cell" colSpan="2" />
            </tr>
          ) : null}
          <tr className="basket-info">
            <td className="cell" colSpan="3" />
            <td className="cell" colSpan="2">
              <div className="price-label">Способ оплаты</div>
            </td>
            <td className="cell">
              <ul className="payment-method radio-group">
               {paymentOptions.map((option) => {
                 const value = (option.Module === 'Yandex.Money') ? 'AC' : option.Module;
                 return <li key={option.Id}>
                   <div className="radio">
                     <input
                       className="radio-input"
                       type="radio"
                       id={option.Id}
                       name="payment-method"
                       value={value}
                       onChange={handleMethod}
                       checked={method === value}
                     />
                     <span className="radio-circle" />
                     <label htmlFor={option.Id} className="radio-label">
                       {option.Name.ru}
                     </label>
                   </div>
                 </li>
               })}
              </ul>
            </td>
            <td className="cell" colSpan="2" />
          </tr>
          { paymentError ? (
            <tr className="basket-info">
              <td className="cell" colSpan="3" />
              <td className="cell" colSpan="2">
                <div className="errors">
                  <div className="error">{paymentError.caption.ru}</div>
                </div>
              </td>
              <td className="cell" />
              <td className="cell" colSpan="2" />
            </tr>
          ) : null }
          {basketErrors ? (
            <tr className="basket-info">
              <td className="cell" colSpan="3" />
              <td className="cell" colSpan="2">
                <div className="errors">
                  {errors.data.map((error, index) => (
                    <div className="error" key={index}>{error.caption.ru}</div>
                  ))}
                </div>
              </td>
              <td className="cell" />
              <td className="cell" colSpan="2" />
            </tr>
          ) : null}
          <tr className="basket-info">
            <td className="cell" colSpan="3" />
            <td className="cell" colSpan="2">
              <form action="https://money.yandex.ru/eshop.xml" method="POST" onSubmit={handlePayment}>
                <input name="shopId" value="75950" type="hidden" />
                <input name="scid" value="75105" type="hidden" />
                <input name="sum" value={TotalSumm} type="hidden" />
                <input name="customerNumber" value={AccountUUID} type="hidden" />
                <input name="paymentType" value={method} type="hidden" />
                <input name="orderNumber" value={orderNumber || 0} type="hidden" />
                <input name="orderUUID" value={orderUUID || 0} type="hidden" />
                <button
                  className="main-button dark"
                  type="submit"
                  disabled={sumError || basket.BasketList.length === 0}
                >Перейти к оплате</button>
              </form>
            </td>
            <td className="cell" />
            <td className="cell" colSpan="2" />
          </tr>
        </tbody>
      </table>
    );
  }
}
