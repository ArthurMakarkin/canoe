import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

export default class BasketPaymentMethod extends Component {
  static propTypes = {
    basket: PropTypes.object,
    regionDeliveryOptions: PropTypes.array,
    deliveryUUID: PropTypes.string,
    TotalSumm: PropTypes.number,
    sumError: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    basketErrors: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.bool,
    ]),
    paymentOptions: PropTypes.array,
    paymentError: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    errors: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    method: PropTypes.string,
    handleMethod: PropTypes.func,
    AccountUUID: PropTypes.string,
    orderNumber: PropTypes.number,
    orderUUID: PropTypes.string,
    handlePayment: PropTypes.func,
  }

  render() {
    const { basket, regionDeliveryOptions, deliveryUUID, TotalSumm, sumError, basketErrors, paymentOptions, paymentError, method, handleMethod, AccountUUID, orderNumber, orderUUID, errors, handlePayment } = this.props;

    return (
      <div className="mobile-basket margin-top">
        <div className="basket-info">
          <ul className="payment-method radio-group">
           {paymentOptions.map((option) => {
             const value = (option.Module === 'Yandex.Money') ? 'AC' : option.Module;
             return <li key={option.Id}>
               <div className="radio">
                 <input
                   className="radio-input"
                   type="radio"
                   id={option.Id}
                   name="payment-method"
                   value={value}
                   onChange={handleMethod}
                   checked={method === value}
                 />
                 <span className="radio-circle" />
                 <label htmlFor={option.Id} className="radio-label">
                   {option.Name.ru}
                 </label>
               </div>
             </li>
           })}
          </ul>
        </div>
        { paymentError ? (
          <div className="basket-info">
            <div className="errors">
              <div className="error">{paymentError.caption.ru}</div>
            </div>
          </div>
        ) : null }
        {basketErrors ? (
          <div className="basket-info">
            <div className="errors">
              {errors.data.map((error, index) => (
                <div className="error" key={index}>{error.caption.ru}</div>
              ))}
            </div>
          </div>
        ) : null}
        <div className="basket-info">
          <form action="https://money.yandex.ru/eshop.xml" method="POST" onSubmit={handlePayment}>
            <input name="shopId" value="75950" type="hidden" />
            <input name="scid" value="75105" type="hidden" />
            <input name="sum" value={TotalSumm} type="hidden" />
            <input name="customerNumber" value={AccountUUID} type="hidden" />
            <input name="paymentType" value={method} type="hidden" />
            <input name="orderNumber" value={orderNumber || 0} type="hidden" />
            <input name="orderUUID" value={orderUUID || 0} type="hidden" />
            <button
              className="main-button dark"
              type="submit"
              disabled={sumError || basket.BasketList.length === 0}
            >Перейти к оплате</button>
          </form>
        </div>
      </div>
    );
  }
}
