import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

import NumberInput from 'app/components/NumberInput';

export default class BasketProducts extends Component {
  static propTypes = {
    basket: PropTypes.object,
    coupon: PropTypes.string,
    couponChange: PropTypes.func,
    errors: PropTypes.object,
    wholesale: PropTypes.bool,
    deleteBasket: PropTypes.func,
    handleUpdate: PropTypes.func,
    sumError: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.bool,
    ]),
    basketErrors: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.bool,
    ]),
    nextStep: PropTypes.func,
    prevStep: PropTypes.func,
  }

  render() {
    const { basket, coupon, deleteBasket, errors, wholesale, handleUpdate, sumError, basketErrors, nextStep, prevStep, couponChange } = this.props;

    return (
      <table className="table order-table">
        <thead className="table-head">
          <tr className="row">
            <td className="cell" />
            <td className="cell">Название</td>
            <td className="cell">Артикул</td>
            <td className="cell">Цвет</td>
            <td className="cell">Размер</td>
            <td className="cell">Цена</td>
            <td className="cell">Количество</td>
            <td className="cell">Сумма</td>
            <td className="cell" />
          </tr>
        </thead>
        <tbody className="table-body">
          {basket.BasketList.map((product, index) => (
            <tr className="row" key={index}>
              <td className="cell image-cell">
                <Link className="image" to={`/catalogue/product/${product.Product.URLName}`}>
                  <img src={product.Product.Img.ThumbUrl} alt="" />
                </Link>
              </td>
              <td className="cell">
                <div className="name">{product.Product.Name}</div>
              </td>
              <td className="cell">
                <div className="vendor-code">{product.Product.Article}</div>
              </td>
              <td className="cell">
                <div className="color">{product.Product.Color.Name.ru}</div>
              </td>
              <td className="cell">
                <div className="size">{product.Product.Size}</div>
              </td>
              <td className="cell">
                <div className="price">
                  <span className="sum">{product.Price}</span>
                  {' '}
                  <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                </div>
              </td>
              <td className="cell">
                <NumberInput id={`q-${product.Product.Id}`} value={product.Quantity} maxValue={product.Quantity !== product.RequestQuantity ? product.Quantity : 999} />
              </td>
              <td className="cell">
                <div className="price">
                  <span className="sum">{product.Summ}</span>
                  {' '}
                  <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                </div>
              </td>
              <td className="cell button-cell">
                <button
                  className="button-remove"
                  onClick={() => { deleteBasket(product.Product.Id); }}
                >
                  <svg className="icon svg-icon-cross"><use xlinkHref="/assets/images/sprite.svg#icon-cross" /></svg>
                </button>
              </td>
            </tr>
          ))}

          <tr className="basket-update" >
            <td className="cell" colSpan="1">
              <div className="coupon-label">Промокод</div>
            </td>
            <td className="cell" colSpan="1">
              <input className="coupon-input" type="text" onChange={(event) => {
                couponChange(event.target.value);
              }} defaultValue={coupon} name="Coupon"/>
            </td>
            <td className="cell" colSpan="4" />
            <td className="cell" colSpan="2">
              <button
                className="main-button"
                onClick={() => { handleUpdate(); }}
              >
                <svg className="icon svg-icon-refresh">
                  <use xlinkHref="/assets/images/sprite.svg#icon-refresh" />
                </svg>
                <span className="text">Пересчитать</span>
              </button>
            </td>
          </tr>
          <tr className="basket-info">
            <td className="cell" colSpan="3">
              <Link className="back-link" to="/catalogue">
                <svg className="arrow svg-arrow-left"><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>
                <span className="text">Вернуться в каталог</span>
              </Link>
            </td>

            {basket.TotalDiscount > 0 ? (
              <td className="cell" colSpan="2">
                <div className="price-label">Скидка</div>
              </td>
              ) : null
             }

            {basket.TotalDiscount > 0 ? (
              <td className="cell">
                <div className="price">
                  <span className="sum">{basket.TotalDiscount}</span>
                  {' '}
                  <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                </div>
              </td>
              ) : null
            }

            <td className="cell" colSpan="2">
              {/* <Link className="link" to="/discounts">О системе скидок</Link> */}
            </td>
          </tr>
          <tr className="basket-info">
            <td className="cell" colSpan="3" />
            <td className="cell" colSpan="2">
              <div className="price-label">Общая стоимость</div>
            </td>
            <td className="cell">
              <div className={classNames({
                price: true,
                'full-price': true,
                error: sumError,
              })}
              >
                <span className="sum">{basket.TotalSumm}</span>
                {' '}
                <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
              </div>
            </td>
            <td className="cell" colSpan="2" />
          </tr>
          {wholesale ? (
            <tr className="basket-info">
              <td className="cell" colSpan="3" />
              <td className="cell" colSpan="2">
                <div className="price-label">Вес</div>
              </td>
              <td className="cell">
                <div className="price full-price">
                  <span className="sum">{basket.TotalWeight}</span>
                  {' '}
                  <span className="currency">кг</span>
                </div>
              </td>
              <td className="cell" colSpan="2" />
            </tr>
          ) : null}
          {wholesale ? (
            <tr className="g-info">
              <td className="cell" colSpan="3" />
              <td className="cell" colSpan="2">
                <div className="price-label">Объём</div>
              </td>
              <td className="cell">
                <div className="price full-price">
                  <span className="sum">{basket.TotalVolume}</span>
                  {' '}
                  <span className="currency">м³</span>
                </div>
              </td>
              <td className="cell" colSpan="2" />
            </tr>
          ) : null}


          {basketErrors ? (
            <tr className="basket-info">
              <td className="cell" colSpan="3" />
              <td className="cell" colSpan="2">
                <div className="errors">
                  {errors.data.map((error, index) => (
                    <div className="error" key={index}>{error.caption.ru}</div>
                  ))}
                </div>
              </td>
              <td className="cell" />
              <td className="cell" colSpan="2" />
            </tr>
          ) : null}
          <tr className="basket-info">
            <td className="cell" colSpan="3" />
            <td className="cell" colSpan="2">
              <button
                className="main-button dark"
                onClick={nextStep}
                disabled={sumError || basket.BasketList.length === 0}
              >Продолжить</button>
            </td>
            <td className="cell" />
            <td className="cell" colSpan="2" />
          </tr>
        </tbody>
      </table>
    );
  }
}
