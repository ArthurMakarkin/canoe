import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import classNames from 'classnames';

import MediaQuery from 'react-responsive';

import Preloader from 'app/components/Preloader';
import NumberInput from 'app/components/NumberInput';

import BasketProducts from 'app/pages/Basket/Blocks/BasketProducts';
import BasketProductsMobile from 'app/pages/Basket/Blocks/BasketProductsMobile';
import BasketPaymentMethod from 'app/pages/Basket/Blocks/BasketPaymentMethod';
import BasketPaymentMethodMobile from 'app/pages/Basket/Blocks/BasketPaymentMethodMobile';


import * as actions from 'app/actions';
import api from 'app/actions/api';

function removeDuplicatesBy(keyFn, array) {
  const mySet = new Set();
  const filteredArray = array.filter(x => x);
  return filteredArray.filter((x) => {
    const key = keyFn(x);
    const isNew = !mySet.has(key);
    if (isNew) mySet.add(key);
    return isNew;
  });
}

export class Basket extends Component {
  constructor(props) {
    super(props);

    let step, paymentError;

    switch (props.location.query.action) {
      case 'PaymentFail':
        step = 4;
        paymentError = {
          caption: {
            ru: 'Ошибка оплаты'
          }
        };
        break;
      case 'PaymentSuccess':
        step = 5;
        paymentError = null;
        break;
      default:
        step = 1;
        paymentError = null;
    }

    this.state = {
      step,
      paymentError,
      order: {},
      orderNumber: null,
      method: '',
      deliveryOptions: {
        Regions: [],
        Options: [],
      },
      deliveryRegion: '',
      deliveryUUID: '',
      deliveryType: 'PVZ',
      paymentOptions: [],
      map: null,
      coupon: "",
    };

    this.prevStep = this.prevStep.bind(this);
    this.nextStep = this.nextStep.bind(this);
    this.handleProfile = this.handleProfile.bind(this);
    this.handleOrder = this.handleOrder.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleDelivery = this.handleDelivery.bind(this);
    this.handleMethod = this.handleMethod.bind(this);
    this.updateDeliveryRegion = this.updateDeliveryRegion.bind(this);
    this.updateDeliveryType = this.updateDeliveryType.bind(this);
    this.handlePayment = this.handlePayment.bind(this);
  }

  componentDidMount() {
    api('delivery/options').then((data) => {

      const defaultRegion = data.Regions.find(r => r.Name === 'Москва');

      this.setState({
        deliveryOptions: data,
        deliveryRegion: defaultRegion.ID,
        deliveryUUID: defaultRegion.DeliveryOptions[0].UUID,
        deliveryType: defaultRegion.DeliveryOptions[0].Type,
        paymentOptions: data.DeliveryTypes.find(t => t.Id === this.state.deliveryType).PaymentOptions
      });
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const state = this.state;

    if (
      (state.step !== prevState.step && state.step === 3) ||
      (prevState.deliveryType !== state.deliveryType && state.deliveryType === 'PVZ') ||
      (prevState.deliveryRegion !== state.deliveryRegion && state.deliveryType === 'PVZ')
    ) {
      setTimeout(() => {
        this.renderMap();
      }, 100);
    }
  }

  updateDeliveryRegion(event) {
    const { deliveryOptions, map } = this.state;
    const newID = event.target.value;
    const region = deliveryOptions.Regions.find(r => r.ID === newID);

    if (map) map.destroy();

    this.setState({
      deliveryRegion: newID,
      deliveryUUID: region.DeliveryOptions[0].UUID,
      deliveryType: region.DeliveryOptions[0].Type,
      paymentOptions: this.state.deliveryOptions.DeliveryTypes.find(t => t.Id === this.state.deliveryType).PaymentOptions
    });
  }

  updateDeliveryType(event) {
    const { deliveryOptions, deliveryRegion, map } = this.state;
    const region = deliveryOptions.Regions.find(r => r.ID === deliveryRegion);
    const type = event.target.value;

    if (map) map.destroy();

    this.setState({
      deliveryUUID: region.DeliveryOptions.find(o => o.Type === type).UUID,
      deliveryType: type,
      paymentOptions: this.state.deliveryOptions.DeliveryTypes.find(t => t.Id === type).PaymentOptions
    });
  }

  prevStep() {
    window.scrollTo(0, 0);
    this.setState({ step: this.state.step - 1 });
  }

  nextStep() {
    const { user } = this.props;
    let nextStep = this.state.step + 1;
    if (!user.ContactInformation) {
      document.querySelector('.header-forms').classList.add('active');
      return;
    }
    window.scrollTo(0, 0);
    nextStep = (user.AccountTYPE == "wholesale" && nextStep === 4) ? nextStep = 5 : nextStep;
    this.setState({ step: nextStep });
  }

  handleProfile(event) {
    event.preventDefault();
    const form = event.target;

    const { user } = this.props;

    const order = {
      Description: '',
      ContractUUID: user.ContractUUID,
      AccountUUID: user.AccountUUID,
    };

    const contactInfo = {};
    contactInfo.FirstName = form.elements.FirstName.value;
    contactInfo.LastName = form.elements.LastName.value;
    contactInfo.Patronymic = '';
    contactInfo.JobTitle = '';
    contactInfo.Phone = form.elements.Phone.value;
    contactInfo.Email = form.elements.Email.value;

    order.ContactInformation = contactInfo;
    order.Coupon = this.state.coupon;


    if (user.AccountTYPE === 'wholesale') {
      const companyInfo = {};
      companyInfo.Name = form.elements.CompanyName.value;
      companyInfo.INN = form.elements.CompanyINN.value;
      companyInfo.KPP = form.elements.CompanyKPP.value;
      companyInfo.Phone = form.elements.CompanyPhone.value;
      companyInfo.UUID = user.CompanyInformation.UUID;
      order.CompanyInformation = companyInfo;
    }

    this.setState({ order });
    this.nextStep();
  }

  handleDelivery(event) {
    event.preventDefault();
    const form = event.target;

    const { user } = this.props;
    const { order, deliveryRegion, deliveryUUID, deliveryType } = this.state;

    let hasErrors = false;

    [
      form.elements.PostCode,
      form.elements.Street,
      form.elements.HouseNumber,
      form.elements.Room,
    ].forEach((item) => {
      if (item && !item.value) {
        item.parentNode.parentNode.classList.add('error');
        hasErrors = true;
      }
    });

    if (hasErrors) return;

    if (deliveryType === 'RUSSIAN_POST' || deliveryType === 'COURIER_ONLINE')
    {
      order.DeliveryInformation = {
        DeliveryOptions: deliveryUUID,
        Region: deliveryRegion,
        Address: {
          PostCode: form.elements.PostCode.value,
          Street: form.elements.Street.value,
          HouseNumber: form.elements.HouseNumber.value,
          Room: form.elements.Room.value,
          Description: form.elements.Description.value,
        },
        DeliveryContact: {
          Phone: form.elements.Phone.value,
        },
      };
    } else {
      order.DeliveryInformation = {
        DeliveryOptions: deliveryUUID,
        Region: deliveryRegion,
        Address: {
          Description: form.elements.Description.value,
        },
        DeliveryContact: {
          Phone: form.elements.Phone.value,
        },
      };
    }

    //alert(user.accountType);

    this.nextStep();

    /*if(user.accountType == "retail")
    {
      this.nextStep();
    }else {
      api('create_order', this.state.order)
        .then((data) => {
          if (data.Id) {
            this.setState({
              order,
              orderNumber: parseInt(data.OrderNum.substr(-6)),
              orderUUID: data.Id,
              deliverySumm: data.DeliverySumm
            });
            this.nextStep();
          }
        });
    }*/
  }

  handleMethod(e) {
    this.setState({ method: e.target.value });
  }

  handlePayment(e) {
    e.preventDefault();
    e.persist();

    //const { user } = this.props;
    const { order, deliveryRegion, deliveryUUID, deliveryType } = this.state;

    //alert(this.state.method);

    if (this.state.method === 'PC' || this.state.method === 'AC') {

      api('create_order', this.state.order)
        .then((data) => {
          if (data.Id) {
            this.setState({
              order,
              orderNumber: parseInt(data.OrderNum.substr(-6)),
              orderUUID: data.Id,
              deliverySumm: data.DeliverySumm
            });
            //this.nextStep();
            e.target.submit();
            this.props.actions.fetchBasket();
          }
        });


    } else {

      api('create_order', this.state.order)
        .then((data) => {
          if (data.Id) {
            this.setState({
              order,
              orderNumber: parseInt(data.OrderNum.substr(-6)),
              orderUUID: data.Id,
              deliverySumm: data.DeliverySumm
            });
            //this.nextStep();
            //e.target.submit();
            this.nextStep();
            this.props.actions.fetchBasket();
          }
        });
      //this.nextStep();
    }

  }

  handleOrder() {

    /*alert("handle Order");
    alert(this.state.orderUUID);*/

    api('create_order', this.state.order).then((data) => {
      this.setState({
        orderNumber: parseInt(data.OrderNum.substr(-6)),
      });
      this.props.actions.fetchBasket();
      this.nextStep();
    });
    /*
    if(  this.state.orderUUID == undefined || this.state.orderUUID.trim() == ""  ) {
      api('create_order', this.state.order).then((data) => {
        this.setState({
          orderNumber: parseInt(data.OrderNum.substr(-6)),
        });
        this.props.actions.fetchBasket();
        this.nextStep();
      });
    }else {
      this.props.actions.fetchBasket();
      this.nextStep();
    }*/
  }

  handleUpdate() {
    const inputs = Array.prototype.slice.call(document.querySelectorAll('[data-quantity]'));
    const products = inputs.map(input => ({
      ProductUUID: input.id.substring(2),
      Quantity: input.value,
    }));
    const {coupon}  = this.state;

    this.props.actions.updateBasket(products,coupon);
  }

  renderStepsThermometer() {
    const { step } = this.state;
    const { user } = this.props;

    return (
      <div className="order-steps full-width">
        <div className={classNames({
          step: true,
          active: step === 1,
          past: step > 1,
        })}
        >
          <svg className="svg-icon-order"><use xlinkHref="/assets/images/sprite.svg#icon-order-basket" /></svg>
          <span className="text">Товары <span>в корзине</span></span>
        </div>

        <svg className={classNames("arrow-icon", "arrow-order-mobile", {
          hidden: step < 2
        })}>
          <use xlinkHref="/assets/images/sprite.svg#arrow-order-mobile" />
        </svg>

        <svg className="arrow svg-arrow-order"><use xlinkHref="/assets/images/sprite.svg#arrow-order" /></svg>

        <div className={classNames({
          step: true,
          active: step === 2,
          past: step > 2,
        })}
        >
          <svg className="svg-icon-order"><use xlinkHref="/assets/images/sprite.svg#icon-order-profile" /></svg>
          <span className="text">Контактные данные</span>
        </div>

        <svg className={classNames("arrow-icon", "arrow-order-mobile", {
          hidden: step < 3
        })}>
          <use xlinkHref="/assets/images/sprite.svg#arrow-order-mobile" />
        </svg>

        <svg className="arrow svg-arrow-order"><use xlinkHref="/assets/images/sprite.svg#arrow-order" /></svg>

        <div className={classNames({
          step: true,
          active: step === 3,
          past: step > 3,
        })}
        >
          <svg className="svg-icon-order"><use xlinkHref="/assets/images/sprite.svg#icon-order-delivery" /></svg>
          <span className="text">Условия доставки</span>
        </div>

        <svg className={classNames("arrow-icon", "arrow-order-mobile", {
          hidden: step < 4
        })}>
          <use xlinkHref="/assets/images/sprite.svg#arrow-order-mobile" />
        </svg>

        <svg className="arrow svg-arrow-order"><use xlinkHref="/assets/images/sprite.svg#arrow-order" /></svg>

        { user.AccountTYPE == 'wholesale' ? null : (
            <div className="payment-step">
              <div className={classNames({
                step: true,
                active: step === 4,
                past: step > 4,
              })}
              >
                <svg className="svg-icon-order"><use xlinkHref="/assets/images/sprite.svg#icon-order-payment" /></svg>
                <span className="text">Способ оплаты</span>
              </div>
            </div>
          )
        }

        { user.AccountTYPE == 'wholesale' ? null : (
          <div style={{display: 'inline-block'}}>
            <svg className="arrow svg-arrow-order"><use xlinkHref="/assets/images/sprite.svg#arrow-order" /></svg>

            <svg className={classNames("arrow-icon", "arrow-order-mobile", {
              hidden: step < 5
            })}>
              <use xlinkHref="/assets/images/sprite.svg#arrow-order-mobile" />
            </svg>
          </div>
          )
        }

        <div className={classNames({
          step: true,
          active: step === 5,
        })}
        >
          <svg className="svg-icon-order"><use xlinkHref="/assets/images/sprite.svg#icon-order-success" /></svg>
          <span className="text">Готово</span>
        </div>
      </div>
    );
  }

  renderBasket() {
    const { basket, user, errors } = this.props;
    const { coupon }   = this.state;
    const wholesale = user.AccountTYPE === 'wholesale';

    const { deleteBasket } = this.props.actions;

    if (!basket.BasketList) {
      return <Preloader />;
    }

    let sumError = null;
    let basketErrors = false;
    if (errors.form && errors.data) {
      sumError = errors.data.find(e => e.name === 'TotalSumm');
      basketErrors = ~errors.form.indexOf('basket');
    }

    const couponChange = (value) => {
      this.state.coupon = value;
      this.handleUpdate()
    }

    let basketProps = {
      basket: basket,
      coupon: coupon,
      couponChange: couponChange,
      errors: errors,
      wholesale: wholesale,
      deleteBasket: deleteBasket,
      handleUpdate: this.handleUpdate,
      sumError: sumError,
      basketErrors: basketErrors,
      nextStep: this.nextStep,
      prevStep: this.prevStep,
    }

    return (
      <div>
        <MediaQuery query='(min-width: 769px)'>
          <BasketProducts {...basketProps} />
        </MediaQuery>
        <MediaQuery query='(max-width: 768px)'>
          <BasketProductsMobile {...basketProps} />
        </MediaQuery>
      </div>
    );
  }

  renderProfile() {
    const { user } = this.props;
    const type = user.AccountTYPE;

    return (
      <div>
        {type === 'retail' ? (
          <form className="form" onSubmit={this.handleProfile}>
            <div className="inputs">
              <div className="input-group">
                <label className="label" htmlFor="#">Имя</label>
                <div className="input-wrapper">
                  <input className="text-input" type="text" defaultValue={user.ContactInformation.FirstName} name="FirstName" />
                  <svg className="check svg-icon-input-check">
                    <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                  </svg>
                </div>
              </div>
              <div className="input-group">
                <label className="label" htmlFor="#">Фамилия</label>
                <div className="input-wrapper">
                  <input className="text-input" type="text" defaultValue={user.ContactInformation.LastName} name="LastName" />
                  <svg className="check svg-icon-input-check">
                    <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                  </svg>
                </div>
              </div>
              <div className="input-group">
                <label className="label" htmlFor="#">Номер телефона</label>
                <div className="input-wrapper">
                  <input className="text-input" type="text" defaultValue={user.ContactInformation.Phone} name="Phone" />
                  <svg className="check svg-icon-input-check">
                    <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                  </svg>
                </div>
              </div>
              <div className="input-group">
                <label className="label" htmlFor="#">Электронная почта</label>
                <div className="input-wrapper">
                  <input className="text-input" type="text" defaultValue={user.ContactInformation.Email} name="Email" />
                  <svg className="check svg-icon-input-check">
                    <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                  </svg>
                </div>
              </div>
            </div>
            <div className="order-button-wrapper">
              <span className="back-link" onClick={this.prevStep}>
                <svg className="arrow svg-arrow-left"><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>
                <span className="text">Вернуться в корзину</span>
              </span>
              <button className="main-button dark" type="submit">Продолжить</button>
            </div>
          </form>
        ) : null}

        {type === 'wholesale' ? (
          <form className="form" onSubmit={this.handleProfile}>
            <div className="inputs two-columns-inputs">
              <div className="column">
                <div className="input-group">
                  <label className="label" htmlFor="#">Название компании</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.CompanyInformation.Name} name="CompanyName" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">КПП</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.CompanyInformation.KPP} name="CompanyKPP" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">ИНН</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.CompanyInformation.INN} name="CompanyINN" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Телефон компании</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.CompanyInformation.Phone} name="CompanyPhone" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <MediaQuery query='(min-width: 769px)'>
                  <div className="column-bottom">
                    <span className="back-link" onClick={this.prevStep}>
                      <svg className="arrow svg-arrow-left"><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>
                      <span className="text">Назад</span>
                    </span>
                  </div>
                </MediaQuery>
              </div>
              <div className="column">
                <div className="input-group">
                  <label className="label" htmlFor="#">Имя</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.ContactInformation.FirstName} name="FirstName" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Фамилия</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.ContactInformation.LastName} name="LastName" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Контактный телефон</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.ContactInformation.Phone} name="Phone" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Электронная почта</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" defaultValue={user.ContactInformation.Email} name="Email" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="order-button-wrapper">
                  <span className="back-link" onClick={this.prevStep}>
                    <svg className="arrow svg-arrow-left"><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>
                    <span className="text">Вернуться в корзину</span>
                  </span>
                  <button className="main-button dark" type="submit">Продолжить</button>
                </div>
              </div>
            </div>
          </form>
        ) : null}
      </div>
    );
  }

  renderMap() {
    const { deliveryOptions, deliveryRegion } = this.state;
    if (!deliveryRegion) return;


    const region = deliveryOptions.Regions.find(r => r.ID === deliveryRegion);
    const options = region.DeliveryOptions;
    const points = options.filter(o => o.Type === 'PVZ');

    const { ymaps } = window;

    ymaps.geocode(region.Name).then(
      (res) => {
        const center = res.geoObjects.get(0).geometry.getCoordinates();
        this.setState({
          map: new ymaps.Map('map', {
            center,
            zoom: 10,
            behaviors: ['drag'],
          }),
        }, () => {
          const { map } = this.state;
          points.forEach((point) => {
            const baloon = new ymaps.GeoObject({
              geometry: {
                type: 'Point',
                coordinates: [point.Latitude, point.Longitude],
              },
            });
            baloon.events.add('click', () => {
              this.setState({
                deliveryUUID: point.UUID,
              });
            });
            map.geoObjects.add(baloon);
          });
        });
      }
    );
  }

  renderDeliveryRetail() {
    const { deliveryOptions, deliveryRegion, deliveryUUID, deliveryType } = this.state;
    const { user } = this.props;

    let regionDeliveryOptions = [];
    let regionDeliveryTypes = [];
    let deliveryInfo = {};
    if (deliveryRegion) {
      regionDeliveryOptions = deliveryOptions.Regions.find(r => r.ID === deliveryRegion).DeliveryOptions;
      regionDeliveryTypes = removeDuplicatesBy(o => o.Type, regionDeliveryOptions);
      deliveryInfo = regionDeliveryOptions.find(o => o.UUID === deliveryUUID);
    }

    return (
      <div>
        <form className="form" name="update_profile" onSubmit={this.handleDelivery}>
          <div className="inputs two-columns-inputs">
            <div className="column">
              <div className="input-group small-margin">
                <label className="label" htmlFor="#">Город</label>
                <div className="select-wrapper">
                  <select className="select" value={deliveryRegion} onChange={this.updateDeliveryRegion}>
                    {deliveryOptions.Regions.map((region, index) => (
                      <option
                        value={region.ID}
                        key={index}
                      >{region.Name}</option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="input-group">
                <div className="info">Если вашего города нет в списке,<br />пожалуйста, обратитесь в <Link to="/conditions">службу доставки</Link>.</div>
              </div>
            </div>
            <div className="column">
              <div className="input-group">
                <label className="label" htmlFor="#">Способ доставки</label>
                <div className="select-wrapper">
                  <select className="select" value={deliveryType} onChange={this.updateDeliveryType}>
                    {regionDeliveryTypes.map((type, index) => (
                      <option
                        value={type.Type}
                        key={index}
                      >{deliveryOptions.DeliveryTypes.find(t => t.Id === type.Type).Name}</option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="input-group delivery-price">
                <div className="label">Стоимость доставки</div>
                <div className="price">
                  <span className="sum">
                    {deliveryUUID ? regionDeliveryOptions.find(o => o.UUID === deliveryUUID).Cost : null}
                  </span>
                  {' '}
                  <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                </div>
              </div>
            </div>
          </div>

          {deliveryType === 'RUSSIAN_POST' || deliveryType === 'COURIER_ONLINE' ? (
            <div className="inputs two-columns-inputs">
              <div className="column">
                <div className="input-group">
                  <label className="label" htmlFor="#">Индекс</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" name="PostCode" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Дом</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" name="HouseNumber" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Телефон</label>
                  <div className="input-wrapper">
                    <input className="text-input"  type="text" name="Phone" defaultValue={user.ContactInformation.Phone} />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="input-group">
                  <label className="label" htmlFor="#">Улица</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" name="Street" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Квартира</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" name="Room" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
                <div className="input-group">
                  <label className="label" htmlFor="#">Комментарий</label>
                  <div className="input-wrapper">
                    <input className="text-input" type="text" name="Description" />
                    <svg className="check svg-icon-input-check">
                      <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                    </svg>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div>
              <div className="pvz-selector">
                <div className="mobile-map-label">Пункт выдачи заказов</div>
                <div id="map" className="map" />
                <div className="pvz-info">
                  <div className="label map-label">Пункт выдачи заказов</div>
                  <div className="title">{deliveryInfo.Name}</div>
                  <div className="address">{deliveryInfo.Address}</div>
                  <div className="details">
                    {deliveryInfo.Metro ? (
                      <div className="row">
                        <div className="label">Метро</div>
                        <div className="value">{deliveryInfo.Metro}</div>
                      </div>
                    ) : null}
                    <div className="row">
                      <div className="label">Как найти?</div>
                      <div className="value how-to-go">{deliveryInfo.Info}</div>
                    </div>
                    <div className="row">
                      <div className="label">Часы работы</div>
                      <div className="value">{deliveryInfo.WorkTime}</div>
                    </div>
                    <div className="row">
                      <div className="label">Время доставки</div>
                      <div className="value">
                        {deliveryInfo.MinDeliveryDay !== deliveryInfo.MaxDeliveryDay ? (
                          <span>
                            {deliveryInfo.MinDeliveryDay}
                            {'—'}
                            {deliveryInfo.MaxDeliveryDay}
                            {' дней'}
                          </span>
                        ) : (
                          <span>
                            {deliveryInfo.MinDeliveryDay}
                            {' день'}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className="row">
                      <div className="label">Телефон</div>
                      <div className="value">{deliveryInfo.Phone}</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="inputs two-columns-inputs">
                <div className="column">
                  <div className="input-group">
                    <label className="label" htmlFor="#">Телефон</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" name="Phone" defaultValue={user.ContactInformation.Phone}/>
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                </div>
                <div className="column">
                  <div className="input-group">
                    <label className="label" htmlFor="#">Комментарий</label>
                    <div className="input-wrapper">
                      <input className="text-input" type="text" name="Description" />
                      <svg className="check svg-icon-input-check">
                        <use xlinkHref="/assets/images/sprite.svg#icon-input-check" />
                      </svg>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}

          <div className="order-button-wrapper">
            <span className="back-link" onClick={this.prevStep}>
              <svg className="arrow svg-arrow-left"><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>
              <span className="text">Вернуться к контактам</span>
            </span>
            <button className="main-button dark" type="submit">Продолжить</button>
          </div>
        </form>
      </div>
    );
  }

  renderDeliveryWholesale() {
    return (
      <div>
        <div className="order-info margin-top">
          <h2 className="info-heading">Условия отгрузки</h2>
          <div className="info-text">
            Для отгрузки товара Вам необходимо оплатить счет, <br />
            который будет отправлен на почту, после оформления заказа.
          </div>
          <h2 className="info-heading">Доставка в регионы</h2>
          <div className="info-text">
            Мы осуществляем доставку до терминалов<br />
            следующих транспортных компаний:<br />
            ПЭК, Деловые Линии, Желдор Экспедиция,<br />
            Байкал Сервис, Энергия.
          </div>
          <h2 className="info-heading">Доставка по Москве и МО</h2>
          <div className="info-text">
            Доставка по Москве и МО осуществляется самовывозом по адресу:<br />
            г. Москва, 119071,  Донская ул., 32<br />
            Проходная завода ЗАО «РН-ВЛАКРА»<br /><br />
            Чтобы забрать товар Вам необходимо за 1 час до отгрузки<br />
            заказать пропуск по телефону +7 (495) 215-07-01
          </div>
        </div>
        <div className="order-button-wrapper">
          <span className="back-link" onClick={this.prevStep}>
            <svg className="arrow svg-arrow-left"><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>
            <span className="text">Вернуться к контактам</span>
          </span>
          <button className="main-button dark" onClick={this.handleOrder}>Продолжить</button>
        </div>
      </div>
    );
  }

  renderPaymentMethod() {
    const { basket, user, errors } = this.props;
    const wholesale = user.AccountTYPE === 'wholesale';
    const AccountUUID = user.AccountUUID;

    const { deliveryOptions, deliveryRegion, deliveryUUID, paymentError, paymentOptions, method, orderNumber, orderUUID } = this.state;

    let regionDeliveryOptions = [];
    if (deliveryRegion) {
      regionDeliveryOptions = deliveryOptions.Regions.find(r => r.ID === deliveryRegion).DeliveryOptions;
    }

    basket.DeliveryCost = deliveryUUID ? regionDeliveryOptions.find(o => o.UUID === deliveryUUID).Cost : 0;

    const TotalSumm = basket.TotalSumm + basket.DeliveryCost;

    if (!basket.BasketList) {
      return <Preloader />;
    }

    let sumError = null;
    let basketErrors = false;
    if (errors.form && errors.data) {
      sumError = errors.data.find(e => e.name === 'TotalSumm');
      basketErrors = ~errors.form.indexOf('basket');
    }

    const options = {
      basket: basket,
      regionDeliveryOptions: regionDeliveryOptions,
      deliveryUUID: deliveryUUID,
      TotalSumm: TotalSumm,
      sumError: sumError,
      basketErrors: basketErrors,
      wholesale: wholesale,
      paymentOptions: paymentOptions,
      paymentError: paymentError,
      method: method,
      AccountUUID: AccountUUID,
      orderNumber: orderNumber,
      orderUUID: orderUUID,
      handleMethod: this.handleMethod,
      handlePayment: this.handlePayment,
      errors: errors,
    }

    return (
      <div>
        <MediaQuery query='(min-width: 769px)'>
          <BasketPaymentMethod {...options} />
        </MediaQuery>
        <MediaQuery query='(max-width: 768px)'>
          <BasketPaymentMethodMobile {...options} />
        </MediaQuery>
      </div>
    );
  }

  renderPayment() {
    return (
      <div>
        <div className="order-button-wrapper">
          <span className="back-link" onClick={this.prevStep}>
            <svg className="arrow svg-arrow-left"><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>
            <span className="text">Вернуться к доставке</span>
          </span>
          <button className="main-button dark" onClick={this.handleOrder}>Продолжить</button>
        </div>
      </div>
    );
  }

  renderSuccess() {
    const { user } = this.props;
    const orderNumber = this.props.location.query.orderNumber || this.state.orderNumber;
    return (
      <div className="order-info margin-top">
        <h2 className="info-heading">Ваш заказ № {orderNumber} принят в работу</h2>
        <div className="info-text">
          {user.AccountTYPE === 'wholesale'
            ? 'Счет для оплаты выслан на ваш электронный адрес.'
            : 'Подтверждение заказа выслано на Ваш электронный адрес.'
          }
        </div>
        {user.AccountTYPE == "wholesale" ? (
          <div className="info-text">
            Если у Вас возникнут какие-либо вопросы,<br />
            обращайтесь к нам по телефону:<br />
            <span>+7 (495) 215-07-01</span>  (ежедневно с 9:00 до 18:00)<br />
            или по электронной почте: <a className="link" href="mailto:c@noe.ru">c@noe.ru</a>
          </div>
          ) : (
          <div className="info-text">
            Если у Вас возникнут какие-либо вопросы,<br />
            обращайтесь к нам по бесплатному телефону:<br />
            <span>+7 (495) 215-07-01</span>  (ежедневно с 9:00 до 18:00)<br />
            или по электронной почте: <a className="link" href="mailto:zakaz@canoe.ru">zakaz@canoe.ru</a>
          </div>
          )
        }
        <Link to="/orders" className="main-button dark mobile-link">Ваши заказы</Link>
      </div>
    );
  }

  render() {

    const { app } = this.props;

    let step;
    switch (this.state.step) {
      case 1:
        step = this.renderBasket();
        break;
      case 2:
        step = this.renderProfile();
        break;
      case 3:
        if (app.wholesaleMode) {
          step = this.renderDeliveryWholesale();
        } else {
          step = this.renderDeliveryRetail();
        }
        break;
      case 4:
        step = this.renderPaymentMethod();
        break;
      case 5:
        step = this.renderSuccess();
        break;
      default:
        break;
    }

    return (
      <section className="page-content basket-page">
        <div className="content-wrapper">
          {this.renderStepsThermometer()}
          {step}
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  const { app, basket, user, errors } = state;
  return {
    app,
    basket,
    user,
    errors,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Basket);
