import React, { Component } from 'react';
import MediaQuery from 'react-responsive';

import moment from 'moment';
import ru from 'moment/locale/ru';
moment.updateLocale('ru', ru);

const $ = window.jQuery;

export default class About extends Component {
  expand() {
    $('.collapsed-content').addClass('expanded');
    $('.expand-btn-container').addClass('hidden');
  }

  play(){
    document.getElementById('video').play();
    document.getElementById('playBtn').classList.add('hidden');
  }

  render() {
    return (
      <section className="page-content legend-page">
        <div className="content-wrapper">
          <MediaQuery query='(max-width: 768px)'>
            <div>
              <div className="technologie-header full-width" style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/mobile/Lookbook-21.jpg)',
                }}
              >
              </div>
              <div className="tech-name">
                <p className="title">Легенда<br/>Canoe</p>
                <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              </div>
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div
              className="banner-with-centered-content full-width"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/Lookbook-21.jpg)',
              }}
            >
              <div className="content">
                <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
                <div className="title">Легенда Canoe</div>
                <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              </div>
            </div>
          </MediaQuery>

          <div className="text-with-decorations text-1">
            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            <div className="text">
              Многовековая традиция стала
              <br />
              вечной классикой
            </div>
            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            <div className="subtext">
              Нас восхищает работа мастера, преданного своему делу: кропотливый труд, <span className="text-highlight">любовь к деталям</span> и <span className="text-highlight">особый подход</span> к каждому изделию. Стремление к совершенству — это то, что мотивирует нас становиться лучше с каждым днём.
              <br />
              <br />
              На протяжении столетий создание головных уборов было сложным искусством и уважаемым ремеслом.
              Навыки работы с материалом оттачивались долгие годы, техника совершенствовалась из поколения в поколение.
              Шляпы были не просто сезонным аксессуаром, а ценным предметом гардероба,
              к качеству которого предъявлялись самые высокие требования.
            </div>
          </div>

          <div className="full-width b-legend__machine">
            <div className="b-legend__machine__title">
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <div className="b-legend__machine__title__text">Новый рассвет</div>

            </div>
            <div className="b-legend__machine__videowrapper b-shift-block" style={{ backgroundColor: '#8191B7' }}>
              <MediaQuery query='(max-width: 768px)'>
                <div className="b-shift-block__shifter b-video legend">
                  <video id="video" src="//media.canoe.ru/videos/legend.mp4" poster="/assets/images/legend.jpg" loop />
                  <svg id="playBtn" className="icon-play" onClick={this.play}><use xlinkHref="/assets/images/sprite.svg#icon-play" /></svg>
                </div>
              </MediaQuery>
              <MediaQuery query='(min-width: 769px)'>
                <div className="b-shift-block__shifter b-video legend">
                  <video src="//media.canoe.ru/videos/legend.mp4" autoPlay loop />
                </div>
              </MediaQuery>
              <div className="b-legend__machine__texts-header">
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                <div className="text">Вдохнуть новую жизнь<br />в традиционный ход вещей</div>
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              </div>
              <div className="b-legend__machine__texts content-wrapper">
                <div className="collapsed-content">
                  <div className="b-legend__machine__text">
                    Представьте старинный шляпный магазин в самом центре Москвы, на
                    Тверской улице, чья история восходит к 1890 году. Котелки, трилби, широкополые шляпы, кокетливые шляпки
                    клош, фетровые кепки, — самые модные головные уборы того времени красовались на полках и терпеливо ждали
                    своих покупателей.
                    <br />
                    <br />
                    В 1996 году такое почетное шляпное дело попало к нам в руки. Время наложило свой отпечаток: ассортимент
                    больше соответствовал духу прошлого, да и сама бизнес-структура сильно устарела.
                  </div>
                  <div className="b-legend__machine__text">
                    Нами овладел дух предпринимательства и горячее желание подарить многолетнему бизнесу новую жизнь. Мы поняли, что здесь нужен совершенно иной подход, и вдохновенно принялись за дело! Отправились на фабрики, вникли во все тонкости производства, обратились к промышленным и графическим дизайнерам и стали вместе создавать новое будущее.
                    <br />
                    <br />
                    Для нас не существовало примеров для подражания и правил ведения бизнеса — мы прокладывали свой путь с нуля, искали необычные эффективные решения и постепенно двигались вперёд. Так мы работаем и сегодня.
                  </div>
                </div>
                <div className="expand-btn-container">
                  <span onClick={this.expand} className="expand-btn"></span>
                  <i className="gradient" />
                </div>
              </div>
            </div>
          </div>

          <div className="b-end-banner b-legend__metaphora full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/shutterstock_212917096.jpg)' }}>
            <div className="content-wrapper">
              <div className="text-with-decorations b-end-banner__content">
                <div className="suptitle">Метафора качества</div>
                <div className="text">Добиться успеха и быть готовым<br />к новым испытаниям</div>

                <MediaQuery query='(max-width: 768px)'>
                  <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                </MediaQuery>

                <MediaQuery query='(min-width: 769px)'>
                  <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
                </MediaQuery>

                <div className="subtext" style={{ width: '560px' }}>
                  Нам удалось сохранить тот безграничный энтузиазм,
                  который 20 лет назад вдохновил нас сделать первый шаг на пути к мечте.
                  Каждый день мы бросаем вызов самим себе в поисках необычных идей и новаторских решений.
                </div>
              </div>
            </div>
          </div>

          <div className="b-legend__house">
            <div className="b-legend__house__icon">
              <img src="/assets/images/brands/house.svg" alt="house illustration" />
            </div>
            <div className="b-legend__house__content">
              Мы продолжаем развивать российское традиционное вязальное и прядильное производства, чтобы сохранить и преумножить богатый опыт технологов и мастеров. Именно они превращают наши изделия в настоящие произведения искусства. И мы на сто процентов уверены:
              <br />
              <span className="text-highlight">Canoe — это качество, проверенное временем.</span>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div>
              <div
                className="banner-with-centered-content full-width"
                style={{
                  backgroundImage: 'url(//media.canoe.ru/images/brand/mobile/Pattern-04.jpg)',
                }}
              >
                <div className="content">
                  <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                  <div className="text with-br">Canoe —<br />это образ-метафора</div>
                  <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                </div>
              </div>
              <div className="mobile-end-text">
                Он вбирает в себя жажду приключений, готовность рисковать и исследовать новые направления. С Canoe можно плыть против течения. В то же время, это символ нашего глубокого уважения к природе, которая вдохновляет нас на ежедневную схватку за безупречность.
              </div>
            </div>
          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <div
              className="banner-with-centered-content full-width"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/Pattern-04.jpg)',
              }}
            >
              <div className="content">
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                <div className="text">Canoe — это образ-метафора</div>
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                <div className="subtext" style={{ width: '560px' }}>
                  Он вбирает в себя жажду приключений, готовность рисковать и исследовать новые направления. С Canoe можно плыть против течения. В то же время, это символ нашего глубокого уважения к природе, которая вдохновляет нас на ежедневную схватку за безупречность.
                </div>
              </div>
            </div>
          </MediaQuery>

          <div className="banner-with-centered-content short white full-width b-legend__end">
            <div className="content">
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <div className="text">Сотканы из духа свободы</div>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
