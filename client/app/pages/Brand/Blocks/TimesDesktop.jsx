import React from 'react';
import { Link } from 'react-router';


const TimesDesktop = () => (
  <div className="philosophy-times">
    <div className="philosophy-times__section">
      <div className="philosophy-times__section__side darkblue">
        <div className="philosophy-times__title">Традиционные ценности</div>
        <svg className="philosophy-times__decoration decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
        <div className="philosophy-times__text">
          Многовековое ткаческое, прядильное,
          <br />
          вязальное ремесло и проверенный временем
          <br />
          опыт шляпных мастеров, передающиеся
          <br />
          из поколения в поколение.
        </div>
      </div>
      <div
        className="philosophy-times__section__side image"
        style={{
          backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/Stocksy_txp7204d1dda08100_Medium_503754.jpg)',
        }}
      />
    </div>
    <div className="philosophy-times__section">
      <div
        className="philosophy-times__section__side image"
        style={{
          backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/iStock_7706240_XLARGE.jpg)',
        }}
      />
      <div className="philosophy-times__section__side blue">
        <div className="philosophy-times__title">Новые перспективы</div>
        <svg className="philosophy-times__decoration decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
        <div className="philosophy-times__text">
          Отточенные до совершенства
          <br />
          новейшие технологии,
          <br />
          инновационные материалы и самые
          <br />
          современные тенденции моды.
        </div>
      </div>
    </div>
    <div className="philosophy-times__content">
      <div className="philosophy-times__content__title">
        Мы не ищем лёгких путей — мы делаем так, как считаем нужным.
        <br />
        Для этого необходимы смелость и твёрдая вера в своё дело
      </div>
      <svg className="philosophy-times__content__decoration decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
      <div className="philosophy-times__content__text">
        Мы верим в огромный потенциал слияния ремесленного производства и новаторских идей. Мы выходим за рамки привычных клише, чтобы найти лучшие решения. Мы вдохновляемся примерами из всех областей жизни, объединяем творческие умы и увлеченных исследователей, чтобы сделать невозможное возможным.
      </div>
    </div>
  </div>
)

export default TimesDesktop;
