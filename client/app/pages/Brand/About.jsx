import React, { Component } from 'react';
import MediaQuery from 'react-responsive';
import MobileGallery from 'app/components/MobileGallery';
import Swiper from 'react-id-swiper';

import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';
import { Link } from 'react-router';

import moment from 'moment';
import ru from 'moment/locale/ru';
moment.updateLocale('ru', ru);

const $ = window.jQuery;

export default class About extends Component {

  onRefresh(inst) {
    const scroller = $(inst.scroller);
    scroller.width(scroller.find(".b-materials__item").length * 142);
  }

  render() {
    const FactsGallery = [
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/about/mobile/fact1.jpg"
        },
        Title: "14M<span className='plus'>+</span>",
        Text: "человек носят Canoe"
      },
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/about/mobile/fact2.jpg"
        },
        Title: "1М",
        Text: "Изделий каждый год"
      },
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/about/mobile/fact3.jpg"
        },
        Title: "2К<span className='plus'>+</span>",
        Text: "Новых моделей в год"
      },
    ]

    const MaterialsGallery = [
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/about/step1.svg"
        },
        Text: "Тщательный отбор натуральных материалов"
      },
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/about/step2.svg"
        },
        Text: "Новые технологии производства пряжи"
      },
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/about/step3.svg"
        },
        Text: "Разработка моделей"
      },
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/about/step4.svg"
        },
        Text: "Тестирование прототипов"
      },
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/about/step5.svg"
        },
        Text: "Контроль производства"
      },
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/about/step6.svg"
        },
        Text: "Обработка готовых изделий"
      },
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/about/step7.svg"
        },
        Text: "Контроль качества"
      },
    ]

    const options = {
      scrollX: true,
      scrollY: false,
      // snap: '.item',
      // probeType: 3,
      // bounce: false,
      // bounceTime: 0,
      // deceleration: 0,
      // momentum: false,
      // mouseWheel: true,
      click: true,
      preventDefault: false,
      eventPassthrough: true,
    }

    return (
      <section className="page-content about-page">
        <div className="content-wrapper">
          <MediaQuery query='(max-width: 768px)'>
            <div>
              <div className="technologie-header full-width" style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/about/mobile/Lookbook-05.jpg)',
                }}
              >
              </div>
              <div className="tech-name">
                <p className="title">Canoe<br />сегодня</p>
              </div>
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div
              className="banner-with-centered-content full-width about-page__header"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/about/Lookbook-05.jpg)',
              }}
            >
              <div className="content">
                <svg className="decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
                <div className="about-page__header__title title">Canoe сегодня</div>
                <svg className="decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              </div>
            </div>
          </MediaQuery>

          <div className="about-page__history">
            <svg className="about-page__history__decoration decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            <div
              className="about-page__history__icon"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/about/logo.svg)',
              }}
            />
            <div className="about-page__history__title">20 лет опыта</div>
            <div className="about-page__history__description">
              Canoe появился на российском рынке в 1996 году.<br />
              С этого момента мы каждый день  открываем новые,<br />
              смелые и перспективные методы работы.
            </div>


            <MediaQuery query='(max-width: 768px)'>
              <MobileGallery name="facts" content={FactsGallery} />
            </MediaQuery>


            <MediaQuery query='(min-width: 769px)'>
              <div>
                <div
                  className="about-page__history__photo"
                  style={{
                    backgroundImage: 'url(//media.canoe.ru/images/brand/about/Y3A9931.jpg)',
                  }}
                />
                <div className="about-page__history__stats">
                  <div className="about-page__history__stat">
                    <div className="about-page__history__stat__value">14M<span className="plus">+</span></div>
                    <div className="about-page__history__stat__title">
                      человек носят<br />
                      головные уборы<br />
                      Canoe
                    </div>
                  </div>
                  <div className="about-page__history__stat">
                    <div className="about-page__history__stat__value">1M</div>
                    <div className="about-page__history__stat__title">
                      изделий<br />
                      каждый<br />
                      год
                    </div>
                  </div>
                  <div className="about-page__history__stat">
                    <div className="about-page__history__stat__value">2K<span className="plus">+</span></div>
                    <div className="about-page__history__stat__title">
                      новых<br />
                      моделей<br />
                      в год
                    </div>
                  </div>
                </div>
              </div>
            </MediaQuery>

            <div className="about-page__history__house">
              <div
                className="about-page__history__house__icon"
                style={{
                  backgroundImage: 'url(//media.canoe.ru/images/brand/about/house.svg)',
                }}
              />
              <div className="about-page__history__house__title">Российское производство</div>
              <div className="about-page__history__house__description">
                Наши продукты разрабатываются и производятся
                на подмосковных фабриках.
              </div>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div className="mobile-about-collections">
              <div
                className="banner-with-centered-content full-width"
                style={{
                  backgroundImage: 'url(//media.canoe.ru/images/brand/about/mobile/Lookbook-22.jpg)',
              }} />

              <div className="collections-wrapper full-width" style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/about/mobile/Pattern-08.jpg)',
              }}>
                <Swiper
                  slidesPerView={1}
                  pagination=".pagination"
                  paginationType="bullets"
                  paginationClickable={true}
                  paginationElement="span"
                  paginationHide={false}
                >
                  <div className="card">
                    <div>
                      <p className="title">Premium</p>
                      <p>Поистине вечная классика. Самые благородные материалы и последние тенденции</p>
                    </div>
                  </div>
                  <div className="card">
                    <div>
                      <p className="title">Neo Classic</p>
                      <p>Свежая интерпретация<br />классических моделей</p>
                    </div>
                  </div>
                  <div className="card">
                    <div>
                      <p className="title">Street Fashion</p>
                      <p>Выразительная и эмоциональная коллекция, излучающая любовь к жизни</p>
                    </div>
                  </div>
                  <div className="card">
                    <div>
                      <p className="title">Young</p>
                      <p>Подростковая коллекция празднует независимость, творчество и бунт</p>
                    </div>
                  </div>
                </Swiper>
              </div>

            </div>

            <div className="full-width b-materials">
              <div className="content-wrapper">
                <div className="text-with-decorations b-materials__header">
                  <div className="text">Каждое изделие Canoe проходит 7 этапов контроля</div>
                </div>
                <ReactIScroll
                  className="b-materials__list b-iscroll"
                  onRefresh={(inst) => this.onRefresh(inst)}
                  options={options}
                  iScroll={iScroll}>

                  <div className="b-iscroll-container">
                    {MaterialsGallery.map((material, index) =>
                      <Link className="b-materials__item" to={material.URL} key={index}>
                        <span className="b-materials__item__icon" style={{ backgroundImage: `url(${material.Image.Url})` }} />
                        <i className="arrow"></i>
                        <span className="b-materials__item__title">{material.Text}</span>
                      </Link>
                    )}
                  </div>
                </ReactIScroll>
              </div>
            </div>

          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <div
              className="about-page__collections full-width"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/about/Lookbook-22.jpg)',
              }}>
              <div className="about-page__collections__content content-wrapper">
                <svg className="about-page__collections__decoration top decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                <div className="about-page__collections__title">Наши Коллекции</div>
                <svg className="about-page__collections__decoration bottom decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                <div className="about-page__collections__list">
                  <div className="about-page__collections__list__item">
                    <span className="about-page__collections__list__item__title">Premium</span>
                    <div className="about-page__collections__list__item__description">
                      Поистине вечная классика.<br />
                      Самые благородные материалы<br />
                      и последние тенденции
                    </div>
                  </div>
                  <div className="about-page__collections__list__item">
                    <span className="about-page__collections__list__item__title">Neo Classic</span>
                    <div className="about-page__collections__list__item__description">
                      Свежая интерпретация<br />
                      классических моделей
                    </div>
                  </div>
                  <div className="about-page__collections__list__item">
                    <span className="about-page__collections__list__item__title">Street Fashion</span>
                    <div className="about-page__collections__list__item__description">
                      Выразительная и эмоциональная<br />
                      коллекция, излучающая<br />
                      любовь к жизни
                    </div>
                  </div>
                  <div className="about-page__collections__list__item">
                    <span className="about-page__collections__list__item__title">Young</span>
                    <div className="about-page__collections__list__item__description">
                      Подростковая коллекция<br />
                      празднует независимость,<br />
                      творчество и бунт
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div
              className="about-page__quality full-width"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/about/Pattern-08.jpg)',
              }}
            >
              <div className="about-page__quality__content content-wrapper">
                <div className="about-page__quality__title">Canoe — это метафора качества</div>
                <div className="about-page__quality__description">Каждое изделие Canoe проходит 7 этапов контроля</div>
                <div className="about-page__quality__steps">
                  <div className="about-page__quality__steps__step">
                    <div
                      className="about-page__quality__steps__step__image"
                      style={{
                        backgroundImage: 'url(//media.canoe.ru/images/brand/about/step1.svg)',
                      }}
                    />
                    <div className="about-page__quality__steps__step__title">тщательный отбор<br />натуральных материалов</div>
                  </div>
                  <div
                    className="about-page__quality__steps__arrow"
                    style={{
                      backgroundImage: 'url(//media.canoe.ru/images/brand/about/arrow.svg)',
                    }}
                  />
                  <div className="about-page__quality__steps__step">
                    <div
                      className="about-page__quality__steps__step__image"
                      style={{
                        backgroundImage: 'url(//media.canoe.ru/images/brand/about/step2.svg)',
                      }}
                    />
                    <div className="about-page__quality__steps__step__title">Новые технологии<br />производства пряжи</div>
                  </div>
                  <div
                    className="about-page__quality__steps__arrow"
                    style={{
                      backgroundImage: 'url(//media.canoe.ru/images/brand/about/arrow.svg)',
                    }}
                  />
                  <div className="about-page__quality__steps__step">
                    <div
                      className="about-page__quality__steps__step__image"
                      style={{
                        backgroundImage: 'url(//media.canoe.ru/images/brand/about/step3.svg)',
                      }}
                    />
                    <div className="about-page__quality__steps__step__title">Разработка<br />моделей</div>
                  </div>
                  <div
                    className="about-page__quality__steps__arrow"
                    style={{
                      backgroundImage: 'url(//media.canoe.ru/images/brand/about/arrow.svg)',
                    }}
                  />
                  <div className="about-page__quality__steps__step">
                    <div
                      className="about-page__quality__steps__step__image"
                      style={{
                        backgroundImage: 'url(//media.canoe.ru/images/brand/about/step4.svg)',
                      }}
                    />
                    <div className="about-page__quality__steps__step__title">тестирование<br />прототипов</div>
                  </div>
                  <div
                    className="about-page__quality__steps__arrow"
                    style={{
                      backgroundImage: 'url(//media.canoe.ru/images/brand/about/arrow.svg)',
                    }}
                  />
                  <div className="about-page__quality__steps__step">
                    <div
                      className="about-page__quality__steps__step__image"
                      style={{
                        backgroundImage: 'url(//media.canoe.ru/images/brand/about/step5.svg)',
                      }}
                    />
                    <div className="about-page__quality__steps__step__title">Контроль<br />производства</div>
                  </div>
                  <div
                    className="about-page__quality__steps__arrow"
                    style={{
                      backgroundImage: 'url(//media.canoe.ru/images/brand/about/arrow.svg)',
                    }}
                  />
                  <div className="about-page__quality__steps__step">
                    <div
                      className="about-page__quality__steps__step__image"
                      style={{
                        backgroundImage: 'url(//media.canoe.ru/images/brand/about/step6.svg)',
                      }}
                    />
                    <div className="about-page__quality__steps__step__title">Обработка<br />готовых изделий</div>
                  </div>
                  <div
                    className="about-page__quality__steps__arrow"
                    style={{
                      backgroundImage: 'url(//media.canoe.ru/images/brand/about/arrow.svg)',
                    }}
                  />
                  <div className="about-page__quality__steps__step">
                    <div
                      className="about-page__quality__steps__step__image"
                      style={{
                        backgroundImage: 'url(//media.canoe.ru/images/brand/about/step7.svg)',
                      }}
                    />
                    <div className="about-page__quality__steps__step__title">Контроль<br />качества</div>
                  </div>
                </div>
              </div>
            </div>
          </MediaQuery>

          <div className="about-page__partner full-width">
            <div className="content-wrapper">
              <svg className="about-page__partner__decoration decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              <div className="about-page__partner__title">Партнёр на долгие годы</div>
              <div className="about-page__partner__description">
                Мы имеем многолетний опыт работы с клиентами
                и используем индивидуальный подход к каждому из них.
              </div>
              <div className="about-page__partner__list">
                <div className="about-page__partner__list__item"><span>•</span> упрощенная процедура заказа</div>
                <div className="about-page__partner__list__item"><span>•</span> консультирование по вопросам формирования коллекций</div>
                <div className="about-page__partner__list__item"><span>•</span> сопровождение процесса на всех этапах</div>
                <div className="about-page__partner__list__item"><span>•</span> маркетинговая поддержка и медиа-материалы</div>
              </div>
            </div>
          </div>

          <div className="about-page__malls">
            <svg className="about-page__malls__decoration decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
            <div className="about-page__malls__description">
              Сегодня Canoe один из самых успешных и быстроразвивающихся брендов. Наша продукция представлена в более чем 2000 торговых сетях.
            </div>
            <div className="about-page__malls__types">
              <div className="about-page__malls__type offline">
                <div
                  className="about-page__malls__type__image offline"
                  style={{
                    backgroundImage: 'url(//media.canoe.ru/images/brand/about/mall_offline.svg)',
                  }}
                />
                <div className="about-page__malls__type__stores">
                  <div className="about-page__malls__type__stores__title">Offline</div>
                  <div className="about-page__malls__type__stores__list">
                    <div className="about-page__malls__type__stores__list__column">
                      <a href="http://www.stockmann.ru/moscow/" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Stockmann</a>
                      <a href="http://www.intersport.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Intersport</a>
                      <a href="http://www.debenhams.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Debenhams</a>
                      <a href="https://www.metro-cc.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Metro</a>
                      <a href="http://www.championnet.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Чемпион</a>
                      <a href="https://mega.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Мега</a>
                    </div>
                    <div className="about-page__malls__type__stores__list__column">
                      <a href="https://snowqueen.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Снежная Королева</a>
                      <a href="http://topliga.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Высшая Лига</a>
                      <a href="http://www.5karmanov.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">5 Карманов</a>
                      <a href="http://jscasual.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Джинсовая Симфония</a>
                      <a href="http://www.detmir.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Детский мир</a>
                      <a href="http://respect-shoes.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Респект</a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="about-page__malls__type online">
                <div
                  className="about-page__malls__type__image online"
                  style={{
                    backgroundImage: 'url(//media.canoe.ru/images/brand/about/mall_online.svg)',
                  }}
                />
                <div className="about-page__malls__type__stores">
                  <div className="about-page__malls__type__stores__title">Online</div>
                  <div className="about-page__malls__type__stores__list">
                    <div className="about-page__malls__type__stores__list__column">
                      <a href="http://www.quelle.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Quelle</a>
                      <a href="http://www.lamoda.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Lamoda</a>
                      <a href="https://www.wildberries.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Wildberries</a>
                      <a href="http://www.ozon.ru" target="_blank" rel="noopener noreferrer" className="about-page__malls__type__stores__list__item">Ozon</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="about-page__business full-width">
            <div className="content-wrapper">
              <div className="about-page__business__title">БОЛЬШЕ, ЧЕМ БИЗНЕС</div>
              <svg className="about-page__business__decoration decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              <div className="about-page__business__description">Мы помогаем нашим партнерам удерживать конкурентноспособное преимущество,
                предлагая инновационные, высокодоходные продукты.</div>
              <div className="about-page__business__stats">
                <div className="about-page__business__stat left">
                  <div className="about-page__business__stat__value">35%</div>
                  <div className="about-page__business__stat__title">Ежегодный рост компании</div>
                </div>
                <div className="about-page__business__stat right">
                  <div className="about-page__business__stat__value">2.5</div>
                  <div className="about-page__business__stat__title">Рентабельность</div>
                  <div className="about-page__business__stat__subtitle">Прибыльная товарная категория, торговая наценка 2.5</div>
                </div>
              </div>
            </div>
          </div>

          <div
            className="banner-with-centered-content full-width about-page"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/about/Lookbook-24.jpg)',
            }}
          />

          <div className="about-page__contacts">
            <div className="about-page__contacts__title">Начать с нами работу легко</div>
            <svg className="about-page__contacts__decoration decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
            <div className="about-page__contacts__phone">+7 (495) 215-07-01</div>
            <div className="about-page__contacts__phone-hint">
              по будням с 10:00 до 18:00
              <div style={{ fontStyle: 'italic' }}>(бесплатно по России)</div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
