import React, { Component } from 'react';
import Swiper from 'react-id-swiper';
import MediaQuery from 'react-responsive';

import TimesDesktop from './Blocks/TimesDesktop';
import MobileGallery from 'app/components/MobileGallery';

import moment from 'moment';
import ru from 'moment/locale/ru';
moment.updateLocale('ru', ru);


export default class Philosophy extends Component {
  render() {
    const PhilosophyGallery = [
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/philosophy/mobile/Stocksy_txp7204d1dda08100_Medium_503754.jpg"
        },
        Title: "Традиционные ценности",
        Text: "Многовековое ткаческое, прядильное, вязальное ремесло и проверенный временем опыт шляпных мастеров, передающиеся из поколения в поколение."
      },
      {
        Image: {
          Url: "//media.canoe.ru/images/brand/philosophy/mobile/iStock_7706240_XLARGE.jpg"
        },
        Title: "Новые<br />перспективы",
        Text: "Отточенные до совершенства новейшие технологии, инновационные материалы и самые современные тенденции моды."
      },
    ];

    return (
      <section className="page-content philosophy-page">
        <div className="content-wrapper">
          <MediaQuery query='(max-width: 768px)'>
            <div>
              <div className="technologie-header full-width" style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/mobile/Lookbook-07.jpg)',
                }}
              >
              </div>
              <div className="tech-name">
                <p className="title">Философия<br/>Canoe</p>
                <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              </div>
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div
              className="banner-with-centered-content full-width"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/Lookbook-07.jpg)',
              }}
            >
              <div className="content">
                <svg className="decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
                <div className="title">Философия Canoe</div>
                <svg className="decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              </div>
            </div>
          </MediaQuery>

          <div
            className="philosophy-dna full-width"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/Pattern-03.jpg)',
            }}
          >
            <div className="philosophy-dna__content">
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <div className="philosophy-dna__title">Наша ДНК</div>
              <div className="philosophy-dna__text">
                Canoe объединяет традиционное
                <br />
                искусство создания головных уборов
                <br />
                с новейшими разработками, технологиями
                <br />
                и текстильными трендами.
              </div>
              <svg className="decoration svg-decoration-2 philosophy-dna__decoration2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div>
              <MobileGallery name="philosophy" colors={true} content={PhilosophyGallery} />
              <div className="philosophy-times__content">
                <svg className="philosophy-times__content__decoration decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
                <div className="philosophy-times__content__title">
                  Мы не ищем лёгких путей — мы делаем так, как считаем нужным.
                  <br />
                  Для этого необходимы смелость и твёрдая вера в своё дело
                </div>
                <div className="philosophy-times__content__text">
                  Мы верим в огромный потенциал слияния ремесленного производства и новаторских идей. Мы выходим за рамки привычных клише, чтобы найти лучшие решения. Мы вдохновляемся примерами из всех областей жизни, объединяем творческие умы и увлеченных исследователей, чтобы сделать невозможное возможным.
                </div>
              </div>
            </div>
          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <TimesDesktop />
          </MediaQuery>

          <div
            className="philosophy-nature full-width"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/iStock_35298864_XXXLARGE.jpg)',
            }}
          >
            <div className="philosophy-nature__title">Наша природа</div>
            <svg className="decoration svg-decoration-1 philosophy-nature__decoration flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
            <div className="philosophy-nature__text">
              Открыть новые земли можно,
              <br />
              только потеряв родной берег из виду
            </div>
          </div>

          <div className="philosophy-create">
            <svg className="decoration svg-decoration-1 philosophy-nature__decoration flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
            <div className="philosophy-create__title">
              Создать что-то по-настоящему новое можно,
              <br />
              только если вы готовы сойти с проторенной дороги
            </div>
            <div className="philosophy-create__text">
              Этим правилом мы руководствуемся, когда разрабатываем продукты
              <br />
              с невиданными свойствами и совершаем технологические прорывы. Сегодня без
              <br />
              них наши изделия невозможно представить: невидимые швы, непромокаемая
              <br />
              шерсть, дышащие материалы, защита от УФ-лучей, стрейч-твид, бейсболки
              <br />
              с пробковыми козырьками, — мы создаем практичные и по-настоящему красивые
              <br />
              решения на все случаи жизни.
            </div>
          </div>

          <div className="philosophy-photos">
            <Swiper
              spaceBetween={40}
              slidesPerView={2}
              loop
              navigation={{
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev"
              }}
            >
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/arcticfox_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/cotton_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/fauxfur_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/fleece_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/lightweight_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/lurex_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/muline_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/polyester_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/rabbit_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/raccoon_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/segmented_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/sequins_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/superkid_square.jpg)' }} />
              <div className="philosophy-photos__item" style={{ backgroundImage: 'url(//media.canoe.ru/images/brand/philosophy/square/viskose_square.jpg)' }} />
            </Swiper>
          </div>

          <div className="banner-with-centered-content short white full-width b-legend__end">
            <div className="content">
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <div className="text">Сотканы из духа свободы</div>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
