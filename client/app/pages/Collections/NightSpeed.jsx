import React, { Component } from 'react';
import { Link } from 'react-router';

import classNames from 'classnames';
import MediaQuery from 'react-responsive';

import initScrollReveal from 'app/animations/artProject';
import NightSpeedDesktop from './Blocks/NightSpeedDesktop';
import NightSpeedMobile from './Blocks/NightSpeedMobile';

const $ = window.jQuery;


export default class NightSpeed extends Component {
  componentDidMount() {
    initScrollReveal();

    let header = document.getElementsByClassName('collection-header')[0];
    let imageToPreload = new Image();

    const placeBanner = () => {
      header.style.backgroundImage = 'url(' + header.dataset.image + ')';
      imageToPreload.remove();
    }

    if(!document.getElementById('appContainer').classList.contains('mobile')){
      imageToPreload.src = header.dataset.image;
      imageToPreload.id = 'temp';
      imageToPreload.addEventListener("load", () => {
        placeBanner();
      });
    } else {
      placeBanner();
    }

    if(window.location.hash === 'scroll' || window.location.hash === '#scroll'){
      setTimeout(this.scrollToContent, 300);
    }
  }

  scrollToContent() {
    $("html, body").animate({scrollTop: $('.text-with-decorations').eq(0).offset().top - 70}, 400);
  }

  render() {

    return (
      <section className="page-content campaign-page collections-2018">
        <div className="content-wrapper">
          <div
            className="collection-header full-width"
            data-image="//media.canoe.ru/images/brand/2018/03.jpg"
          >
            <div className="heading">
              <div className="name">Выбери <br />свое приключение<br /><span>|</span></div>
            </div>
            <div className="page-link left" onClick={this.scrollToContent.bind(this)}>
              <img src="//media.canoe.ru/images/brand/2018/02.jpg" />
              <i className="button">Смотреть</i>
              <span>Наперегонки<br />с ночью</span>
            </div>
            <div className="page-link right">
              <Link to="/collections/horizons#scroll">
                <span>Новые<br />горизонты</span>
                <i className="button">Смотреть</i>
                <img src="//media.canoe.ru/images/brand/2018/01.jpg" />
              </Link>
            </div>

            <div className="mobile-page-links">
              <ul>
                <li>Наперегонки<br />с ночью</li>
                <li>
                  <Link to="/collections/horizons">Новые<br />горизонты</Link>
                </li>
              </ul>
              <div className="page-img">
                <img src="//media.canoe.ru/images/brand/2018/02.jpg" />
              </div>
            </div>

            <i className="collections-bg collections-bg-3" />
          </div>

          <div className="text-with-decorations">
            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            <div className="text">Наперегонки с ночью</div>
            <div className="subtext">Каждые 24 часа — это новый виток<br />твоего приключения.</div>
            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <NightSpeedMobile />
          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <NightSpeedDesktop />
          </MediaQuery>
        </div>
      </section>
    );
  }
}
