import React, { Component } from 'react';
import { Link } from 'react-router';

import classNames from 'classnames';
import MediaQuery from 'react-responsive';

import initScrollReveal from 'app/animations/artProject';
import ArtProjectDesktop from './Blocks/ArtProjectDesktop';
import ArtProjectMobile from './Blocks/ArtProjectMobile';


export default class ArtProject extends Component {
  state = {
    headerLoaded: false
  }

  componentDidMount() {
    initScrollReveal();

    let header = document.getElementsByClassName('collection-header')[0];
    let imageToPreload = new Image();

    const placeBanner = () => {
      header.style.backgroundImage = 'url(' + header.dataset.image + ')';
      imageToPreload.remove();
      this.setState({headerLoaded: true});
    }

    if(!document.getElementById('appContainer').classList.contains('mobile')){
      imageToPreload.src = header.dataset.image;
      imageToPreload.id = 'temp';
      imageToPreload.addEventListener("load", () => {
        placeBanner();
      });
    } else {
      placeBanner();
    }
  }

  render() {
    return (
      <section className="page-content campaign-page">
        <div className="content-wrapper">
          <div
            className={classNames("collection-header full-width", {'showed': this.state.headerLoaded})}
            data-image="//media.canoe.ru/images/brand/campaign/header.jpg"
          >
            <div className="heading">
              <div className="name">Творчество свободы</div>
            </div>
          </div>

          <div className="text-with-decorations">
            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            <div className="text">Творчество — это<br />суть жизни</div>
            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <ArtProjectMobile />
          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <ArtProjectDesktop />
          </MediaQuery>
        </div>
      </section>
    );
  }
}
