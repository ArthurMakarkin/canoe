import React, { Component } from 'react';
import Swiper from 'react-id-swiper';
import { Link } from 'react-router';

import MediaQuery from 'react-responsive';
import MobileGallery from 'app/components/MobileGallery';
// import ReactIScroll from 'react-iscroll';
// import iScroll from 'iscroll/build/iscroll';

import Preloader from 'app/components/Preloader';
import Instagram from 'app/components/Instagram';
import InstagramFeedMobile from 'app/pages/Main/InstagramFeedMobile';
import BottomButtons from 'app/components/BottomButtons';
import CollectionHeader from './Blocks/CollectionHeader';
import CollectionHeaderMobile from './Blocks/CollectionHeaderMobile';
import TextWithDecorations from './Blocks/TextWithDecorations';
import Banner from './Blocks/Banner';


export default class Lookbook extends Component {
  state = {
    lookbook: {},
  }

  componentWillMount() {
    const { lookbook } = this.props.routeParams;
    this.fetchLookbook({ lookbook });
  }

  componentWillReceiveProps(nextProps) {
    const { lookbook } = nextProps.routeParams;
    const { lookbook: prevLookbook } = this.props.routeParams;
    if (prevLookbook !== lookbook) {
      this.fetchLookbook({ lookbook });
    }
  }

  fetchLookbook = ({ lookbook }) => {
    fetch(`https://service.canoe.ru/page/collections/${lookbook}`)
      .then(response => response.json())
      .then(data => this.setState({ lookbook: data.return }));
  }

  render() {
    const { lookbook } = this.state;
    if (lookbook && lookbook.PageContent && lookbook.PageContent.length > 0) {
      const {bottomButtons} = lookbook;

      return (
        <section className="page-content collection-page" style={{ backgroundColor: lookbook.PageMeta.BackgroundColor}}>
          <div className="content-wrapper">
            {
              lookbook.PageContent.map((block, blockIndex) => {
                switch (block.block_name) {
                  case 'collection-header':
                    return (
                      block.contents.map((item, index) => (
                        <div key={index}>
                          <MediaQuery query='(max-width: 768px)'>
                            <CollectionHeaderMobile key={blockIndex} {...item} />
                          </MediaQuery>
                          <MediaQuery query='(min-width: 769px)'>
                            <CollectionHeader key={blockIndex} {...item} />
                          </MediaQuery>
                        </div>
                      ))
                    );
                  case 'text-with-decorations':
                    return (
                      block.contents.map((item, index) => (
                        <TextWithDecorations key={blockIndex} index={blockIndex} {...item} />
                      ))
                    );
                  case 'two-photos':
                    return (
                      <div key={blockIndex} className="two-photos">
                        {
                          block.contents.map((photo, index) => (
                            <Link key={`${blockIndex}-${index}`} to={photo.ImageURL} className="photo">
                              <img src={photo.Image.Url} alt="" />
                            </Link>
                          ))
                        }
                      </div>
                    );
                  case 'banner-with-centered-content':
                    return (
                      block.contents.map((item, index) => (
                        <Banner key={blockIndex} isMobile={true} {...item} />
                      ))
                    );
                  case 'colored-background':
                    return (
                      <MediaQuery key={blockIndex} query='(min-width: 769px)'>
                        {
                          block.contents.map((item, index) => (
                            <div
                              key={index}
                              className="colored-background full-width"
                              style={{
                                backgroundColor: item.BackgroundColor,
                                backgroundImage: `url("${item.Image.Url}")`,
                              }}
                            />
                          ))
                        }
                      </MediaQuery>
                    );
                  case 'banner full-width':
                    return (
                      block.contents.map((item, index) => (
                        <div
                          key={blockIndex}
                          className="banner full-width"
                          style={{
                            backgroundImage: `url("${item.Image.Url}")`,
                          }}
                        />
                      ))
                    );
                  case 'photos-list':
                    return (
                      <div key={blockIndex}>
                        <MediaQuery query='(max-width: 768px)'>
                          <MobileGallery noText={true} name={`lookbook-${blockIndex}`} content={block.contents} />
                        </MediaQuery>
                        <MediaQuery query='(min-width: 769px)'>
                          <div className="philosophy-photos">
                            <Swiper
                              spaceBetween={40}
                              slidesPerView={2}
                              loop
                              navigation={{
                                nextEl: ".swiper-button-next",
                                prevEl: ".swiper-button-prev"
                              }}
                            >
                              {
                                block.contents.map((item, index) => (
                                  <Link key={`${blockIndex}-${index}`} to={item.ImageURL}>
                                    <div className="philosophy-photos__item" style={{ backgroundImage: `url(${item.Image.Url})` }} />
                                  </Link>
                                ))
                              }
                            </Swiper>
                          </div>
                        </MediaQuery>
                      </div>
                    );
                  default:
                    return null;
                }
              })
            }


            <MediaQuery query='(max-width: 768px)'>
              <div className="remove-top-margin">
                <BottomButtons {...bottomButtons} />
                <InstagramFeedMobile hashtag="#canoeheadwear" />
              </div>
            </MediaQuery>

            <MediaQuery query='(min-width: 769px)'>
              <div>
                <Instagram hashtag="#canoeheadwear" />
                <BottomButtons {...bottomButtons} />
              </div>
            </MediaQuery>
          </div>
        </section>
      );
    }

    return (
      <Preloader />
    );
  }
}
