import React from 'react';
import { Link } from 'react-router';

import MobileGallery from 'app/components/MobileGallery';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';

import InstagramFeedMobile from 'app/pages/Main/InstagramFeedMobile';
import BottomButtons from 'app/components/BottomButtons';

const $ = window.jQuery;


const ArtProjectMobile = () => {

  const onRefreshProducts = (inst) => {
    const scroller = $(inst.scroller);
    scroller.width(scroller.find(".product").length * 132);
  }

  const expand = () => {
    $('.collapsed-content').addClass('expanded');
    $('.expand-btn-container').addClass('hidden');
  }

  const ArtProject1 = [
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-1.jpg"
      }
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-2.jpg"
      }
    },
  ];

  const ArtProject2 = [
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-3-1.jpg"
      }
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-3.jpg"
      }
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-3-2.jpg"
      }
    },
  ];

  const ArtProject3 = [
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-10-1.jpg"
      },
      Text: "Творческая энергия —<br />то, что соединяет нас, вопреки<br />всем различиям"
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-10-2.jpg"
      },
      Text: "Так мы реализуем себя<br />и создаём ценность для других людей.<br />Так мы двигаем мир вперёд"
    },
  ];

  const ArtProject4 = [
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-16-1.jpg"
      },
      Text: "Делай вдох —<br />и узнавай себя лучше"
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-16.jpg"
      },
      Text: "Создавай новое, потому что<br />не можешь иначе.<br />Меняй мир, оставаясь собой"
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-16-2.jpg"
      },
      Text: "Делай выдох —<br />и заявляй о себе миру"
    },
  ];

  const ArtProject5 = [
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-17-2.jpg"
      },
      Text: "Мы объединили дизайнеров, иллюстраторов, стилистов, фотографов, чтобы в режиме реального времени создать нечто особенное"
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/campaign/mobile/campaign-18-1.jpg"
      },
      Text: "Мы рисовали на стекле, создавали композиции, чтобы превратить фотографию в искусство"
    },
  ];

  const options = {
    scrollX: true,
    scrollY: false,
    // snap: '.item',
    // probeType: 3,
    // bounce: false,
    // bounceTime: 0,
    // deceleration: 0,
    // momentum: false,
    // mouseWheel: true,
    click: true,
    preventDefault: false,
    eventPassthrough: true,
  }

  return (
    <div className="image-blocks">
      <div className="c-text">Творить — значит быть свободным<br />от ограничений, следовать<br />внутреннему компасу</div>
      <MobileGallery name="artproject-1" noText={true} content={ArtProject1} />

      <div className="c-text">Не бояться своих желаний,<br />мечтать о большем,<br />смотреть вглубь</div>
      <MobileGallery name="artproject-2" noText={true} showPage={1} content={ArtProject2} />
      <div className="c-text">Быть вдохновением,<br />делать первым,<br />ставить эксперименты</div>

      <div className="centered-photo full-width">
        <span
          className="bg bg-left"
          style={{
            backgroundColor: '#ffeff0'
          }}
        />
        <span
          className="bg bg-right"
          style={{
            backgroundColor: '#d1c4bb'
          }}
        />
        <div
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/mobile/campaign-4.jpg)'
          }}
        />
        <div className="c-text">совершать открытия<br />и делиться ими</div>
      </div>

      <ReactIScroll
        className="products-block b-iscroll"
        onRefresh={(inst) => onRefreshProducts(inst)}
        options={options}
        iScroll={iScroll}>

        <div className="products b-iscroll-container">
          <div className="product">
            <a className="image" href="/catalogue/product/snood_idella_lilac_3445986"><img src="//media.canoe.ru/images/brand/campaign/campaign-6-1.jpg" /></a>
          </div>
          <div className="product">
            <a className="image" href="http://canoe.ru/catalogue/product/beanie_nica_lilac_3447036"><img src="//media.canoe.ru/images/brand/campaign/campaign-6-2.jpg" /></a>
          </div>
          <div className="product">
            <a className="image" href="/catalogue/product/beanie_nerve_lilac_4701836"><img src="//media.canoe.ru/images/brand/campaign/campaign-7-1.jpg" /></a>
          </div>
          <div className="product">
            <a className="image" href="/catalogue/product/beanie_kivke_lilac_4701636"><img src="//media.canoe.ru/images/brand/campaign/campaign-7-2.jpg" /></a>
          </div>
          <div className="product">
            <a className="image" href="/catalogue/product/beanie_sprite_red_4714682"><img src="//media.canoe.ru/images/brand/campaign/campaign-8-1.jpg" /></a>
          </div>
          <div className="product">
            <a className="image" href="/catalogue/product/beanie_sekret_white_4714950"><img src="//media.canoe.ru/images/brand/campaign/campaign-8-2.jpg" /></a>
          </div>
          <div className="product">
            <a className="image" href="/catalogue/product/gloves_piano_red_6700372_onesize"><img src="//media.canoe.ru/images/brand/campaign/campaign-9-1.jpg" /></a>
          </div>
          <div className="product">
            <a className="image" href="/catalogue/product/beanie_cruza_red_4712972"><img src="//media.canoe.ru/images/brand/campaign/campaign-9-2.jpg" /></a>
          </div>
        </div>
      </ReactIScroll>

      <div className="centered-photo full-width">
        <div
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/mobile/campaign-5.jpg)'
          }}
        />
      </div>

      <div className="text-with-decorations">
        <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
        <div className="text">Творить<br />значит жить</div>
        <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
      </div>

      <div className="collapsed-content">
        <MobileGallery name="artproject-3" content={ArtProject3} />

        <div className="centered-photo small-margin full-width">
          <div className="c-text">Создание головных уборов Canoe — творческий порыв, которым мы делимся с вами</div>
          <span
            className="bg bg-wide"
            style={{
              backgroundColor: '#b5d0e5'
            }}
          />
          <div
            className="image square"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/mobile/campaign-11.jpg)'
            }}
          />
        </div>

        <div className="c-text small">Мы любим жизнь и находим вдохновение в том, что нас окружает. Объединяя знания традиционных мастеров и новейшие технологические разработки, мы создаем аксессуары, заряженные на движение вперёд и свободу самовыражения.</div>

        <ReactIScroll
          className="products-block b-iscroll"
          onRefresh={(inst) => onRefreshProducts(inst)}
          options={options}
          iScroll={iScroll}>

          <div className="products b-iscroll-container">
            <div className="product">
              <a className="image" href="/catalogue/product/snood_dixon_lilac_3448856"><img src="//media.canoe.ru/images/brand/campaign/campaign-12-1.jpg" /></a>
            </div>
            <div className="product">
              <a className="image" href="/catalogue/product/beanie_niko_lilac_3441996"><img src="//media.canoe.ru/images/brand/campaign/campaign-12-2.jpg" /></a>
            </div>
            <div className="product">
              <a className="image" href="/catalogue/product/beanie_warn_black_4712641"><img src="//media.canoe.ru/images/brand/campaign/campaign-13-1.jpg" /></a>
            </div>
            <div className="product">
              <a className="image" href="/catalogue/product/beanie_real_black_4712441"><img src="//media.canoe.ru/images/brand/campaign/campaign-13-2.jpg" /></a>
            </div>
            <div className="product">
              <a className="image" href="/catalogue/product/scarf_sher_red_3447552"><img src="//media.canoe.ru/images/brand/campaign/campaign-14-1.jpg" /></a>
            </div>
            <div className="product">
              <a className="image" href="/catalogue/product/beanie_kipr_red_4714052"><img src="//media.canoe.ru/images/brand/campaign/campaign-14-2.jpg" /></a>
            </div>
            <div className="product">
              <a className="image" href="/catalogue/product/beanie_kipr_red_4714052"><img src="//media.canoe.ru/images/brand/campaign/campaign-15-1.jpg" /></a>
            </div>
            <div className="product">
              <span className="image"><img src="//media.canoe.ru/images/brand/campaign/campaign-15-2.jpg" /></span>
            </div>
          </div>
        </ReactIScroll>

        <MobileGallery name="artproject-4" showPage={1} content={ArtProject4} />

        <div
          className="banner-with-centered-content full-width"
          style={{
            backgroundImage: `url(//media.canoe.ru/images/brand/campaign/banner.jpg)`,
          }}
        >
          <div className="content text-promo">
            <p className="text with-br">создай арт<br />прямо сейчас!</p>
            <Link to="/contest" className="b-with-media__content__link">Принять участие</Link>
          </div>
        </div>


        <MobileGallery name="artproject-5" textHeight={140} content={ArtProject5} />
      </div>

      <div className="expand-btn-container">
        <span onClick={expand} className="expand-btn"></span>
        <i className="gradient" />
        <p onClick={expand}>Узнать больше</p>
      </div>
      <BottomButtons title="Наши коллекции" description="Лето-Зима 17/18" buttons={[{name: 'Кампания зима 18', url: '/collections/winter_2018'}, {name: 'Лукбук лето 17', url: '/collections/summer_2017'}]} />
      <InstagramFeedMobile hashtag="#canoe_artlovers" />

    </div>
  );
};


export default ArtProjectMobile;
