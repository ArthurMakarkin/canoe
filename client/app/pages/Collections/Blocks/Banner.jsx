import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import getImageSize from 'app/mixins/imageSizes';


export class Banner extends Component {
  static propTypes = {
    Image: PropTypes.oneOfType([PropTypes.shape({
      Url: PropTypes.string,
      ThumbUrl: PropTypes.string,
    }), PropTypes.string]),
    Title: PropTypes.string,
    Text: PropTypes.string,
    buttons: PropTypes.array,
    isMobile: PropTypes.bool,
  }

  render() {
    const { Image, Title, Text, buttons, isMobile } = this.props;
    const { pagewidth } = this.props.app;

    const style = {};
    if (Image.Url !== '' && Image !== '') {
      const bg = isMobile && Image.Url ? getImageSize(pagewidth, Image.Url, 'tallBanner') : Image.Url;
      style.backgroundImage = Image.Url ? `url(${bg})` : Image.replace(';', '');
    }

    const hasTitle = Title.length > 0;
    const hasText = Text.length > 0;
    const hasButtons = buttons && buttons.length > 0;

    return (
      <div
        className={`banner-with-centered-content ${!hasTitle && !hasText ? 'mobile-bg-only' : 'mobile-no-bg'} ${Image.Url ? 'full-width' : 'banner-with-gradient'}`}
        style={style}
      >
        <div className="content">
          {
            hasTitle &&
            <svg className={`decoration svg-decoration-1`}><use xlinkHref={`/assets/images/sprite.svg#decoration-1`} /></svg>
          }
          {
            hasTitle &&
            <div className="text" dangerouslySetInnerHTML={{ __html: Title }} />
          }
          {
            hasText &&
            <div className="subtext" dangerouslySetInnerHTML={{ __html: Text }} />
          }
          {
            (hasText || hasTitle) &&
            <svg className={`decoration svg-decoration-1`}><use xlinkHref={`/assets/images/sprite.svg#decoration-1`} /></svg>

          }
          {
            hasButtons && buttons.map((button, index) => (
              <Link key={index} className="b-with-media__content__link" to={button.url}>{button.name}</Link>
            ))
          }
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { app } = state;
  return {
    app,
  };
}

// function mapDispatchToProps(dispatch) {
//   return {
//     actions: bindActionCreators(actions, dispatch),
//   };
// }

export default connect(mapStateToProps)(Banner);
