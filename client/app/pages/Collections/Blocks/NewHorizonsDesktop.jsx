import React from 'react';
import { Link } from 'react-router';
import fetchJsonp from 'fetch-jsonp';
import Instagram from 'app/components/Instagram';
import BottomButtons from 'app/components/BottomButtons';

const $ = window.jQuery;


const NewHorizonsDesktop = () => {

  const changeImage = (elem) =>{
    let link = elem.target.closest('.js-toggle');
    let active = link.querySelector('.image.active');
    let hidden = link.querySelector('.image:not(.active)');
    if(active.classList.contains('prepared')) return false;
    active.classList.add('prepared');
    hidden.classList.add('prepared');
    active.classList.remove('active');
    hidden.classList.add('active');
    active.classList.add('hide');
    setTimeout(() => {
      active.classList.remove('prepared', 'hide');
      hidden.classList.remove('prepared', 'hide');
      hidden.classList.add('active');
    }, 1000);
  }

  // const video = $('#video');
  // const videoContainer = video.closest('.image');

  // const setVideoSize = () => {
  //   video.height(videoContainer.width() * 532 / 944);
  // }

  // setVideoSize();
  // $(window).on('resize', setVideoSize);

  return (
    <div>
      <div className="diagonal-blocks two-diagonal-blocks no-margin">
        <div className="block">
          <div className="text">Небо вдохновляет на приключения,<br />в небе нет границ и условностей.</div>
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/04-1.jpg)'
            }}
          />
        </div>
        <div className="block">
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/05.jpg)'
            }}
          />
          <div className="text">Оно дарит освобождение<br />и желание двигаться вперёд.</div>
        </div>
      </div>

      <div className="diagonal-blocks three-diagonal-blocks full-width">
        <div className="block block-left">
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/06.jpg)'
            }}
          />
        </div>
        <div className="block">
          <div className="text">Новые горизонты — новые вопросы.<br />Ищи ответы и открывай себя заново каждый день.</div>
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/07.jpg)'
            }}
          />
          <div className="text">Истинна только свобода.<br />Когда ты свободен, твой потенциал безграничен.</div>
        </div>
        <div className="block block-right">
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/06.jpg)'
            }}
          />
        </div>
        <i className="collections-bg collections-bg-2" />
      </div>

      <div className="diagonal-blocks odd-blocks full-width">
        <div className="block odd-block">
          <div className="text">Новые горизонты — новые вопросы.<br />Ищи ответы и открывай себя<br />заново каждый день.</div>
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/09.jpg)'
            }}
          />
        </div>
        <div className="block">
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/08.jpg)'
            }}
          />
        </div>
      </div>

      <div className="centered-photo full-width no-margin">
        <div className="text">Загляни вглубь неба и вспомни то, что всегда знал, —<br />у тебя от рождения были крылья. Нужно только разрешить себе летать.</div>
        <div className="image">
          {/*backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/21-1.jpg)'*/}
          <video id="video" autoPlay loop>
            <source src="//media.canoe.ru/images/brand/2018/1.mp4" type="video/mp4" />
          </video>
          <div
            className="flying-image left"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/10.jpg)'
          }}/>
          <div
            className="flying-image right"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/12.jpg)'
          }}/>
        </div>
        <div
          className="centered-small-image" 
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/11.jpg)'
          }}
        />
        <div className="text">Доверься своей природе, будь бесстрашным —<br />и небо сделает тебя неуязвимым.</div>
      </div>

      <div className="centered-photo top-padding full-width">
        <span
          className="bg bg-wide"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/13.jpg)'
          }}
        />
        <div
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/14.jpg)'
          }}
        />
      </div>

      <div className="diagonal-blocks three-diagonal-blocks full-width">
        <div className="block block-left">
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/15.jpg)'
            }}
          />
        </div>
        <div className="block">
          <div className="text">Вырвись из клетки собственных<br />мыслей</div>
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/16.jpg)'
            }}
          />
          <div className="text">почувствуй безграничную свободу<br />быть собой</div>
        </div>
        <div className="block block-right">
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/17.jpg)'
            }}
          />
        </div>
      </div>

      <div className="centered-photo full-width">
        <span
          className="bg bg-left on-top"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/18.jpg)'
          }}
        />
        <span
          className="bg bg-right"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/19.jpg)'
          }}
        />
        <div
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/20.jpg)'
          }}
        />
      </div>

      <div className="text-with-decorations">
        <svg className="decoration svg-decoration-3"><use xlinkHref="/assets/images/sprite.svg#decoration-3" /></svg>
        <div className="text">Взлетай</div>
        <svg className="decoration svg-decoration-3"><use xlinkHref="/assets/images/sprite.svg#decoration-3" /></svg>
      </div>

{/*      <div className="products-block">
        <div className="products">
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/snood_dixon_lilac_3448856"><img src="//media.canoe.ru/images/brand/campaign/campaign-12-1.jpg" /></a>
              <a className="image" href="/catalogue/product/beanie_niko_lilac_3441996"><img src="//media.canoe.ru/images/brand/campaign/campaign-12-2.jpg" /></a>
            </span>
          </div>
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/beanie_warn_black_4712641"><img src="//media.canoe.ru/images/brand/campaign/campaign-13-1.jpg" /></a>
              <a className="image" href="/catalogue/product/beanie_real_black_4712441"><img src="//media.canoe.ru/images/brand/campaign/campaign-13-2.jpg" /></a>
            </span>
          </div>
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/scarf_sher_red_3447552"><img src="//media.canoe.ru/images/brand/campaign/campaign-14-1.jpg" /></a>
              <a className="image" href="/catalogue/product/beanie_kipr_red_4714052"><img src="//media.canoe.ru/images/brand/campaign/campaign-14-2.jpg" /></a>
            </span>
          </div>
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/beanie_kipr_red_4714052"><img src="//media.canoe.ru/images/brand/campaign/campaign-15-1.jpg" /></a>
              <span className="image"><img src="//media.canoe.ru/images/brand/campaign/campaign-15-2.jpg" /></span>
            </span>
          </div>
        </div>
      </div>
*/}
      <Instagram hashtag="#freewithcanoe" />
      <BottomButtons title="Кампания Зима 18/19" description="Выбери свое приключение" buttons={[{name: 'Новые горизонты', url: '/collections/horizons'}, {name: 'Наперегонки с ночью', url: '/collections/night'}]} />


    </div>
  );
};


export default NewHorizonsDesktop;
