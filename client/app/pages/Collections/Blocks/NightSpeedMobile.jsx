import React from 'react';
import { Link } from 'react-router';

import MobileGallery from 'app/components/MobileGallery';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';

import InstagramFeedMobile from 'app/pages/Main/InstagramFeedMobile';
import BottomButtons from 'app/components/BottomButtons';

import tiltAnimation from 'app/mixins/tilt';

const $ = window.jQuery;


const NightSpeedMobile = () => {

  const onRefreshProducts = (inst) => {
    const scroller = $(inst.scroller);
    scroller.width(scroller.find(".product").length * 132);
  }

  const NightSpeed1 = [
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/2018/02/05.jpg"
      },
      Text: "Научись видеть совершенство <br />в каждом моменте, ведь он больше<br />не повторится."
    },
    // {
    //   Image: {
    //     Url: "//media.canoe.ru/images/brand/2018/02/04.jpg"
    //   },
    //   Text: ""
    // },
  ];

  // const NightSpeed2 = [
  //   {
  //     Image: {
  //       Url: "//media.canoe.ru/images/brand/2018/02/07.jpg"
  //     },
  //     Text: "Звезды подскажут, какое желание загадать.<br />Небо покажет путь."
  //   },
  //   {
  //     Image: {
  //       Url: "//media.canoe.ru/images/brand/2018/02/06.jpg"
  //     },
  //     Text: "Сердце отзовётся жаждой приключений.<br />Вдыхай их полной грудью."
  //   },
  //   {
  //     Image: {
  //       Url: "//media.canoe.ru/images/brand/2018/02/08.jpg"
  //     },
  //     Text: ""
  //   },
  // ];

  // const NightSpeed3 = [
  //   {
  //     Image: {
  //       Url: "//media.canoe.ru/images/brand/2018/02/09.jpg"
  //     },
  //     Text: "Дорога — твой союзник.<br />главная цель — это движение вперед."
  //   },
  //   {
  //     Image: {
  //       Url: "//media.canoe.ru/images/brand/2018/02/10.jpg"
  //     },
  //     Text: ""
  //   },
  // ];

  const NightSpeed4 = [
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/2018/02/21-1.jpg"
      },
      Text: "Скорость — это незамутнённая свобода.<br />Скорость — это чистый восторг."
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/2018/02/11.jpg"
      },
      Text: "Наперегонки с самим собой — единственный поединок,<br />который стоит того, чтобы выиграть."
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/2018/02/12.jpg"
      },
      Text: ""
    },
  ];


  const options = {
    scrollX: true,
    scrollY: false,
    click: true,
    preventDefault: false,
    eventPassthrough: true,
  }

  return (
    <div className="image-blocks">
      <MobileGallery name="nightspeed-1" showPage={1} content={NightSpeed1} />

      <div className="mobile-bg-container">
        <i className="collections-bg collections-bg-4" />
        
{/*        <MobileGallery name="nightspeed-2" showPage={2} content={NightSpeed2} />
        
        <MobileGallery name="nightspeed-3" showPage={2} content={NightSpeed3} />
*/}
        <div className="banner-with-centered-content full-width" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/02/06.jpg)"}} />

        <div className="text">Звезды подскажут, какое желание загадать.<br />Небо покажет путь.</div>

        <div className="banner-with-centered-content full-width" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/02/09.jpg)"}} />

        <div className="text">Дорога — твой союзник.<br />главная цель — это движение вперед.</div>
      </div>

      <div className="centered-photo full-width no-margin">
        <div
          id="tiltBlock2"
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/2018/02/21-1.jpg)'
          }}>
          {tiltAnimation('tiltBlock2')}
          <div className="flying-images">
            <div
              className="flying-image left"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/2018/02/11.jpg)'
            }}/>
            <div
              className="flying-image right"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/2018/02/12.jpg)'
            }}/>
            <div
              className="centered-small-image" 
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/2018/02/22-1.jpg)'
              }}
            />
          </div>
        </div>
        <div className="text">Наперегонки с самим собой — единственный поединок,<br />который стоит того, чтобы выиграть.</div>
      </div>

      <div className="banner-with-centered-content mobile-bg-only full-width" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/02/14.jpg"}} />

      <MobileGallery name="nightspeed-4" showPage={2} content={NightSpeed4} />

      <div className="vertical-scroll no-height full-width">
        <div
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/2018/02/19.jpg)'
          }}
        />
      </div>

      <div className="text-with-decorations">
        <svg className="decoration svg-decoration-3"><use xlinkHref="/assets/images/sprite.svg#decoration-3" /></svg>
        <div className="text">Взлетай</div>
        <svg className="decoration svg-decoration-3"><use xlinkHref="/assets/images/sprite.svg#decoration-3" /></svg>
      </div>

      {/*тут должны быть продукты*/}

      <div className="cross-link">
        <div className="image" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/01.jpg)"}}></div>
        
        <div className="text bg-left" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/03.jpg)"}}>
          
          <div className="title">Новые<br />горизонты</div>
          <p className="comment">Испытай один раз полёт,<br />и твои глаза навечно будут<br />устремлены в небо.<br />— Леонардо да Винчи —</p>
          <a className="button" href="/collections/horizons">Смотреть лукбук</a>
        </div>
      </div>

      <InstagramFeedMobile hashtag="#freewithcanoe" />
      <BottomButtons title="Кампания Зима 18/19" description="Выбери свое приключение" buttons={[{name: 'Новые горизонты', url: '/collections/horizons'}, {name: 'Наперегонки с ночью', url: '/collections/night'}]} />

    </div>
  );
};


export default NightSpeedMobile;
