import React from 'react';
import PropTypes from 'prop-types';


const CollectionHeader = ({
  Image,
  Title,
  Text,
}) => (
  <div
    className="collection-header full-width"
    style={{
      backgroundImage: `url(${Image.Url})`,
    }}
  >
    <div className="heading">
      <div className="text">{Text}</div>
      <div className="name">{Title}</div>
    </div>
  </div>
);

CollectionHeader.propTypes = {
  Image: PropTypes.shape({
    Url: PropTypes.string,
    ThumbUrl: PropTypes.string,
  }),
  Title: PropTypes.string,
  Text: PropTypes.string,
};

export default CollectionHeader;
