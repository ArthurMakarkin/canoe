import React from 'react';
import PropTypes from 'prop-types';


const TextWithDecorations = ({
  Title,
  Text,
  index,
}) => {
  const hasTitle = Title.length > 0;
  return (
    <div className={`text-with-decorations text-with-decorations-${index} mobile-no-decorations`}>
      {
        hasTitle &&
        <svg className={`decoration svg-decoration-1`}><use xlinkHref={`/assets/images/sprite.svg#decoration-1`} /></svg>
      }
      {
        hasTitle &&
        <div className="text" dangerouslySetInnerHTML={{ __html: Title }} />
      }
      <svg className={`decoration svg-decoration-1`}><use xlinkHref={`/assets/images/sprite.svg#decoration-1`} /></svg>
      <div className="subtext" dangerouslySetInnerHTML={{ __html: Text }} />
    </div>
  );
};

TextWithDecorations.propTypes = {
  Title: PropTypes.string,
  Text: PropTypes.string,
  index: PropTypes.number
};

export default TextWithDecorations;
