import React from 'react';
import { Link } from 'react-router';

import MobileGallery from 'app/components/MobileGallery';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';

import InstagramFeedMobile from 'app/pages/Main/InstagramFeedMobile';
import BottomButtons from 'app/components/BottomButtons';

import tiltAnimation from 'app/mixins/tilt';

const $ = window.jQuery;


const NewHorizonsMobile = () => {

  const onRefreshProducts = (inst) => {
    const scroller = $(inst.scroller);
    scroller.width(scroller.find(".product").length * 132);
  }

  const NewHorizons4 = [
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/2018/01/15.jpg"
      },
      Text: "Вырвись из клетки собственных<br />мыслей"
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/2018/01/16.jpg"
      },
      Text: "почувствуй безграничную свободу<br />быть собой"
    },
    {
      Image: {
        Url: "//media.canoe.ru/images/brand/2018/01/17.jpg"
      },
      Text: ""
    },
  ];


  const options = {
    scrollX: true,
    scrollY: false,
    click: true,
    preventDefault: false,
    eventPassthrough: true
  }

  return (
    <div className="image-blocks">
      <div className="banner-with-centered-content no-height-limit full-width" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/01/04-2.jpg)"}} />

      <div className="text">Небо вдохновляет на приключения,<br />в небе нет границ и условностей.</div>

      <div className="mobile-bg-container">
        <i className="collections-bg collections-bg-2" />
        
{/*        <MobileGallery name="newhorizions-2" showPage={2} content={NewHorizons2} />
*/}        
        <div className="banner-with-centered-content full-width" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/01/07.jpg)"}} />

        <div className="text">Истинна только свобода.<br />Когда ты свободен, твой потенциал безграничен.</div>

        <div className="banner-with-centered-content full-width" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/01/09.jpg)"}} />

        <div className="text">Новые горизонты — новые вопросы.<br />Ищи ответы и открывай себя<br />заново каждый день.</div>

        {/*<MobileGallery name="newhorizions-3" showPage={2} content={NewHorizons3} />*/}
      </div>

      <div className="centered-photo full-width no-margin tilt-container">
        <div
          id="tiltBlock"
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/21-1.jpg)'
          }}>
          {tiltAnimation('tiltBlock')}
          <div className="flying-images">
            <div
              className="flying-image left"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/10.jpg)'
            }}/>
            <div
              className="flying-image right"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/12.jpg)'
            }}/>
            <div
              className="centered-small-image" 
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/11.jpg)'
              }}
            />
          </div>
        </div>
        <div className="text">Доверься своей природе, будь бесстрашным —<br />и небо сделает тебя неуязвимым.</div>
      </div>

      <div className="banner-with-centered-content mobile-bg-only full-width" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/01/14.jpg)"}} />

      <MobileGallery name="newhorizions-4" showPage={2} content={NewHorizons4} />

      <div className="vertical-scroll no-height full-width">
        <div
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/2018/01/20.jpg)'
          }}
        />
      </div>

      <div className="text-with-decorations">
        <svg className="decoration svg-decoration-3"><use xlinkHref="/assets/images/sprite.svg#decoration-3" /></svg>
        <div className="text">Взлетай</div>
        <svg className="decoration svg-decoration-3"><use xlinkHref="/assets/images/sprite.svg#decoration-3" /></svg>
      </div>

      {/*тут должны быть продукты*/}

      <div className="cross-link">
        <div className="image" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/02.jpg)"}}></div>
        
        <div className="text" style={{backgroundImage: "url(//media.canoe.ru/images/brand/2018/03.jpg)"}}>
          
          <div className="title">Наперегонки<br />с ночью</div>
          <p className="comment">Каждые 24 часа — это новый виток<br />твоего приключения.</p>
          <a className="button" href="/collections/night">Смотреть лукбук</a>
        </div>
      </div>

      <InstagramFeedMobile hashtag="#freewithcanoe" />
      <BottomButtons title="Кампания Зима 18/19" description="Выбери свое приключение" buttons={[{name: 'Новые горизонты', url: '/collections/horizons'}, {name: 'Наперегонки с ночью', url: '/collections/night'}]} />

    </div>
  );
};


export default NewHorizonsMobile;
