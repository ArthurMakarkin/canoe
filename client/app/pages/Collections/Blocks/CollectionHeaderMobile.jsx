import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import getImageSize from 'app/mixins/imageSizes';


export class CollectionHeaderMobile extends Component {
  static propTypes = {
    Image: PropTypes.shape({
      Url: PropTypes.string,
      ThumbUrl: PropTypes.string,
    }),
    Title: PropTypes.string,
    Text: PropTypes.string,
  }

  render() {
    const { Image, Title, Text } = this.props;
    const { pagewidth } = this.props.app;
    const headerImage = getImageSize(pagewidth, Image.Url, 'tallBanner');

    return (
      <div>
        <div
          className="collection-header full-width"
          style={{
            backgroundImage: `url(${headerImage})`,
          }}
        >
        </div>
        <div className="mobile-heading">
          <div className="text">{Text}</div>
          <div className="name">{Title}</div>
        </div>

      </div>
    )
  }
}

function mapStateToProps(state) {
  const { app } = state;
  return {
    app,
  };
}

// function mapDispatchToProps(dispatch) {
//   return {
//     actions: bindActionCreators(actions, dispatch),
//   };
// }

export default connect(mapStateToProps)(CollectionHeaderMobile);
