import React from 'react';
import { Link } from 'react-router';
import fetchJsonp from 'fetch-jsonp';
import Instagram from 'app/components/Instagram';
import BottomButtons from 'app/components/BottomButtons';


const ArtProjectDesktop = () => {

  const changeImage = (elem) =>{
    let link = elem.target.closest('.js-toggle');
    let active = link.querySelector('.image.active');
    let hidden = link.querySelector('.image:not(.active)');
    if(active.classList.contains('prepared')) return false;
    active.classList.add('prepared');
    hidden.classList.add('prepared');
    active.classList.remove('active');
    hidden.classList.add('active');
    active.classList.add('hide');
    setTimeout(() => {
      active.classList.remove('prepared', 'hide');
      hidden.classList.remove('prepared', 'hide');
      hidden.classList.add('active');
    }, 1000);
  }

  return (
    <div>
      <div className="diagonal-blocks two-diagonal-blocks no-margin">
        <div className="block">
          <div className="text">Творить — значит быть свободным<br />от ограничений, следовать<br />внутреннему компасу</div>
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-1.jpg)'
            }}
          />
        </div>
        <div className="block">
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-2.jpg)'
            }}
          />
        </div>
      </div>

      <div className="diagonal-blocks three-diagonal-blocks full-width">
        <div className="block block-left">
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-3-1.jpg)'
            }}
          />
        </div>
        <div className="block">
          <div className="text">Не бояться своих желаний,<br />мечтать о большем,<br />смотреть вглубь</div>
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-3.jpg)'
            }}
          />
          <div className="text">Быть вдохновением,<br />делать первым,<br />ставить эксперименты</div>
        </div>
        <div className="block block-right">
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-3-2.jpg)'
            }}
          />
        </div>
      </div>

      <div className="centered-photo full-width">
        <span
          className="bg bg-left"
          style={{
            backgroundColor: '#ffeff0'
          }}
        />
        <span
          className="bg bg-right"
          style={{
            backgroundColor: '#d1c4bb'
          }}
        />
        <div
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-4.jpg)'
          }}
        />
      </div>

      <div className="products-block">
        <p className="text uppercase">совершать открытия<br />и делиться ими</p>
        <div className="products">
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/snood_idella_lilac_3445986"><img src="//media.canoe.ru/images/brand/campaign/campaign-6-1.jpg" /></a>
              <a className="image" href="http://canoe.ru/catalogue/product/beanie_nica_lilac_3447036"><img src="//media.canoe.ru/images/brand/campaign/campaign-6-2.jpg" /></a>
            </span>
          </div>
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/beanie_nerve_lilac_4701836"><img src="//media.canoe.ru/images/brand/campaign/campaign-7-1.jpg" /></a>
              <a className="image" href="/catalogue/product/beanie_kivke_lilac_4701636"><img src="//media.canoe.ru/images/brand/campaign/campaign-7-2.jpg" /></a>
            </span>
          </div>
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/beanie_sprite_red_4714682"><img src="//media.canoe.ru/images/brand/campaign/campaign-8-1.jpg" /></a>
              <a className="image" href="/catalogue/product/beanie_sekret_white_4714950"><img src="//media.canoe.ru/images/brand/campaign/campaign-8-2.jpg" /></a>
            </span>
          </div>
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/gloves_piano_red_6700372_onesize"><img src="//media.canoe.ru/images/brand/campaign/campaign-9-1.jpg" /></a>
              <a className="image" href="/catalogue/product/beanie_cruza_red_4712972"><img src="//media.canoe.ru/images/brand/campaign/campaign-9-2.jpg" /></a>
            </span>
          </div>
        </div>
      </div>

      <div className="centered-photo full-width">
        <span
          className="bg bg-left"
          style={{
            backgroundColor: '#db8971'
          }}
        />
        <span
          className="bg bg-right"
          style={{
            backgroundColor: '#0a389a'
          }}
        />
        <div
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-5.jpg)'
          }}
        />
      </div>

      <div className="text-with-decorations">
        <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
        <div className="text">Творить значит<br />жить</div>
        <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
      </div>

      <div className="diagonal-blocks two-diagonal-blocks">
        <div className="block">
          <div className="text">Творческая энергия —<br />то, что соединяет нас, вопреки<br />всем различиям</div>
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-10-1.jpg)'
            }}
          />
        </div>
        <div className="block">
          <div
            className="image"
            style={{
              backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-10-2.jpg)'
            }}
          />
          <div className="text">Так мы реализуем себя<br />и создаём ценность для других людей.<br />Так мы двигаем мир вперёд</div>
        </div>
      </div>

      <div className="text-with-decorations">
        <svg className="decoration svg-decoration-3"><use xlinkHref="/assets/images/sprite.svg#decoration-3" /></svg>
        <div className="text">Создание<br />головных уборов</div>
        <svg className="decoration svg-decoration-3"><use xlinkHref="/assets/images/sprite.svg#decoration-3" /></svg>
      </div>

      <div className="centered-photo small-margin full-width">
        <p className="text">Создание головных уборов Canoe —<br />творческий порыв,<br />которым мы делимся с вами</p>
        <span
          className="bg bg-wide"
          style={{
            backgroundColor: '#b5d0e5'
          }}
        />
        <div
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-11.jpg)'
          }}
        />
      </div>

      <div className="products-block">
        <p className="text large">Мы любим жизнь и находим вдохновение в том, что нас окружает.<br />Объединяя знания традиционных мастеров и новейшие<br />технологические разработки, мы создаем аксессуары,<br />заряженные на движение вперёд и свободу самовыражения.</p>
        <div className="products">
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/snood_dixon_lilac_3448856"><img src="//media.canoe.ru/images/brand/campaign/campaign-12-1.jpg" /></a>
              <a className="image" href="/catalogue/product/beanie_niko_lilac_3441996"><img src="//media.canoe.ru/images/brand/campaign/campaign-12-2.jpg" /></a>
            </span>
          </div>
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/beanie_warn_black_4712641"><img src="//media.canoe.ru/images/brand/campaign/campaign-13-1.jpg" /></a>
              <a className="image" href="/catalogue/product/beanie_real_black_4712441"><img src="//media.canoe.ru/images/brand/campaign/campaign-13-2.jpg" /></a>
            </span>
          </div>
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/scarf_sher_red_3447552"><img src="//media.canoe.ru/images/brand/campaign/campaign-14-1.jpg" /></a>
              <a className="image" href="/catalogue/product/beanie_kipr_red_4714052"><img src="//media.canoe.ru/images/brand/campaign/campaign-14-2.jpg" /></a>
            </span>
          </div>
          <div className="product">
            <span className="js-toggle" onMouseEnter={changeImage.bind(this)}>
              <a className="image active" href="/catalogue/product/beanie_kipr_red_4714052"><img src="//media.canoe.ru/images/brand/campaign/campaign-15-1.jpg" /></a>
              <span className="image"><img src="//media.canoe.ru/images/brand/campaign/campaign-15-2.jpg" /></span>
            </span>
          </div>
        </div>
      </div>

      <div className="centered-photo full-width">
        <div className="bg bg-padding bg-left">
          <span
            style={{
              backgroundColor: '#e3e3e3',
              backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-16-2.jpg)'
            }}
          />
        </div>
        <div className="bg bg-padding bg-right">
          <span
            style={{
              backgroundColor: '#e3e3e3',
              backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-16-1.jpg)'
            }}
          />
        </div>
        <div className="text columns">
          <p className="column">Делай вдох —<br />и узнавай себя лучше.</p>
          <p className="column">Делай выдох —<br />и заявляй о себе миру.</p>
        </div>
        <div
          className="image"
          style={{
            backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-16.jpg)'
          }}
        />
        <p className="text">Создавай новое, потому что<br />не можешь иначе.<br />Меняй мир, оставаясь собой.</p>
      </div>

      <div className="cta">
        <p className="text">Креативность — это внутренний огонь,<br />который превращает твои идеи в реальность.</p>
        <p className="title">Вдохновляйся</p>
      </div>

      <div
        className="banner full-width"
        style={{
          backgroundImage: `url(//media.canoe.ru/images/brand/campaign/banner.jpg)`,
        }}
      >
        <p className="text">создай арт<br />прямо сейчас!</p>
        <Link to="/contest" className="b-with-media__content__link">Принять участие</Link>
      </div>

      <div className="text-with-decorations for-campaign">
        <div className="suptitle">Творческий эксперимент</div>
        <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
      </div>

      <div className="diagonal-blocks two-diagonal-blocks">
        <div className="block">
          <div className="text">Мы объединили дизайнеров,<br />иллюстраторов, стилистов, фотографов,<br />чтобы в режиме реального времени<br />создать нечто особенное</div>
          <div className="images js-toggle" onMouseEnter={changeImage.bind(this)}>
            <div
              className="image active"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-17-1.jpg)'
              }}
            />
            <div
              className="image"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-17-2.jpg)'
              }}
            />
          </div>
        </div>
        <div className="block">
          <div className="images js-toggle" onMouseEnter={changeImage.bind(this)}>
            <div
              className="image active"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-18-2.jpg)'
              }}
            />
            <div
              className="image"
              style={{
                backgroundImage: 'url(//media.canoe.ru/images/brand/campaign/campaign-18-1.jpg)'
              }}
            />
          </div>
          <div className="text big-text">
            <p>Мы рисовали на стекле, создавали<br />композиции, чтобы превратить<br />фотографию в искусство</p>
            <p className="comment">В проекте участвовали бренды:<br />INS, ELEN MOCK, DEEP FLOW, TOTO</p>
          </div>
        </div>
      </div>

      <Instagram hashtag="#canoe_artlovers" />
      <BottomButtons title="Наши коллекции" description="Лето-Зима 17/18" buttons={[{name: 'Кампания зима 18', url: '/collections/winter_2018'}, {name: 'Лукбук лето 17', url: '/collections/summer_2017'}]} />


    </div>
  );
};


export default ArtProjectDesktop;
