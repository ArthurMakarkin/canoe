import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import LazyLoad from 'react-lazyload';
import classNames from 'classnames';
import _ from 'lodash';
import Slider from 'react-slick';

import Preloader from 'app/components/Preloader';
import NumberInput from 'app/components/NumberInput';
import MediaQuery from 'react-responsive';

import getImageSize from 'app/mixins/imageSizes';

import * as actions from 'app/actions';

export class Catalogue extends Component {
  state = {
    ascending: false,
    sortMenuOpen: false,
    mobileSearchShowed: false,
    mobileFiltersShowed: false,
  }

  headerBanner = {
    img: '',
    url: ''
  }

  isSearchActive = false;

  componentDidMount() {
    const { fetchFilters, fetchProducts } = this.props.actions;
    const { filters, products } = this.props;
    const ss = sessionStorage;
    if(ss){
      if(!ss.getItem("filter")){
        this.resetFilters();
      }
    }
    if (!filters.length) fetchFilters();
    if (!products.length) fetchProducts();
  }

  getRandomInt(min, max) {
    return min + Math.floor(Math.random() * (max - min + 1));
  }

  getRandomBanner(banners) {
    return banners[this.getRandomInt(0, banners.length - 1)];
  }

  addToBasket(id) {
    this.props.actions.addBasket(
      id,
      document.getElementById(`quantity-${id}`).value
    );
  }

  changeView(view) {
    this.props.actions.updateCatalogueView(view);
  }

  changeSorting(sort) {
    let state = this.state;
    state.sortMenuOpen = false;
    this.setState(state);
    this.props.actions.updateCatalogueSorting(sort);
  }

  toggleSortMenu(isOpen) {
    let state = this.state;
    state.sortMenuOpen = isOpen;
    this.setState(state);
  }

  toggleOrder() {
    let state = this.state;
    state.ascending = !state.ascending
    this.setState(state);
  }

  toggleSearchBar(isOpen) {
    let state = this.state;
    state.mobileSearchShowed = isOpen;
    if(!isOpen){
      this.mobileSearchInput.value = '';
      this.props.actions.fetchProducts();
    }
    this.setState(state);
  }

  handleSearch = (event) => {
    const value = event.target.value;
    if (value.length > 2) {
      this.isSearchActive = true;
      this.props.actions.fetchProducts(value);
    } else if (this.isSearchActive) {
      this.isSearchActive = false;
      this.props.actions.fetchProducts();
    }
  }

  prefetchProduct = (event) => {
    this.props.actions.fetchProduct(event.target.dataset.url, true);
  }

  toggleFilter(value) {
    const { toggleFilter } = this.props.actions;
    const ss = sessionStorage;
    if(ss){
      ss.setItem("filter", true);
    }
    toggleFilter(value);
  }

  resetFilters() {
    const { updateFilters } = this.props.actions;
    //updateFilters(['Season_1c421836-fdea-11e1-99a4-00259034376e']);
    updateFilters(['Season_1c421837-fdea-11e1-99a4-00259034376e']);
  }

  isDefaultFiltersState(){
    //return _.isEqual(this.props.app.activeFilters, ['Season_1c421836-fdea-11e1-99a4-00259034376e']);
    return _.isEqual(this.props.app.activeFilters, ['Season_1c421837-fdea-11e1-99a4-00259034376e']);
  }

  toggleFiltersWindow(isShowed) {
    let state = this.state;
    state.mobileFiltersShowed = isShowed;
    this.setState(state);
  }

  render() {
    const { app, user, products } = this.props;
    const { filters } = this.props.filters;
    const { sort } = this.props.filters;
    const { pagewidth } = app;

    // images
    let headerBanner = '';

    let catalogueBanners = [];
    let ascProducts = [];
    let productsToShow = [];
    let productsInGrid = undefined;

    const preloading = !filters.length;

    const sliderSettings = {
      dots: true,
      fade: true,
      prevArrow: <button type="button" className="slick-arrow slick-prev"><svg className="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" strokeMiterlimit="10"><path d="M7.707.354l-7 7 7 7M.707 7.354h18" vectorEffect="non-scaling-stroke" /></g></svg></button>,
      nextArrow: <button type="button" className="slick-arrow slick-next"><svg className="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" strokeMiterlimit="10"><path d="M11 14.354l7-7-7-7M18 7.354H0" vectorEffect="non-scaling-stroke" /></g></svg></button>,
    };


    if (!preloading) {
      if(this.headerBanner.img === ''){
        this.headerBanner = this.getRandomBanner(this.props.filters.banners.catalogue_header);
      }
      catalogueBanners = this.props.filters.banners.catalogue;
      ascProducts = products.slice(0).reverse();
      productsToShow = (this.state.ascending) ? ascProducts : products;

      // get correct images
      headerBanner = getImageSize(pagewidth, this.headerBanner.img, 'shortBanner');

      productsInGrid = <div className="catalogue-grid">
                    {productsToShow.length ? productsToShow.slice(0, 6).map((product, index) =>
                      <div className="product-tile" key={index}>
                        <Link className="photo" to={`/catalogue/product/${product.URLName}`}>
                          <LazyLoad height={300} offset={600} once>
                            <img
                              src={product.Img.ThumbUrl}
                              alt=""
                              data-url={product.URLName}
                              onMouseEnter={this.prefetchProduct}
                            />
                          </LazyLoad>
                          {(product.Discount && product.Discount.Value > 0) ? <span className="discount-value">-{product.Discount.Value}%</span> : null}
                        </Link>
                        <div className="info">
                          <div className="name">{product.Name}</div>
                          {' '}
                          {user.AccountTYPE !== 'wholesale' && app.wholesaleMode ? (
                            <div className="price">
                              <span className="currency">Войдите в B2B портал</span>
                            </div>
                          ) : (
                            <div className="price">
                              {(product.Discount && product.Discount.Value > 0) ? <span className="old-sum">{product.Discount.PriceOld}</span> : null}
                              <span className="sum">{product.Price}</span>
                              {' '}
                              <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                            </div>
                          )}
                        </div>
                      </div>
                    ) : null}

                    <MediaQuery query='(min-width: 769px)'>
                      <Slider {...sliderSettings}>
                        {catalogueBanners.map((banner, index) =>
                          <div className="photo" key={index}>
                            <Link to={banner.url}>
                              <img src={`${banner.img}`} alt="" />
                            </Link>
                          </div>
                        )}
                      </Slider>
                    </MediaQuery>

                    {productsToShow.length ? productsToShow.slice(6).map((product, index) =>
                      <div className="product-tile" key={index}>
                        <Link className="photo" to={`/catalogue/product/${product.URLName}`}>
                          <LazyLoad height={300} offset={600} once>
                            <img
                              src={product.Img.ThumbUrl}
                              alt=""
                              data-url={product.URLName}
                              onMouseEnter={this.prefetchProduct}
                            />
                          </LazyLoad>
                          {(product.Discount && product.Discount.Value > 0) ? <span className="discount-value">-{product.Discount.Value}%</span> : null}
                        </Link>
                        <div className="info">
                          <div className="name">{product.Name}</div>
                          {' '}
                          {user.AccountTYPE !== 'wholesale' && app.wholesaleMode ? (
                            <div className="price">
                              <span className="currency">Войдите в B2B портал</span>
                            </div>
                          ) : (
                            <div className="price">
                              {(product.Discount && product.Discount.Value > 0) ? <span className="old-sum">{product.Discount.PriceOld}</span> : null}
                              <span className="sum">{product.Price}</span>
                              {' '}
                              <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                            </div>
                          )}
                        </div>
                      </div>
                    ) : null}
                  </div>;

    }


    return (
      <section className="page-content">
        {preloading ? (
          <Preloader />
        ) : (
          <div className="content-wrapper">
            <Link
              className="catalogue-header-banner full-width"
              to={this.headerBanner.url}
              style={{
                backgroundImage: `url(${headerBanner})`,
              }}
            />

            <div className="catalogue">
              <MediaQuery query='(min-width: 769px)'>
                <div className="catalogue-controls">
                  <div className="view">
                    <button
                      className={classNames({
                        'view-button': true,
                        active: app.catalogueView === 'grid',
                      })}
                      onClick={() => this.changeView('grid')}
                    >
                      <svg className="icon svg-icon-catalogue-grid"><use xlinkHref="/assets/images/sprite.svg#icon-catalogue-grid" /></svg>
                    </button>
                    <button
                      className={classNames({
                        'view-button': true,
                        active: app.catalogueView === 'list',
                      })}
                      onClick={() => this.changeView('list')}
                    >
                      <svg className="icon svg-icon-catalogue-list"><use xlinkHref="/assets/images/sprite.svg#icon-catalogue-list" /></svg>
                    </button>
                    {' '}
                    <div className="sorting">
                      <span className="text">Сортировать </span>
                      <div className="drop-down" onMouseEnter={() => {this.toggleSortMenu(true)}} onMouseLeave={() => {this.toggleSortMenu(false)}}>
                        <span className="selected">{sort.find((elem) => {return elem.id === app.sort}).name.ru}</span>
                        <div className={classNames("types", {"showed": this.state.sortMenuOpen})}>
                          { sort.map((elem, index) =>
                              <span key={index} onClick={() => this.changeSorting(elem.id)} className={classNames("type", {"active": elem.id === app.sort})}>{elem.name.ru}</span>
                            )}
                        </div>
                      </div>
                      <span className={classNames("arrow", {'ascending': this.state.ascending})} onClick={this.toggleOrder.bind(this)}>
                        <svg className="svg-sorting-arrow-down"><use xlinkHref="/assets/images/sprite.svg#sorting-arrow-down" /></svg>
                        <svg className="svg-sorting-arrow-up"><use xlinkHref="/assets/images/sprite.svg#sorting-arrow-up" /></svg>
                      </span>
                    </div>
                  </div>
                  {' '}
                  <div className="search">
                    <svg className="icon svg-icon-search"><use xlinkHref="/assets/images/sprite.svg#icon-search" /></svg>
                    <input
                      className="input"
                      type="text"
                      placeholder="Поиск"
                      onChange={this.handleSearch}
                    />
                  </div>
                </div>
              </MediaQuery>
              <MediaQuery query='(max-width: 768px)'>
                <div className="catalogue-controls">
                  <div className="sorting">
                    <svg className={classNames("icon svg-icon-sort", {
                      'flipped': this.state.ascending
                    })} onClick={() => {this.toggleSortMenu(true)}}><use xlinkHref="/assets/images/sprite.svg#icon-sort" /></svg>
                    <div className="drop-down">
                      <div className={classNames("types", {"showed": this.state.sortMenuOpen})}>
                        { sort.map((elem, index) =>
                            <span key={index} onClick={() => this.changeSorting(elem.id)} className={classNames("type", {"active": elem.id === app.sort})}>
                              <svg className="icon svg-icon-filter-check">
                                <use xlinkHref="/assets/images/sprite.svg#icon-filter-check-mobile"/>
                              </svg>
                              {elem.name.ru}
                              <span className={classNames("arrow", {'ascending': this.state.ascending})} onClick={this.toggleOrder.bind(this)}>
                                <svg className="svg-sorting-arrow-down"><use xlinkHref="/assets/images/sprite.svg#sorting-arrow-down-mobile" /></svg>
                                <svg className="svg-sorting-arrow-up"><use xlinkHref="/assets/images/sprite.svg#sorting-arrow-up-mobile" /></svg>
                              </span>
                            </span>
                          )}
                      </div>
                    </div>
                  </div>
                  <div className={classNames("search", {'active': this.state.mobileSearchShowed})}>
                    <svg className="icon svg-icon-search" onClick={() => this.toggleSearchBar(true)}><use xlinkHref="/assets/images/sprite.svg#icon-search-mobile" /></svg>
                    <div className="bar">
                      <svg className="icon svg-icon-cross" onClick={() => this.toggleSearchBar(false)}><use xlinkHref="/assets/images/sprite.svg#icon-cross-mob" /></svg>
                      <input
                        className="mobile-input"
                        type="text"
                        placeholder="Поиск"
                        ref={(input) => {this.mobileSearchInput = input;}}
                        onChange={this.handleSearch}
                      />
                    </div>
                  </div>
                  <div className="filters-counter" onClick={() => this.toggleFiltersWindow(true)}>
                    <i>фильтры</i>
                    {app.activeFilters.length !== 0 &&
                      <span>{app.activeFilters.length}</span>
                    }
                  </div>
                </div>
              </MediaQuery>

              <div className={classNames("catalogue-filter", {"active": this.state.mobileFiltersShowed})}>

                <div className={classNames("filter-group", "reset-filters", {"hidden": this.isDefaultFiltersState()})}>
                  <div className="filter-group-title" onClick={() => this.resetFilters()}>
                    <span className="text">Сбросить все фильтры</span>
                    <svg className="svg-icon-plus"><use xlinkHref="/assets/images/sprite.svg#icon-plus" /></svg>
                    <svg className="svg-icon-plus icon-mobile"><use xlinkHref="/assets/images/sprite.svg#icon-plus-mobile" /></svg>
                  </div>
                </div>
                {filters.length ? filters.map((group, groupIndex) =>
                  <div
                    className={classNames({
                      'filter-group': true,
                      active: group.Active,
                    })}
                    key={`filter-group-${groupIndex}`}
                  >
                    <div className="filter-group-title">
                      <span className="text js-toggle-filter-group">{group.Name.ru}</span>
                      <button className="toggle-group-button js-toggle-filter-group">
                        <svg className="svg-sorting-arrow-down"><use xlinkHref="/assets/images/sprite.svg#sorting-arrow-down-mobile" /></svg>
                        <svg className="svg-sorting-arrow-up"><use xlinkHref="/assets/images/sprite.svg#sorting-arrow-up-mobile" /></svg>
                      </button>

                    </div>
                    {group.Values.map((filter, filterIndex) => {
                      return (<div
                        className={classNames({
                          'filter-item': true,
                          active: app.activeFilters.find(f => f === filter.Value),
                        })}
                        onClick={() => this.toggleFilter(filter.Value)}
                        key={`filter-item-${filterIndex}`}
                      >
                        <span className="text">{filter.Name.ru}</span>
                        <svg className="svg-icon-plus"><use xlinkHref="/assets/images/sprite.svg#icon-plus" /></svg>
                      </div>)
                    })}
                  </div>
                ) : null}
                <div className="confirm" onClick={() => this.toggleFiltersWindow(false)}>Применить фильтры</div>
              </div>

              <div className="catalogue-view">
                <MediaQuery query='(max-width: 768px)'>
                  {productsInGrid}
                </MediaQuery>

                <MediaQuery query='(min-width: 769px)'>
                  {app.catalogueView === 'grid' ? (
                    productsInGrid
                  ) : null}

                  {app.catalogueView === 'list' ? (
                    <table className="table catalogue-list">
                      <tbody className="table-body">
                        {
                          productsToShow.length > 0 &&
                          productsToShow.map(p => (
                            [p].concat([]).map((product, index) =>
                              <tr className="row" key={index}>
                                <td className="cell image-cell">
                                  <Link className="image" to={`/catalogue/product/${product.URLName}`}>
                                    <LazyLoad height={120} offset={600} once>
                                      <img
                                        src={product.Img.ThumbUrl}
                                        alt=""
                                        data-url={product.URLName}
                                        onMouseEnter={this.prefetchProduct}
                                      />
                                    </LazyLoad>
                                  </Link>
                                </td>
                                <td className="cell">
                                  <div className="name">{product.Name}</div>
                                </td>
                                <td className="cell">
                                  <div className="vendor-code">{product.Article}</div>
                                  {
                                    product.Color && product.Color.Name && product.Color.Name.ru &&
                                    <div className="color">{product.Color.Name.ru}</div>
                                  }
                                </td>
                                <td className="cell">
                                  {user.AccountTYPE !== 'wholesale' && app.wholesaleMode ? (
                                    <div className="price">
                                      <span className="currency">Войдите в B2B портал</span>
                                    </div>
                                  ) : (
                                    <div className="price">
                                      {(product.Discount && product.Discount.Value > 0) ? <span className="old-sum">{product.Discount.PriceOld}</span> : null}
                                      <span className="sum">{product.Price}</span>
                                      {' '}
                                      <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                                    </div>
                                  )}
                                </td>
                                <td className="cell">
                                  <NumberInput id={`quantity-${product.Id}`} />
                                </td>
                                <td className="cell">
                                  <button
                                    className="button-basket"
                                    onClick={() => { this.addToBasket(product.Id); }}
                                  >
                                    <svg className="arrow svg-icon-basket-add"><use xlinkHref="/assets/images/sprite.svg#icon-basket-add" /></svg>
                                  </button>
                                </td>
                              </tr>
                            )
                          ))
                        }
                      </tbody>
                    </table>
                  ) : null}
                </MediaQuery>
              </div>
            </div>
          </div>
        )}
      </section>
    );
  }
}

function mapStateToProps(state) {
  const { app, filters, user, product, products } = state;
  return {
    app,
    filters,
    user,
    product,
    products,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Catalogue);
