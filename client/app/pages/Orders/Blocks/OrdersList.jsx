import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

export default class OrdersList extends Component {
  static propTypes = {
    orders: PropTypes.array,
    wholesale: PropTypes.bool,
  }

  render() {
    const { orders, wholesale } = this.props;

    return (
      <table className="table striped-table history-table">
        <thead className="table-head">
          <tr className="row">
            <td className="cell">№ заказа</td>
            <td className="cell">Дата</td>
            <td className="cell">Сумма</td>
            { wholesale ? (
              <td className="cell">Вес, кг</td>
            ) : null}
            { wholesale ? (
              <td className="cell">Объём, м³</td>
            ) : null}
            { wholesale ? (
              <td className="cell">Количество мест</td>
            ) : null}
            <td className="cell">Статус оплаты</td>
            <td className="cell">Состояние заказа</td>
          </tr>
        </thead>
        <tbody className="table-body">
          {orders.map((order, index) => (
            <tr className="row" key={index}>
              <td className="cell">
                <div className="order-number">
                  <Link to={`/orders/${order.Id}`}>{order.Number}</Link>
                </div>
              </td>
              <td className="cell">
                <div className="date">{order.Date}</div>
              </td>
              <td className="cell">
                <div className="price">{order.TotalSumm} <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span></div>
              </td>
              { wholesale ? (
                <td className="cell">
                  <div className="weight">{order.TotalWeight}</div>
                </td>
              ) : null}
              { wholesale ? (
                <td className="cell">
                  <div className="volume">{order.TotalVolume}</div>
                </td>
              ) : null}
              { wholesale ? (
                <td className="cell">
                  <div className="places">{order.TotalPackage}</div>
                </td>
              ) : null}
              <td className="cell">
                <div className="payment">{order.PayStatus.ru}</div>
              </td>
              <td className="cell">
                <div className="status">{order.OrderStatus}</div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}
