import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

export default class OrdersList extends Component {
  static propTypes = {
    orders: PropTypes.array,
    wholesale: PropTypes.bool,
  }

  render() {
    const { orders, wholesale } = this.props;

    return (
      <div className="orders-history-list">
        {orders.map((order, index) => (
          <div key={index} className="order">
            <div className="column">
              <div className="order-number">
                <Link to={`/orders/${order.Id}`}>{`#${order.Number}`}</Link>
              </div>
              <div className="date">{order.Date}</div>
            </div>
            <div className="column">
              <div className="price">{order.TotalSumm} <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span></div>
              { wholesale ? (
                <div className="wholesale-info">
                  <p>{order.TotalWeight} кг</p>
                  <p>{order.TotalVolume} м3</p>
                  <p>{order.TotalPackage} мест</p>
                </div>
              ) : null}
            </div>
            <div className="column">
              <div className={classNames("payment", {
                'not-paid': order.PayStatus.ru == 'Не оплачено'
              })}>{order.PayStatus.ru}</div>
              <div className="status">{order.OrderStatus}</div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
