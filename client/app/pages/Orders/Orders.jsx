import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import MediaQuery from 'react-responsive';
import Preloader from 'app/components/Preloader';
import OrdersList from 'app/pages/Orders/Blocks/OrdersList';
import OrdersListMobile from 'app/pages/Orders/Blocks/OrdersListMobile';

import api from 'app/actions/api';

export class Orders extends Component {
  constructor(props) {
    super(props);

    this.state = {
      orders: null,
    };
  }

  componentDidMount() {
    const { user } = this.props;

    if (user && user.AccountUUID !== 0) {
      api('orders', {
        AccountUUID: user.AccountUUID,
        beginPeriod: '01.01.1970',
        endPeriod: '01.01.2222',
      }).then((orders) => {
        this.setState({ orders });
      });
    }
  }

  render() {
    const { orders } = this.state;
    const wholesale = this.props.user.AccountTYPE === 'wholesale';

    return (
      <section className="page-content">
        <div className="content-wrapper">
          <h1>История заказов</h1>
          {orders ? (
            <div>
              {orders.length ? (
                <div>
                  <MediaQuery query='(max-width: 768px)'>
                    <OrdersListMobile orders={orders} wholesale={wholesale} />
                  </MediaQuery>
                  <MediaQuery query='(min-width: 769px)'>
                    <OrdersList orders={orders} wholesale={wholesale} />
                  </MediaQuery>
                </div>
              ) : (
                <div className="empty-page">
                  <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                  <div className="title">У вас пока нет заказов</div>
                  <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                </div>
              )}
            </div>
          ) : <Preloader />}
        </div>
      </section>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state;
  return {
    user,
  };
}

export default connect(mapStateToProps)(Orders);
