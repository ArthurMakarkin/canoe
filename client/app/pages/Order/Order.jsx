import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import classNames from 'classnames';

import MediaQuery from 'react-responsive';

import OrderBlock from 'app/pages/Order/Blocks/OrderBlock';
import OrderBlockMobile from 'app/pages/Order/Blocks/OrderBlockMobile';
import Preloader from 'app/components/Preloader';

import api from 'app/actions/api';

export class Order extends Component {
  constructor(props) {
    super(props);

    this.state = {
      order: null,
    };
  }

  componentDidMount() {
    const { user } = this.props;

    if (user && user.AccountUUID !== 0) {
      api('order', {
        AccountUUID: user.AccountUUID,
        OrderUUID: this.props.params.id,
      }).then((order) => {
        this.setState({ order });
      });
    }
  }

  render() {
    const { order } = this.state;
    const wholesale = this.props.user.AccountTYPE === 'wholesale';

    const options = {
      order: order,
      wholesale: wholesale,
    }

    return (
      <section className="page-content">
        {order ? (
          <div>
            <MediaQuery query='(min-width: 769px)'>
              <OrderBlock {...options} />
            </MediaQuery>
            <MediaQuery query='(max-width: 768px)'>
              <OrderBlockMobile {...options} />
            </MediaQuery>
          </div>
        ) : <Preloader />}
      </section>
    );
  }
}

function mapStateToProps(state) {
  const { user, orders } = state;
  return {
    user,
    orders,
  };
}

export default connect(mapStateToProps)(Order);
