import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

export default class OrderBlock extends Component {
  static propTypes = {
    order: PropTypes.object,
    wholesale: PropTypes.bool,
  }

  render() {
    const { order, wholesale } = this.props;

    return (
      <div className="content-wrapper">
        <h1>Заказ № {order.Number} от {order.Date}</h1>
        <table className="table order-table">
          <thead className="table-head">
            <tr className="row">
              <td className="cell" />
              <td className="cell">Название</td>
              <td className="cell">Артикул</td>
              <td className="cell">Цвет</td>
              <td className="cell">Размер</td>
              <td className="cell">Цена</td>
              <td className="cell">Количество</td>
              <td className="cell">Сумма</td>
            </tr>
          </thead>
          <tbody className="table-body">
            {order.BasketList.map((product, index) => (
              <tr className="row" key={index}>
                <td className="cell image-cell">
                  <Link className="image" to={`/catalogue/product/${product.Product.URLName}`}>
                    <img src={product.Product.Img.ThumbUrl} alt="" />
                  </Link>
                </td>
                <td className="cell">
                  <div className="name">{product.Product.Name}</div>
                </td>
                <td className="cell">
                  <div className="vendor-code">{product.Product.Article}</div>
                </td>
                <td className="cell">
                  <div className="color">{product.Product.Color.Name.ru}</div>
                </td>
                <td className="cell">
                  <div className="size">{product.Product.Size}</div>
                </td>
                <td className="cell">
                  <div className="price">
                    <span className="sum">{product.Price}</span>
                    {' '}
                    <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                  </div>
                </td>
                <td className="cell">
                  <span className="quantity">{product.Quantity}</span>
                </td>
                <td className="cell">
                  <div className="price">
                    <span className="sum">{product.Summ}</span>
                    {' '}
                    <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                  </div>
                </td>
              </tr>
            ))}
            <tr className="basket-info">
              <td className="cell" colSpan="3">
                <Link className="back-link" to="/orders">
                  <svg className="arrow svg-arrow-left"><use xlinkHref="/assets/images/sprite.svg#arrow-left" /></svg>
                  <span className="text">Все заказы</span>
                </Link>
              </td>
              <td className="cell" colSpan="2">
                <div className="price-label">Общая стоимость</div>
              </td>
              <td className="cell">
                <div className={classNames({
                  price: true,
                  'full-price': true,
                })}>
                  <span className="sum">{order.TotalSumm}</span>
                  {' '}
                  <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                </div>
              </td>
              <td className="cell" colSpan="2" />
            </tr>
            {wholesale ? (
              <tr className="basket-info">
                <td className="cell" colSpan="3" />
                <td className="cell" colSpan="2">
                  <div className="price-label">Вес</div>
                </td>
                <td className="cell">
                  <div className="price full-price">
                    <span className="sum">{order.TotalWeight}</span>
                    {' '}
                    <span className="currency">кг</span>
                  </div>
                </td>
                <td className="cell" colSpan="2" />
              </tr>
            ) : null}
            {wholesale ? (
              <tr className="basket-info">
                <td className="cell" colSpan="3" />
                <td className="cell" colSpan="2">
                  <div className="price-label">Объём</div>
                </td>
                <td className="cell">
                  <div className="price full-price">
                    <span className="sum">{order.TotalVolume}</span>
                    {' '}
                    <span className="currency">м³</span>
                  </div>
                </td>
                <td className="cell" colSpan="2" />
              </tr>
            ) : null}
          </tbody>
        </table>
      </div>
    );
  }
}
