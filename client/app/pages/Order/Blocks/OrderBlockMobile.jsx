import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router';

export default class OrderBlockMobile extends Component {
  static propTypes = {
    order: PropTypes.object,
    wholesale: PropTypes.bool,
  }

  render() {
    const { order, wholesale } = this.props;

    return (
      <div className="content-wrapper mobile-history">
        <h1 className="mobile-align-left">Заказ № {order.Number} от {order.Date}</h1>
        {
          order.OrderStatus &&
          <p className="order-status">{order.OrderStatus}</p>
        }
        <p className={classNames('payment-status', {
          'not-paid': order.PayStatus.ru == 'Не оплачен'
        })}>{order.PayStatus.ru}</p>
        <div className="mobile-products mobile-basket">
          <div className="products">
            {order.BasketList.map((product, index) => (
              <div className="mobile-product" key={index}>
                <Link className="image" to={`/catalogue/product/${product.Product.URLName}`}>
                  <img src={product.Product.Img.ThumbUrl} alt="" />
                </Link>
                <div className="info">
                  <span className="name">{product.Product.Name}</span>
                  <span className="vendor-code">Арт. {product.Product.Article}</span>
                  <span className="color">{product.Product.Color.Name.ru}</span>
                  <span className="size">{product.Product.Size}</span>
                </div>
                {/*
                  <div className="price">
                    <span className="sum">{product.Price}</span>
                    {' '}
                    <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                  </div>
                */}
                <div className="quantity">
                  <span>{product.Quantity} {product.Product.Unit.Name.ru}</span>
                </div>
                <div className="price">
                  <span className="sum">{product.Summ}</span>
                  {' '}
                  <span className="currency"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
                </div>
              </div>
            ))}
          </div>
          <div className="basket-info">
            <label className="price-label">Общая стоимость</label>
            <div className={classNames({
              price: true,
              'full-price': true,
            })}
            >
              <span className="sum">{order.TotalSumm}</span>
              {' '}
              <span className="currency currency-big"><svg className="icon icon-ruble"><use xlinkHref="/assets/images/sprite.svg#icon-ruble" /></svg></span>
            </div>
          </div>
          {wholesale ? (
            <div className="basket-info">
              <label>Вес</label>
              <div className="price">
                <span className="sum">{order.TotalWeight}</span>
                {' '}
                <span className="currency">кг</span>
              </div>
            </div>
          ) : null}
          {wholesale ? (
            <div className="g-info">
              <label>Объём</label>
              <div className="price">
                <span className="sum">{order.TotalVolume}</span>
                {' '}
                <span className="currency">м³</span>
              </div>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}
