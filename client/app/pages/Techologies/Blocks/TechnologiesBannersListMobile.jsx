import React from 'react';
import { Link } from 'react-router';
import MobileGallery from 'app/components/MobileGallery';

const technologies = [
  {
    Image: {
      Url: "//media.canoe.ru/images/technologies/mobile/teflon-mini.jpg"
    },
    Title: "Teflon<sup>TM</sup>",
    Text: "<p class='subtitle'>Водоотталкивающая защита</p><p>Борьба со стихией на молекулярном уровне: выводит влагу наружу, недоспускает намокания и попадания грязи на поверхность.</p><a class='main-button white' href='/technologies/teflon'>Подробнее</a>",
    bg: '#6671a6',
  },
  {
    Image: {
      Url: "//media.canoe.ru/images/technologies/mobile/h2dry-mini.jpg"
    },
    Title: "H2DRY<sup>TM</sup>",
    Text: "<p class='subtitle'>Концепция дышашей<br />шерсти</p><p>Свежесть и комфорт: супердышащий материал, c естественной терморегруляцией. Ощущение свежести и комфорта даже при резких перепадах температуры.</p><a class='main-button white' href='/technologies/h2dry'>Подробнее</a>",
    bg: '#5ac1f0',
  },
  {
    Image: {
      Url: "//media.canoe.ru/images/technologies/mobile/high-twist-mini.jpg"
    },
    Title: "High Twist",
    Text: "<p class='subtitle'>Пряжа высокой крутки</p><p>Всегда в идеальной форме: головной убор не мнется, восстанавливает форму, не растягиваться со временем, не подвержен пилингу и прост в уходе.</p><a class='main-button white' href='/technologies/high-twist'>Подробнее</a>",
    bg: '#be132b',
  },
  {
    Image: {
      Url: "//media.canoe.ru/images/technologies/mobile/3d-tube-mini.jpg"
    },
    Title: "3D TUBE",
    Text: "<p class='subtitle'>Воздушная пряжа</p><p>Головной убор на 35% легче, чем из традиционной шерсти. Революция в производстве воздушных пряж для супер-нежных и чувствительных материалов.</p><a class='main-button white' href='/technologies/3d-tube'>Подробнее</a>",
    bg: '#e6c5c2',
  },
  {
    Image: {
      Url: "//media.canoe.ru/images/technologies/mobile/nanosilver-mini.jpg"
    },
    Title: "Nano Silver",
    Text: "<p class='subtitle'>Серебряная защита от бактерий</p><p>Головные уборы с наночастицами серебра — проверенная защита от бактерий и нежелательного запаха даже в экстремальных ситуациях.</p><a class='main-button white' href='/technologies/nano-silver'>Подробнее</a>",
    bg: '#a4b2ba',
  },
]

const TechnologiesBannersListMobile = () => (
  <MobileGallery name="technologies" showPage={1} colors={true} content={technologies} />
);

export default TechnologiesBannersListMobile;
