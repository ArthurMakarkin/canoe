import React from 'react';
import { Link } from 'react-router';


const TechnologiesBannersListDesktop = () => (
  <div className="technologies-banners full-width">
    <div className="technologie-banner">
      <div className="description" style={{ backgroundColor: '#6671a6' }}>
        <div className="content">
          <div className="title">
            <strong>Teflon <sup>TM</sup></strong><br />
            Водоотталкивающая защита
          </div>
          <div className="text">Борьба со стихией на молекулярном уровне: выводит влагу наружу, недоспускает намокания и попадания грязи на поверхность.</div>
          <Link className="main-button white" to="/technologies/teflon">Подробнее</Link>
        </div>
      </div>
      <div className="image" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/teflon-mini.jpg)' }} />
    </div>
    <div className="technologie-banner">
      <div className="image" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/h2dry-mini.jpg)' }} />
      <div className="description" style={{ backgroundColor: '#5ac1f0' }}>
        <div className="content">
          <div className="title">
            <strong>H2DRY <sup>TM</sup></strong><br />
            Концепция дышашей шерсти
          </div>
          <div className="text">Свежесть и комфорт: супердышащий материал, c естественной терморегруляцией. Ощущение свежести и комфорта даже при резких перепадах температуры.</div>
          <Link className="main-button white" to="/technologies/h2dry">Подробнее</Link>
        </div>
      </div>
    </div>
    <div className="technologie-banner">
      <div className="description" style={{ backgroundColor: '#be132b' }}>
        <div className="content">
          <div className="title">
            <strong>High Twist</strong><br />
            Пряжа высокой крутки
          </div>
          <div className="text">Всегда в идеальной форме: головной убор не мнется, восстанавливает форму, не растягиваться со временем, не подвержен пилингу и прост в уходе.</div>
          <Link className="main-button white" to="/technologies/high-twist">Подробнее</Link>
        </div>
      </div>
      <div className="image" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist-mini.jpg)' }} />
    </div>
    <div className="technologie-banner">
      <div className="image" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/3d-tube-mini.jpg)' }} />
      <div className="description" style={{ backgroundColor: '#e6c5c2' }}>
        <div className="content">
          <div className="title">
            <strong>3D TUBE</strong><br />
            Воздушная пряжа
          </div>
          <div className="text">Головной убор на 35% легче, чем из традиционной шерсти. Революция в производстве воздушных пряж для супер-нежных и чувствительных материалов.</div>
          <Link className="main-button white" to="/technologies/3d-tube">Подробнее</Link>
        </div>
      </div>
    </div>
    <div className="technologie-banner">
      <div className="description" style={{ backgroundColor: '#a4b2ba' }}>
        <div className="content">
          <div className="title">
            <strong>Nano Silver</strong><br />
            Серебряная защита от бактерий
          </div>
          <div className="text">Головные уборы с наночастицами серебра — проверенная защита от бактерий и нежелательного запаха даже в экстремальных ситуациях.</div>
          <Link className="main-button white" to="/technologies/nano-silver">Подробнее</Link>
        </div>
      </div>
      <div className="image" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/nanosilver-mini.jpg)' }} />
    </div>
  </div>
);

export default TechnologiesBannersListDesktop;
