import React, { Component } from 'react';
import Scroll from 'react-scroll';
import MediaQuery from 'react-responsive';
import MobileGallery from 'app/components/MobileGallery';

import SimilarProducts from 'app/components/SimilarProducts';
import * as filters from 'app/constants/Filters';

import initScrollReveal from 'app/animations/technologies';

export default class ThreeDTube extends Component {
  componentDidMount() {
    initScrollReveal();
  }

  play(){
    document.getElementById('video').play();
    document.getElementById('icons').classList.remove('hidden');
    document.getElementById('playBtn').classList.add('hidden');
  }

  render() {
    const TubeGallery = [
      {
        Image: {
          Url: "//media.canoe.ru/images/technologies/3d-tube/mobile/dulsinea.jpg"
        },
        Title: "Лёгкая нежность",
        Text: "<p class='subtitle'>22% мохер, 60% вискоза,<br />18% полиамид</p><p>Изумительно легкая и воздушная пряжа из мохера первой стрижки и вискозы. Гипнотизирующая своей нежностью текстура и эластичный материал для идеальной посадки.</p>",
      },
      {
        Image: {
          Url: "//media.canoe.ru/images/technologies/3d-tube/mobile/hat.jpg"
        },
        Title: "Лёгкая нежность",
        Text: "<p class='subtitle'>22% мохер, 60% вискоза,<br />18% полиамид</p><p>Изумительно легкая и воздушная пряжа из мохера первой стрижки и вискозы. Гипнотизирующая своей нежностью текстура и эластичный материал для идеальной посадки.</p>",
      },
    ]

    return (
      <section className="page-content technology">
        <div className="content-wrapper">
          <MediaQuery query='(max-width: 768px)'>
            <div>
              <div className="technologie-header full-width" style={{
              backgroundImage: 'url(/assets/images/technologies/mobile/3d-tube-header.jpg)',
              backgroundColor: '#eac4c1',
                }}
              >
              </div>
              <div className="tech-name">
                <p className="title">3D Tube</p>
                <p className="subtitle">самая нежная и воздушная пряжа<br />в изысканных моделях</p>
              </div>
              <div className="video 3d-tube">
                <video id="video" src="//media.canoe.ru/videos/3d-tube.mp4" poster="/assets/images/technologies/3d-tube-poster.jpg" loop />
                <svg id="playBtn" className="icon-play" onClick={this.play}><use xlinkHref="/assets/images/sprite.svg#icon-play" /></svg>
                <span id="icons" className="icons hidden" />
              </div>
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-header full-width" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/3d-tube-wide.jpg)',
              backgroundColor: '#eac4c1',
            }}>
              <Scroll.Link className="scroll-link" to="content" smooth offset={-170} duration={1000}>
                <svg className="scroll-icon"><use xlinkHref="/assets/images/sprite.svg#scroll" /></svg>
              </Scroll.Link>
              <div className="video 3d-tube">
                <video src="//media.canoe.ru/videos/3d-tube.mp4" autoPlay loop />
                <span className="icons" />
              </div>
            </div>
          </MediaQuery>

          <div className="technologie-description" id="content" style={{
            backgroundImage: 'url(//media.canoe.ru/images/technologies/decorations/feather-1.svg)',
            backgroundPosition: '100% 0',
          }}>
            <div className="title">Новое поколение головных уборов<br />с воздушной технологией <strong>3D Tube</strong></div>
            <div className="text">Когда события сменяют друг друга, а каждый день преподносит что-то новое,<br />так важно обрести уют и тепло, которые всегда будут с тобой.<br />Воздушно-легкие головные уборы Canoe, изготовленные из самых мягких<br />и шелковистых пряж по технологии 3D Tube, подарят вам<br />непередаваемую атмосферу нежности и заботы — как объятия<br />любимого человека.</div>
          </div>

          <div className="technologie-photos-grid">
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/3d-tube/mobile/image-1.jpg)',
            }} />
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/3d-tube/mobile/dulsinea.jpg)',
            }} />
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/3d-tube/mobile/image-2.jpg)',
            }} />
            <div className="photo w50 h50 no-padding">
              <div className="photo w100 h50" style={{
                backgroundImage: 'url(//media.canoe.ru/images/technologies/3d-tube/mobile/image-3.jpg)',
              }} />
              <div className="photo w100 h50" style={{
                backgroundImage: 'url(//media.canoe.ru/images/technologies/3d-tube/mobile/image-4.jpg)',
              }} />
            </div>
          </div>

          <div className="technologie-trapezium bg-3dtube full-width">
            <i className="bg" />
            <div className="content">
              <div className="title"><strong>3D Tube</strong> — это инновационный процесс  производства  воздушной пряжи</div>
              <div className="text">Технология позволяет создавать пряжу из самой тонкой шерсти, которую практически невозможно обрабатывать традиционным методом кручения. Нежнейшие материалы теперь стали доступны как никогда — при этом они совершенно безопасны и не вызывают аллергии.</div>
            </div>
          </div>

          <div className="technologie-diagrams">
            <div className="row" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/3d-tube-1.png)',
              backgroundPosition: '45% 0',
              padding: '100px 0 70px',
            }}>
              <div className="column">
                <div className="title">Универсальный подход</div>
                <div className="text">Материал 3D Tube гипоаллергенен и подходит абсолютно всем. Можно забыть про непослушные ворсинки и пух: эти головные уборы устойчивы к пилингу и выглядят опрятно даже после нескольких лет носки.</div>
              </div>
              <img src="//media.canoe.ru/images/technologies/diagrams/3d-tube-1.png" className="mobile-diagram" />
              {' '}
              <div className="column">
                <div className="title">Магия цвета</div>
                <div className="text">Изделия с 3D Tube обладают невероятно шелковистой, нежной и гладкой текстурой. Двухцветные нити образуют паттерн, который визуально воспринимается, как необычная объемная структура. Такой 3D-эффект позволяет создавать настоящие цветовые шедевры: глубокие, чувственные оттенки, градиентные переходы.</div>
              </div>
            </div>

            <div className="technologie-trapezium bg-3dtube mobile-trapezium full-width">
              <i className="bg" />
            </div>

            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>

            <div className="row" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/3d-tube-2.png)',
              backgroundPosition: '55% 0',
              padding: '130px 0 90px',
            }}>
              <div className="column">
                <div className="title">Воздушный поцелуй</div>
                <div className="text">Невесомое волокно, облачённый в соблазнительную форму. Наслаждение  и удовольствие от каждого момента обладания - такие ощущения вызывают  изделия созданные по технологии 3D Tube.</div>
              </div>
              <img src="//media.canoe.ru/images/technologies/diagrams/3d-tube-2.png" className="mobile-diagram" />
              {' '}
              <div className="column">
                <div className="title">Эластичность</div>
                <div className="text">Нити, созданные на основе натуральных волокон и сетчатых синтетических трубочек, очень упруги и эластичны. Головные уборы не мнутся, хорошо сохраняют форму и всегда превосходно садятся по голове.</div>
              </div>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div className="technologie-style-banner banner-with-centered-content no-height full-width">
              <img src="//media.canoe.ru/images/technologies/3d-tube/mobile/banner-1.jpg" />
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-style-banner full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/3d-tube/banner-1.jpg)' }} />
          </MediaQuery>

          <div className="technologie-diagram-description full-width" style={{
            backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/3d-tube-3.png)',
            backgroundSize: '1659px auto',
            backgroundPosition: 'calc(50% - 140px) 180px',
            paddingBottom: '5px',
          }}>
            <div className="content">
              <div className="title mobile-show-title">Материал, созданный по технологии<br /><strong>3D Tube</strong>, на 35% легче, чем пряжа, полученная<br />традиционным способом</div>
              <div className="text">Конструкция нити <span className="text-highlight">3D Tube</span> уникальна и позволяет экспериментировать<br />с самыми нежными и чувствительными материалами. За основу берется<br />искусственно созданная сетчатая трубочка из полиамида, в который<br />под давлением вдувается тонкая шерсть или пух.</div>
              <MediaQuery query='(max-width: 768px)'>
                <div style={{ marginTop: '-20px', marginBottom: '20px', width: '100%' }}>
                  <img src="//media.canoe.ru/images/technologies/diagrams/3d-tube-4-mobile.jpg" alt="" />
                </div>
              </MediaQuery>
              <MediaQuery query='(min-width: 769px)'>
                <div style={{ height: '410px' }} />
              </MediaQuery>
              <div className="title">палитра новых ощущений от <strong>3D Tube</strong></div>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <MobileGallery name="technologies-3dtube" content={TubeGallery} />
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-two-photos full-width" style={{ background: '#f3e0df' }}>
              <div className="photos">
                <div className="photo">
                  <img src="//media.canoe.ru/images/technologies/3d-tube/dulsinea.jpg" alt="" />
                  <div className="title">Легкая нежность</div>
                  <div className="subtitle"><span className="text-highlight">22%</span> мохер, <span className="text-highlight">60%</span> вискоза, <span className="text-highlight">18%</span> полиамид</div>
                  <div className="text">Изумительно легкая и воздушная пряжа из мохера первой стрижки и вискозы. Гипнотизирующая своей нежностью текстура и эластичный материал для идеальной посадки.</div>
                </div>Мягкий объем
                <div className="photo">
                  <img src="//media.canoe.ru/images/technologies/3d-tube/hat.jpg" alt="" />
                  <div className="title">Мягкий объем</div>
                  <div className="subtitle"><span className="text-highlight">22%</span> мохер, <span className="text-highlight">60%</span> вискоза, <span className="text-highlight">18%</span> полиамид</div>
                  <div className="text">Эффект толстой, объемной вязки при абсолютно невесомой пряже. Изделия приятно прилегают к голове и выглядят стильно и современно.</div>
                </div>
              </div>
            </div>
          </MediaQuery>

          <MediaQuery query='(max-width: 768px)'>
            <div className="technologie-style-banner banner-with-centered-content full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/3d-tube/mobile/banner-2.jpg)' }}>
              <div className="content text-dark">
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                <div className="text">Изделия Canoe, изготовленные по технологии, окутают вас легкой дымкой тепла и уюта тогда,<br />когда это больше всего необходимо</div>
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              </div>
            </div>
          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-style-banner full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/3d-tube/banner-2.jpg)' }} />
          </MediaQuery>

          <div className="technologie-description last-description">
            <div className="title">Изделия Canoe, изготовленные по технологии <strong>3D Tube</strong>,<br />окутают вас легкой дымкой тепла и уюта тогда,<br />когда это больше всего необходимо.</div>
          </div>

          <SimilarProducts
            filters={filters.THREE_D_TUBE}
            title="Товары с этой технологией"
          />
        </div>
      </section>
    );
  }
}
