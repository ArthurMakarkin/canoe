import React, { Component } from 'react';
import Scroll from 'react-scroll';
import MediaQuery from 'react-responsive';

import SimilarProducts from 'app/components/SimilarProducts';
import * as filters from 'app/constants/Filters';

import initScrollReveal from 'app/animations/technologies';

export default class Teflon extends Component {
  componentDidMount() {
    initScrollReveal();
  }

  play(){
    document.getElementById('video').play();
    document.getElementById('icons').classList.remove('hidden');
    document.getElementById('playBtn').classList.add('hidden');
  }

  render() {
    return (
      <section className="page-content technology">
        <div className="content-wrapper">
          <MediaQuery query='(max-width: 768px)'>
            <div>
              <div className="technologie-header full-width" style={{
                  backgroundImage: 'url(//media.canoe.ru/images/technologies/mobile/teflon-header.jpg)',
                  backgroundColor: '#5a66a3',
                }}
              >
              </div>
              <div className="tech-name">
                <p className="title">Teflon</p>
                <p className="subtitle">Защита от намокания,<br/ >пыли и грязи</p>
              </div>
              <div className="video teflon">
                <video id="video" src="//media.canoe.ru/videos/teflon.mp4" poster="/assets/images/technologies/teflon-poster.jpg" loop />
                <svg id="playBtn" className="icon-play" onClick={this.play}><use xlinkHref="/assets/images/sprite.svg#icon-play" /></svg>
                <span id="icons" className="icons hidden" />
              </div>
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-header full-width" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/teflon-wide.jpg)',
              backgroundColor: '#5a66a3',
            }}>
              <Scroll.Link className="scroll-link" to="content" smooth offset={-170} duration={1000}>
                <svg className="scroll-icon"><use xlinkHref="/assets/images/sprite.svg#scroll" /></svg>
              </Scroll.Link>
              <div className="video teflon">
                <video src="//media.canoe.ru/videos/teflon.mp4" autoPlay loop />
                <span className="icons" />
              </div>
            </div>
          </MediaQuery>

          <div className="technologie-description" id="content" style={{
            backgroundImage: 'url(//media.canoe.ru/images/technologies/decorations/bubbles.svg)',
            backgroundPosition: '-330px -180px',
            paddingBottom: '180px',
          }}>
            <svg className={`decoration svg-decoration-1`}><use xlinkHref={`/assets/images/sprite.svg#decoration-1`} /></svg>
            <div className="title">Новое поколение головных уборов из шерсти<br />с водоотталкивающей технологией<br /><strong>Teflon <sup>TM</sup></strong></div>
            <div className="text">Активный образ жизни даёт энергию и уверенность в собственных силах.<br />Мы делаем открытия и совершаем удивительные поступки,<br />когда у нас есть возможность сосредоточиться на главном, не отвлекаясь<br />на внешние обстоятельства, будь то городской трафик<br />или резкая смена погоды.</div>
          </div>

          <div className="technologie-photos-grid">
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/teflon/mobile/image-1.jpg)',
            }} />
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/teflon/mobile/image-2.jpg)',
            }} />
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/teflon/mobile/image-3.jpg)',
            }} />
            <div className="photo w50 h50 no-padding">
              <div className="photo w100 h50" style={{
                backgroundImage: 'url(//media.canoe.ru/images/technologies/teflon/mobile/image-4.jpg)',
              }} />
              <div className="photo w100 h50" style={{
                backgroundImage: 'url(//media.canoe.ru/images/technologies/teflon/mobile/image-5.jpg)',
              }} />
            </div>
          </div>

          <div className="technologie-trapezium bg-teflon full-width">
            <i className="bg" />
            <div className="content">
              <div className="title"><strong>Контроль</strong> уровня влажности и <strong>защита</strong> от намокания</div>
              <div className="text">Canoe знает цену персональному комфорту и удобству в любых условиях, поэтому мы создали для вас головные уборы с технологией <span className="text-highlight">Teflon<sup>TM</sup></span> — универсальное решение для большого города.</div>
            </div>
          </div>

          <div className="technologie-diagrams">
            <div className="row" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/teflon-1.png)',
              backgroundPosition: '55% 0',
              padding: '250px 0 170px',
            }}>
              <div className="column">
                <div className="title">Защита от осадков</div>
                <div className="text">Поверхность изделия не допускает проникновение жидкости извне. Достаточно встряхнуть шапку – и капли дождя или снега просто скатятся с её поверхности.</div>
              </div>
              <img src="//media.canoe.ru/images/technologies/diagrams/teflon-1.png" className="mobile-diagram" />
              {' '}
              <div className="column">
                <div className="title">Защита от грязи</div>
                <div className="text">Предотвращает проникновение загрязнения внутрь волокна. Пыль и сухая грязь легко удаляются с материала, не оставляя следов.</div>
              </div>
            </div>

            <div className="technologie-trapezium bg-teflon mobile-trapezium full-width">
              <i className="bg" />
            </div>

            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>

            <div className="row" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/teflon-2.png)',
              backgroundPosition: '44% 0',
              padding: '180px 0 50px',
            }}>
              <div className="column">
                <div className="title">Оптимальная температура</div>
                <div className="text">Идеальная терморегуляция обеспечивает комфортную для кожи температуру, а излишки влаги моментально выводятся с поверхности головы.</div>
              </div>
              <img src="//media.canoe.ru/images/technologies/diagrams/teflon-2.png" className="mobile-diagram" />
              {' '}
              <div className="column">
                <div className="title">Усиливает свойства шерсти</div>
                <div className="text">Обработка водоотталкивающим составом усиливает естественные свойства шерсти Cashwool, обеспечивает хорошую износоустойчивость, не влияя при этом на внешний вид и на тактильные свойства изделия.</div>
              </div>
            </div>
          </div>


          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-style-banner banner-with-centered-content full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/teflon/banner-1.jpg)' }} />
          </MediaQuery>
          <MediaQuery query='(max-width: 768px)'>
            <div className="technologie-style-banner banner-with-centered-content full-width">
              <img src="//media.canoe.ru/images/technologies/teflon/mobile/banner-1.jpg" />
            </div>
          </MediaQuery>

          <div className="technologie-diagram-description full-width" style={{
            backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/teflon-3.svg)',
            backgroundPosition: '50% 0',
            padding: '150px 0 480px',
          }}>
            <div className="content">
              <img src="//media.canoe.ru/images/technologies/diagrams/teflon-logo.svg" className="mobile-diagram teflon-logo" />
              <div className="title">Технология <strong>
                Teflon<sup>TM</sup></strong> — защита от влаги, пыли и грязи</div>
              <div className="text">Принцип действия защиты <span className="text-highlight">Teflon<sup>TM</sup></span> прост: вокруг каждого волокна<br />на молекулярном уровне образуется особая оболочка. Расстояние между<br />волокнами становится настолько маленьким, что частицы воды и грязи<br />не могут проникнуть вглубь материала.</div>
              <img src="//media.canoe.ru/images/technologies/diagrams/teflon-diagram.svg" className="mobile-diagram teflon-diagram" />
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div className="technologie-style-banner banner-with-centered-content no-height full-width">
              <img src="//media.canoe.ru/images/technologies/teflon/mobile/banner-2.jpg" />
            </div>
          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-style-banner full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/teflon/banner-2.jpg)' }} />
          </MediaQuery>

          <div className="technologie-description last-description">
            <div className="title">Ощущение естественного <strong>комфорта</strong><br />в любой ситуации? Оставьте эту задачу <strong>Canoe</strong> —<br />думайте о важном.</div>
          </div>

          <SimilarProducts
            filters={filters.TEFLON}
            title="Товары с этой технологией"
          />
        </div>
      </section>
    );
  }
}
