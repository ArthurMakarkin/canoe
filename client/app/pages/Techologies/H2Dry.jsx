import React, { Component } from 'react';
import Scroll from 'react-scroll';
import MediaQuery from 'react-responsive';

import SimilarProducts from 'app/components/SimilarProducts';
import * as filters from 'app/constants/Filters';

import initScrollReveal from 'app/animations/technologies';

export default class H2Dry extends Component {
  componentDidMount() {
    initScrollReveal();
  }

  play(){
    document.getElementById('video').play();
    document.getElementById('icons').classList.remove('hidden');
    document.getElementById('playBtn').classList.add('hidden');
  }

  render() {
    return (
      <section className="page-content technology">
        <div className="content-wrapper">
          <MediaQuery query='(max-width: 768px)'>
            <div>
              <div className="technologie-header full-width" style={{
              backgroundImage: 'url(/assets/images/technologies/mobile/h2dry-header.jpg)',
              backgroundColor: '#cdebf3',
                }}
              >
              </div>
              <div className="tech-name">
                <p className="title">H2DRY</p>
                <p className="subtitle">Концепция дышащей<br />шерсти</p>
              </div>
              <div className="video h2dry">
                <video id="video" src="//media.canoe.ru/videos/h2dry.mp4" poster="/assets/images/technologies/h2dry-poster.jpg" loop />
                <svg id="playBtn" className="icon-play" onClick={this.play}><use xlinkHref="/assets/images/sprite.svg#icon-play" /></svg>
                <span id="icons" className="icons hidden" />
              </div>
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-header full-width" style={{
              backgroundImage: 'url(/assets/images/technologies/h2dry-wide.jpg)',
              backgroundColor: '#cdebf3',
            }}>
              <Scroll.Link className="scroll-link" to="content" smooth offset={-170} duration={1000}>
                <svg className="scroll-icon"><use xlinkHref="/assets/images/sprite.svg#scroll" /></svg>
              </Scroll.Link>
              <div className="video h2dry">
                <video src="/assets/videos/h2dry.mp4" autoPlay loop />
                <span className="icons" />
              </div>
            </div>
          </MediaQuery>

          <div className="technologie-description" id="content" style={{
            backgroundImage: 'url(/assets/images/technologies/decorations/radials-1.svg)',
            backgroundPosition: '50% 70%',
            paddingTop: '180px',
            paddingBottom: '180px',
          }}>
            <img className="mobile-decoration mobile-decoration-top" src="/assets/images/technologies/h2dry/mobile/h2dry-t.svg" />
            <h2>Жизнь в большом городе — это калейдоскоп событий каждый день.<br />Необходимо, чтобы голова оставалась свежей в любой ситуации.<br />Canoe берёт эту миссию на себя.</h2>
            <div className="text">Новое поколение ультрадышащих головных уборов,<br />которые соответствуют вашему ритму жизни.</div>
            <img className="mobile-decoration mobile-decoration-bottom" src="/assets/images/technologies/h2dry/mobile/h2dry-b.svg" />
          </div>

          <div className="technologie-photos-grid">
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(/assets/images/technologies/h2dry/mobile/image-1.jpg)',
            }} />
            <div className="photo w50 h50 no-padding">
              <div className="photo w100 h50 no-padding">
                <div className="photo w50 h100" style={{
                  backgroundImage: 'url(/assets/images/technologies/h2dry/mobile/image-2.jpg)',
                }} />
                <div className="photo w50 h100" style={{
                  backgroundImage: 'url(/assets/images/technologies/h2dry/mobile/image-3.jpg)',
                }} />
              </div>
              <div className="photo w100 h50" style={{
                backgroundImage: 'url(/assets/images/technologies/h2dry/mobile/banner-1.jpg)',
              }} />
            </div>
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(/assets/images/technologies/h2dry/mobile/image-4.jpg)',
            }} />
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(/assets/images/technologies/h2dry/mobile/image-5.jpg)',
            }} />
          </div>

          <div className="technologie-trapezium bg-h2dry full-width">
            <i className="bg" />
            <div className="content">
              <div className="subtitle">Представляем шапку с технологией, которая дышит<br />и твоя голова остается сухой.</div>
            </div>
          </div>

          <div className="technologie-diagrams">
            <div className="row" style={{
              backgroundImage: 'url(/assets/images/technologies/diagrams/h2dry-1.png)',
              backgroundPosition: '50% 0',
              padding: '120px 0 140px',
            }}>
              <div className="column">
                <div className="title">Всегда сухо</div>
                <div className="text">Технология H2Dry позволяет коже головы дышать и моментально выводит влагу с поверхности головы. Кожа всегда остаётся сухой, а причёска — такой, какой вы видите её в зеркале перед выходом из дома.</div>
              </div>
              <img src="//media.canoe.ru/images/technologies/diagrams/h2dry-1.png" className="mobile-diagram" />
              {' '}
              <div className="column">
                <div className="title">Легкий в уходе</div>
                <div className="text">Головные уборы Canoe можно стирать в машинке с другой одеждой. Даже при каждодневном использовании и частой стирке они сохраняют форму и эластичность. Изделия не мнутся и высыхают за 20 минут.</div>
              </div>
            </div>

            <div className="technologie-trapezium bg-h2dry mobile-trapezium full-width">
              <i className="bg" />
            </div>

            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>

            <div className="row" style={{
              backgroundImage: 'url(/assets/images/technologies/diagrams/h2dry-2.png)',
              backgroundPosition: '50% 0',
              padding: '140px 0 50px',
            }}>
              <div className="column">
                <div className="title">Всегда с иголочки</div>
                <div className="text">Мы позаботились об идеальном внешнем виде, головной убор не мнется и восстанавливает свой прежний вид, без малейших следов складок.</div>
              </div>
              <img src="//media.canoe.ru/images/technologies/diagrams/h2dry-2.png" className="mobile-diagram" />
              {' '}
              <div className="column">
                <div className="title">Эластичный от природы</div>
                <div className="text">Обработка H2Dry усиливает естественную эластичность волокон шерсти, что делает пряжу более лёгкой и гибкой. Поэтому все модели головных уборов Canoe невесомы и прекрасно садятся.</div>
              </div>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div className="technologie-style-banner banner-with-centered-content no-height full-width">
              <img src="//media.canoe.ru/images/technologies/h2dry/mobile/banner-1.jpg" />
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-style-banner banner-with-centered-content full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/h2dry/banner-1.jpg)' }} />
          </MediaQuery>


          <div className="technologie-diagram-description full-width">
            <div className="content">
              <div style={{ paddingBottom: '40px' }}>
                <img className="mobile-h2dry-logo" src="//media.canoe.ru/images/technologies/diagrams/h2dry-3.1.png" alt="" />
              </div>
              <div className="text"><span className="text-highlight">H2Dry</span>, уникальная и эксклюзивная обработка,<br />придающая шерсти <span className="text-highlight">Cashwool</span> и натуральным волокнам<br />неповторимые характеристики, которые до сих пор<br />не были достигнуты.</div>
              <div style={{ paddingTop: '40px' }}>
                <img className="mobile-h2dry-diagram" src="//media.canoe.ru/images/technologies/diagrams/h2dry-3.2.svg" alt="" />
              </div>
            </div>
          </div>

          <div className="technologie-style-banner banner-with-centered-content full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/h2dry/mobile/banner-2.jpg)' }}>
            <div className="content" style={{ color: '#000' }}>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <div className="text">H2Dry — всегда на высоте</div>
              <div className="subtext">Технология <span className="text-highlight">H2Dry</span> берёт лучшее у природы<br />и превращает это в совершенство.</div>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
            </div>
          </div>

          <SimilarProducts
            filters={filters.H2DRY}
            title="Товары с этой технологией"
          />
        </div>
      </section>
    );
  }
}
