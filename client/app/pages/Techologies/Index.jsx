import React, { Component } from 'react';
import { Link } from 'react-router';
import MediaQuery from 'react-responsive';

import TechnologiesBannersListDesktop from 'app/pages/Techologies/Blocks/TechnologiesBannersListDesktop';
import TechnologiesBannersListMobile from 'app/pages/Techologies/Blocks/TechnologiesBannersListMobile';
import initScrollReveal from 'app/animations/technologies';
import MaterialsMobile from 'app/components/MaterialsMobile';

const $ = window.jQuery;

export default class Index extends Component {
  state = {
    materials: [],
    technologies: [],
  }

  componentDidMount() {
    initScrollReveal();
    this.getMaterials();
    this.getTechnologies();
  }

  getMaterials = () => {
    fetch('https://service.canoe.ru/pages/materials')
      .then(response => response.json())
      .then(materials => this.setState({ materials: materials.return }));
  }

  getTechnologies = () => {
    fetch('https://service.canoe.ru/pages/technology')
      .then(response => response.json())
      .then(technologies => this.setState({ technologies: technologies.return }));
  }

  render() {
    const options = {
      scrollX: true,
      scrollY: false,
      // snap: '.item',
      // probeType: 3,
      // bounce: false,
      // bounceTime: 0,
      // deceleration: 0,
      // momentum: false,
      // mouseWheel: true,
      click: true,
      preventDefault: false,
      eventPassthrough: true,
    }

    return (
      <section className="page-content">
        <div className="content-wrapper">
          <div className="banner-with-centered-content full-width darkened mobile-no-text" style={{ backgroundImage: 'url(//media.canoe.ru/images/backgrounds/sheeps.jpg)' }}>
            <div className="content">
              <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              <div className="text">Наш подход к инновациям:<br />технологии объединенные с эмоциями</div>
              <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div className="mobile-heading">
              <div className="name">Материалы</div>
            </div>
          </MediaQuery>

          {
            this.state.materials.length > 0 &&
            <div className="full-width b-materials">
              <div className="content-wrapper">
                <div className="text-with-decorations b-materials__header">
                  <svg className="decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
                  <div className="text">
                    Мы смело прокладываем свой собственный путь,
                    <br />
                    и постоянно ищем новые методы работы с лучшими в мире
                    <br />
                    натуральными материалами
                  </div>
                </div>
                <MediaQuery query='(max-width: 768px)'>
                  <MaterialsMobile noLinks={true} materials={this.state.materials} />
                </MediaQuery>
                <MediaQuery query='(min-width: 769px)'>
                  <div className="b-materials__list">
                    {this.state.materials.map((material, index) =>
                      <Link className="b-materials__item" to={material.URL} key={index}>
                        <span className="b-materials__item__icon" style={{ backgroundImage: `url(${material.Icon})` }} />
                        <span className="b-materials__item__title">{material.Title}</span>
                      </Link>
                    )}
                  </div>
                </MediaQuery>
              </div>
            </div>
          }

          <div className="banner-with-text full-width grey">
            <div className="content">
              <h2 className="title">Представляем нано-технологии<br />обработки материалов</h2>
              <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              <div className="text">Природа — источник жизни, наука — кладезь знаний,<br />вместе они превращают естественные свойства шерсти в совершенство.</div>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div>
              <TechnologiesBannersListMobile />

              <div className="mobile-heading">
                <div className="name">Текстильные технологии</div>
              </div>

              <div className="full-width b-materials">
                <div className="content-wrapper">
                  <div className="text-with-decorations b-materials__header">
                    <svg className="decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
                    <div className="text">Инновации могут принимать множество форм,<br />чтобы придать продукту неповторимые тактильные свойства.</div>
                  </div>
                  <MaterialsMobile materials={this.state.technologies} />
                </div>
              </div>
            </div>
          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <div>
              <TechnologiesBannersListDesktop />

              <div className="banner-with-text full-width">
                <div className="content">
                  <h2 className="title">Текстильные технологии</h2>
                  <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
                  <div className="text">Инновации могут принимать множество форм,<br />чтобы придать продукту неповторимые тактильные свойства.</div>
                </div>
                <div className="content-wrapper">
                  <div className="b-materials__list">
                    {this.state.technologies.map((technology, index) =>
                      <Link className="b-materials__item" to={technology.URL} key={index}>
                        <span className="b-materials__item__icon" style={{ backgroundImage: `url(${technology.Icon})` }} />
                        <span className="b-materials__item__title">{technology.Title}</span>
                      </Link>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </MediaQuery>


          <div className="banner-with-centered-content full-width darkened mobile-no-text" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/index.jpg)' }}>
            <div className="content">
              <svg className="decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
              <div className="text">Мы работаем, чтобы вы получали вещи только высшего качества<br />и не соглашались на меньшее. Canoe призван вдохновлять.</div>
              <svg className="decoration svg-decoration-1"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
            </div>
          </div>

          <div className="banner-with-text mobile-showed full-width">
            <div className="content">
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              <h2 className="title">Сотканы из духа свободы</h2>
              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>

              <MediaQuery query='(max-width: 768px)'>
                <p className="text">Мы работаем, чтобы вы получали вещи высшего качества и не соглашались на меньшее. Canoe призван вдохновлять.</p>
              </MediaQuery>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
