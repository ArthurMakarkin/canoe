import React, { Component } from 'react';
import Scroll from 'react-scroll';
import MediaQuery from 'react-responsive';

import SimilarProducts from 'app/components/SimilarProducts';
import * as filters from 'app/constants/Filters';

import initScrollReveal from 'app/animations/technologies';


export default class NanoSilver extends Component {
  componentDidMount() {
    initScrollReveal();
  }

  play(){
    document.getElementById('video').play();
    document.getElementById('icons').classList.remove('hidden');
    document.getElementById('playBtn').classList.add('hidden');
  }

  render() {
    return (
      <section className="page-content technology">
        <div className="content-wrapper">
          <MediaQuery query='(max-width: 768px)'>
            <div>
              <div className="technologie-header full-width" style={{
              backgroundImage: 'url(/assets/images/technologies/mobile/nanosilver-header.jpg)',
              backgroundColor: '#ADBCC5',
                }}
              >
              </div>
              <div className="tech-name">
                <p className="title">Nano Silver</p>
                <p className="subtitle">Серебряная защита<br />от бактерий</p>
              </div>
              <div className="video nano-silver">
                <video id="video" src="//media.canoe.ru/videos/nano-silver.mp4" poster="/assets/images/technologies/nano-silver-poster.jpg" loop />
                <svg id="playBtn" className="icon-play" onClick={this.play}><use xlinkHref="/assets/images/sprite.svg#icon-play" /></svg>
                <span id="icons" className="icons hidden" />
              </div>
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-header full-width" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/nanosilver-wide.jpg)',
              backgroundColor: '#ADBCC5',
            }}>
              <Scroll.Link className="scroll-link" to="content" smooth offset={-170} duration={1000}>
                <svg className="scroll-icon"><use xlinkHref="//media.canoe.ru/images/sprite.svg#scroll" /></svg>
              </Scroll.Link>
              <div className="video nano-silver">
                <video src="//media.canoe.ru/videos/nano-silver.mp4" autoPlay loop />
                <span className="icons" />
              </div>
            </div>
          </MediaQuery>

          <div className="technologie-description nano-silver" id="content" style={{
            backgroundImage: 'url(//media.canoe.ru/images/technologies/decorations/radials-2.svg)',
            backgroundPosition: '18% 36%',
            paddingTop: '180px',
            paddingBottom: '180px',
            backgroundSize: 'auto 88%',
          }}>
            <h2>новейшая методика обогащения шерсти<br />наноразмерными частицами серебра</h2>
            <div className="text">Cегодня вопрос о защите — как от пагубного воздействия внешней среды,<br />так и от вредной микрофлоры самого человека — стоит очень остро.<br />Современный способ профилактики целого спектра распространённых заболеваний —<br /><span className="text-highlight">умные аксессуары</span> с содержанием <span className="text-highlight">наночастиц серебра</span>, которые<br />справляются с этой задачей <span className="text-highlight">быстро и эффективно</span>.</div>
          </div>

          <div className="technologie-photos-grid">
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/nano-silver/mobile/image-1.jpg)',
            }} />
            <div className="photo w50 h50 no-padding">
              <div className="photo w100 h50 no-padding">
                <div className="photo w100 h100" style={{
                  backgroundImage: 'url(//media.canoe.ru/images/technologies/nano-silver/mobile/image-2.jpg)',
                }} />
              </div>
              <div className="photo w100 h50" style={{
                backgroundImage: 'url(//media.canoe.ru/images/technologies/nano-silver/mobile/image-3.jpg)',
              }} />
            </div>
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/nano-silver/mobile/image-4.jpg)',
            }} />
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/nano-silver/mobile/image-5.jpg)',
            }} />
          </div>

          <div className="technologie__nano-silver_bg full-width">
            <div className="technologie-trapezium nano-silver">
              <div className="content">
                <div className="subtitle">Двойной эффект: защита от вредных микроорганизмов<br />и контроль неприятного запаха.</div>
                <div className="text">Технология обработки шерсти <span className="text-highlight">Nano Silver</span> позволяет головным уборам<br />предотвращать рост бактерий и предупреждать появление<br />неприятного запаха. Уже при концентрации <span className="text-highlight">0,1 мг/л серебро</span> обладает<br />явным противогрибковым действием. </div>
              </div>
            </div>

            <div className="technologie-diagrams">
              <div className="row" style={{
                backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/nano-silver-1.png)',
                backgroundPosition: '50% 0',
                backgroundSize: '450px auto',
                padding: '140px 0 105px',
              }}>
                <div className="column">
                  <div className="title">Серебряная защита</div>
                  <div className="text">Защищает от воздействия <span className="text-highlight">болезнетворных организмов и вредной микрофлоры</span>.</div>
                  <div className="icon" style={{
                    backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/nano-silver-3.1.svg)',
                  }}></div>
                </div>
                <img src="//media.canoe.ru/images/technologies/diagrams/nano-silver-1.png" className="mobile-diagram" />
                {' '}
                <div className="column">
                  <div className="title">Чувствительная кожа</div>
                  <div className="text">Подходит для поврежденной и чувствительной кожи, восстанавливает <span className="text-highlight">естественный кожный барьер</span>.</div>
                  <div className="icon" style={{
                    backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/nano-silver-3.2.svg)',
                  }}></div>
                </div>
              </div>

              <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>

              <div className="row" style={{
                backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/nano-silver-2.png)',
                backgroundPosition: '50% 0',
                backgroundSize: '410px auto',
                padding: '130px 0 105px',
              }}>
                <div className="column">
                  <div className="title">Свежесть и комфорт</div>
                  <div className="text"><span className="text-highlight">Нейтрализует бактерии</span>, вызывающие нежелательный запах.</div>
                  <div className="icon" style={{
                    backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/nano-silver-3.3.svg)',
                  }}></div>
                </div>
                <img src="//media.canoe.ru/images/technologies/diagrams/nano-silver-2.png" className="mobile-diagram" />
                {' '}
                <div className="column">
                  <div className="title">Долговечность</div>
                  <div className="text">Обеспечивает изделию свежесть и опрятный вид даже при регулярной носке.</div>
                  <div className="icon" style={{
                    backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/nano-silver-3.4.svg)',
                  }}></div>
                </div>
              </div>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div className="technologie-style-banner banner-with-centered-content no-height full-width">
              <img src="//media.canoe.ru/images/technologies/nano-silver/mobile/banner-1.jpg" />
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-style-banner full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/nano-silver/banner-1.jpg)' }} />
          </MediaQuery>


          <div className="technologie-diagram-description full-width">
            <div className="content">
              <div className="title mobile-show-title">Как действует технология nano silver?</div>
              <div className="text">Натуральное волокно обрабатывается гидрозолем с содержанием<br />активного компонента — наноразмерного серебра — при температуре<br />40 градусов в несколько этапов, пока общая площадь поверхности частиц<br />серебра в составе шерстяного полотна не составит около 50%.<br />Такая концентрация может <span className="text-highlight">эффективно препятствовать<br />размножению бактерий</span> на коже даже в экстремальных условиях.</div>
              <MediaQuery query='(max-width: 768px)'>
                <div style={{ padding: '20px 0 60px', width: '80%', margin: '0 auto' }}>
                  <img src="//media.canoe.ru/images/technologies/diagrams/nano-silver-4-mobile.svg" alt="" />
                </div>
              </MediaQuery>
              <MediaQuery query='(min-width: 769px)'>
                <div style={{ padding: '40px 0 0 140px' }}>
                  <img src="//media.canoe.ru/images/technologies/diagrams/nano-silver-4.svg" alt="" />
                </div>
              </MediaQuery>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div className="technologie-style-banner banner-with-centered-content full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/nano-silver/mobile/banner-2.jpg)' }}>
              <div className="content">
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                <div className="text">Nano Silver — проверенная защита от болезнетворных бактерий<br />и нежелательного запаха даже в экстремальным ситуациях.<br />Крепкий иммунитет вместе с Canoe!</div>
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              </div>
            </div>
          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-style-banner banner-with-centered-content full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/nano-silver/banner-2.jpg)' }}>
              <div className="content" style={{ color: '#fff' }}>
                <div className="text">Nano Silver — проверенная защита от болезнетворных бактерий<br />и нежелательного запаха даже в экстремальным ситуациях.<br />Крепкий иммунитет вместе с Canoe!</div>
              </div>
            </div>
          </MediaQuery>

          <SimilarProducts
            filters={filters.NANO_SILVER}
            title="Товары с этой технологией"
          />
        </div>
      </section>
    );
  }
}
