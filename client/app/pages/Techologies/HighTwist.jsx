import React, { Component } from 'react';
import Scroll from 'react-scroll';
import MediaQuery from 'react-responsive';

import SimilarProducts from 'app/components/SimilarProducts';
import * as filters from 'app/constants/Filters';

import initScrollReveal from 'app/animations/technologies';

export default class HighTwist extends Component {
  componentDidMount() {
    initScrollReveal();
  }

  play(){
    document.getElementById('video').play();
    document.getElementById('icons').classList.remove('hidden');
    document.getElementById('playBtn').classList.add('hidden');
  }

  render() {
    return (
      <section className="page-content technology">
        <div className="content-wrapper">
          <MediaQuery query='(max-width: 768px)'>
            <div>
              <div className="technologie-header full-width" style={{
              backgroundImage: 'url(/assets/images/technologies/mobile/high-twist-header.jpg)',
              backgroundColor: '#d01530',
                }}
              >
              </div>
              <div className="tech-name">
                <p className="title">High Twist</p>
                <p className="subtitle">Уверенность<br />в каждом движении</p>
              </div>
              <div className="video high-twist">
                <video id="video" src="//media.canoe.ru/videos/high-twist.mp4" poster="/assets/images/technologies/high-twist-poster.jpg" loop />
                <svg id="playBtn" className="icon-play" onClick={this.play}><use xlinkHref="/assets/images/sprite.svg#icon-play" /></svg>
                <span id="icons" className="icons hidden" />
              </div>
            </div>
          </MediaQuery>
          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-header full-width" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist-wide.jpg)',
              backgroundColor: '#d01530',
            }}>
              <Scroll.Link className="scroll-link" to="content" smooth offset={-170} duration={1000}>
                <svg className="scroll-icon"><use xlinkHref="/assets/images/sprite.svg#scroll" /></svg>
              </Scroll.Link>
              <div className="video high-twist">
                <video src="//media.canoe.ru/videos/high-twist.mp4" autoPlay loop />
                <span className="icons" />
              </div>
            </div>
          </MediaQuery>

          <div className="technologie-description mobile-show-bg" id="content" style={{
            backgroundImage: 'url(//media.canoe.ru/images/technologies/decorations/knit-1.svg)',
            backgroundPosition: '50% 60%',
            paddingTop: '180px',
            paddingBottom: '180px',
          }}>
            <div className="title">Революция в движении,<br />технология <strong>высокого кручения</strong></div>
            <div className="text">Сегодня Москва — завтра Токио. Сегодня такси — завтра велосипед.<br />Сегодня марафон — завтра дачный отдых с друзьями. В современном мире<br />всё меняется молниеносно. Но есть вещи, постоянные в своём совершенстве.<br />Головные уборы Canoe с технологией High Twist сохраняют <br />идеальную форму. Всегда.</div>
          </div>

          <div className="technologie-photos-grid">
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist/mobile/image-1.jpg)',
            }} />
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist/mobile/image-2.jpg)',
            }} />
            <div className="photo w50 h50" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist/mobile/image-3.jpg)',
            }} />
            <div className="photo w50 h50 no-padding">
              <div className="photo w100 h50" style={{
                backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist/mobile/image-4.jpg)',
              }} />
              <div className="photo w100 h50 no-padding">
                <div className="photo w50 h100" style={{
                  backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist/mobile/image-5.jpg)',
                }} />
                <div className="photo w50 h100" style={{
                  backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist/mobile/image-6.jpg)',
                }} />
              </div>
            </div>
          </div>

          <div className="technologie-trapezium bg-hightwist full-width">
            <i className="bg" />
            <div className="content">
              <div className="title">Стильное <strong>превосходство</strong></div>
              <div className="text">Головной убор, который моментально завоюет статус незаменимого в любом гардеробе: лёгкий и эластичный, он прекрасно садится и выглядит ненавязчиво стильно. Технология <span className="text-highlight">High Twist</span> предотвращает появление катышков и заломов, поэтому даже после нескольких активных сезонов изделия держат форму и выглядят опрятно.</div>
            </div>
          </div>

          <div className="technologie-diagrams">
            <div className="row" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/high-twist-1.png)',
              backgroundPosition: '43% 0',
              padding: '120px 0 150px',
            }}>
              <div className="column">
                <div className="title">Хочется потрогать</div>
                <div className="text">Тончайшая высококрученная пряжа обладает особенными тактильными свойствами — она невероятно мягкая, но при этом упругая, что позволяет ей быстро восстанавливать форму и не растягиваться со временем.</div>
              </div>
              <img src="//media.canoe.ru/images/technologies/diagrams/high-twist-1.png" className="mobile-diagram" />
              {' '}
              <div className="column">
                <div className="title">Всегда по погоде</div>
                <div className="text">Легкий, дышащий материал с превосходной терморегуляцией — в головных уборах Canoe будет комфортно даже в тёплую погоду.</div>
              </div>
            </div>

            <div className="technologie-trapezium bg-hightwist mobile-trapezium full-width">
              <i className="bg" />
            </div>

            <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>

            <div className="row" style={{
              backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/high-twist-2.png)',
              backgroundPosition: '53% 0',
              padding: '150px 0 50px',
            }}>
              <div className="column">
                <div className="title">Долговечность</div>
                <div className="text">Изделия не подвержены пиллингу, это значит, что при носке они не образуют «катышки», устойчивы к микроразрывам и деформации.</div>
              </div>
              <img src="//media.canoe.ru/images/technologies/diagrams/high-twist-2.png" className="mobile-diagram" />
              {' '}
              <div className="column">
                <div className="title">Идеальная форма</div>
                <div className="text">Если такую пряжу потянуть, нити стремятся как можно быстрее вернуться в исходное положение, что позволяет головным уборам долго сохранять первоначальный вид и наилучшим образом облегать голову.</div>
              </div>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div className="technologie-style-banner banner-with-centered-content no-height full-width">
              <img src="//media.canoe.ru/images/technologies/high-twist/mobile/banner-1.jpg" />
              <div className="content content-absolute">
                <div style={{ fontSize: '80px', fontWeight: 300, lineHeight: '60px' }}>600</div>
                <div style={{ fontSize: '28px', fontWeight: 300, lineHeight: '40px', textTransform: 'uppercase' }}>оборотов</div>
              </div>
            </div>
          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-style-banner banner-with-centered-content full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist/banner-1.jpg)' }}>
              <div className="content">
                <div style={{ fontSize: '200px', fontWeight: 200, lineHeight: 1 }}>600</div>
                <div style={{ fontSize: '18px', fontWeight: 700, textTransform: 'uppercase' }}>оборотов</div>
              </div>
            </div>
          </MediaQuery>

          <div className="technologie-diagram-description full-width" style={{
            backgroundImage: 'url(//media.canoe.ru/images/technologies/diagrams/high-twist-3.1.svg)',
            backgroundPosition: '50% 0',
            paddingTop: '50px',
          }}>
            <div className="content">
              <div className="title mobile-show-title">Технология <strong>High Twist</strong></div>
              <div className="text">Особый способ крутки пряжи, при котором количество оборотов<br />на метр составляет <span className="text-highlight">600</span>, что на <span className="text-highlight">50%</span> превышает количество оборотов<br />в обычной пряже. В процессе обработки волокна сжимаются, как пружина,<br />под действием радиального давления, трение между ними растёт,<br />и это позволяет устойчиво закрепить нити в скрученном положении.</div>
              <MediaQuery query='(max-width: 768px)'>
                <div style={{ padding: '20px 0 60px', width: '80%', margin: '0 auto' }}>
                  <img src="//media.canoe.ru/images/technologies/diagrams/high-twist-3.2.svg" alt="" />
                </div>
              </MediaQuery>
              <MediaQuery query='(min-width: 769px)'>
                <div style={{ padding: '120px 0 70px' }}>
                  <img src="//media.canoe.ru/images/technologies/diagrams/high-twist-3.2.svg" alt="" />
                </div>
              </MediaQuery>
            </div>
          </div>

          <MediaQuery query='(max-width: 768px)'>
            <div className="technologie-style-banner banner-with-centered-content full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist/mobile/banner-2.jpg)' }}>
              <div className="content">
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                <div className="text">Для технологии высокой крутки подходит<br />только самая лучшая шерсть тонкорунных<br />мериносов</div>
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              </div>
            </div>
          </MediaQuery>

          <MediaQuery query='(min-width: 769px)'>
            <div className="technologie-style-banner banner-with-centered-content full-width" style={{ backgroundImage: 'url(//media.canoe.ru/images/technologies/high-twist/banner-2.jpg)' }}>
              <div className="content">
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
                <div className="text">Для технологии высокой крутки подходит<br />только самая лучшая шерсть тонкорунных<br />мериносов</div>
                <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
              </div>
            </div>
          </MediaQuery>

          <div className="technologie-description last-description">
            <div className="title">Каждый день может быть не похож на предыдущий,<br />но место <strong>комфорту</strong> и <strong>элегантному стилю</strong><br />в нём найдётся всегда.</div>
          </div>

          <SimilarProducts
            filters={filters.HIGH_TWIST}
            title="Товары с этой технологией"
          />
        </div>
      </section>
    );
  }
}
