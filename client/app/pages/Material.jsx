import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import Preloader from 'app/components/Preloader';
import SimilarProducts from 'app/components/SimilarProducts';
import Swiper from 'react-id-swiper';
// import demoMaterial from 'app/demo_data/material';

import MaterialBanner from './Material/Banner';
import MaterialHeader from './Material/Header';
import MaterialPicture2Cols from './Material/Picture2Cols';
import MaterialShopBanner from './Material/ShopBanner';
import MaterialProperties from './Material/Properties';
import MaterialEnd from './Material/End';

import * as actions from 'app/actions';

class Material extends Component {
  render() {
    let picture2cols;
    const { material } = this.props;
    const materials = this.props.materials || [];
    const currentIndex = materials.findIndex(m => m.URL === this.props.location.pathname);

    if (material && material.PageContent && material.PageContent.length > 0) {
      const contents = {};
      for (const slug of ['banner', 'header', 'catalogue', 'shop_banner', 'end', 'properties']) {
        contents[slug] = material.PageContent.find(c => c.block_name === `Materials_${slug}`).contents[0];
      }
      picture2cols = material.PageContent.find(c => c.block_name === 'Materials_picture2cols').contents;
      return (
        <section className="page-content material-page">
          <div className="content-wrapper">
            { contents.banner && <MaterialBanner {...contents.banner} /> }

            {
              materials.length > 0 &&
              <div className="b-material__materials">
                <Swiper
                  spaceBetween={40}
                  slidesPerView={10}
                  navigation={{
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev"
                  }}
                  initialSlide={currentIndex}
                >
                  {materials.map((item, index) =>
                    <Link className={`b-material__materials__item ${item.URL === this.props.location.pathname ? 'current' : ''}`} to={item.URL} key={index}>
                      <span
                        className="b-material__materials__item__icon"
                        style={{ backgroundImage: `url(${item.Icon})` }}
                      />
                      <span className="b-material__materials__title">{item.Title}</span>
                    </Link>
                  )}
                </Swiper>
              </div>
            }

            { contents.header && <MaterialHeader {...contents.header} /> }

            { picture2cols &&
              <div className="b-twocols">
                <div className="b-twocols__sides">
                  { picture2cols.map((col, index) => <MaterialPicture2Cols key={index} {...col} {...{ index }} />) }
                </div>
              </div>
            }

            { contents.shop_banner && <MaterialShopBanner {...contents.shop_banner} /> }
            { contents.properties && <MaterialProperties {...contents.properties} /> }
            { contents.end && <MaterialEnd {...contents.end} /> }

            { contents.catalogue && contents.catalogue.Title &&
            <SimilarProducts
              title={contents.catalogue.Title}
              filters={[contents.catalogue.CatalogFilterValue]}
            />
            }
          </div>
        </section>
      );
    }
    return (
      <section className="page-content material-page">
        <Preloader />
      </section>
    );
  }
}

function mapStateToProps(state) {
  const { material, materials } = state;
  return {
    material,
    materials,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Material);
