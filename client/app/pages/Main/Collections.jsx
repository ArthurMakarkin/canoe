import React, { PureComponent } from 'react';
import { Link } from 'react-router';

const $ = window.jQuery;

export default class InstagramFeed extends PureComponent {
  componentDidMount() {
    this.initSliders();
  }

  initSliders() {
    const sliders = $('.main-page-carousel');
    if (sliders.hasClass('slick-initialized')) return;

    const prevArrow = '<button  class="slick-arrow slick-prev"><svg class="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" stroke-miterlimit="10"><path d="M7.707.354l-7 7 7 7M.707 7.354h18"/></g></svg></button>';
    const nextArrow = '<button  class="slick-arrow slick-next"><svg class="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" stroke-miterlimit="10"><path d="M11 14.354l7-7-7-7M18 7.354H0"/></g></svg></button>';

    const settings = {
      centerMode: true,
      variableWidth: true,
      initialSlide: 1,
      centerPadding: '40px',
      focusOnSelect: true,
      speed: 500,
      autoplay: true,
      pauseOnHover: false,
      autoplaySpeed: 5000,
      prevArrow,
      nextArrow,
    };

    sliders.slick(settings);
  }

  render() {
    return (
      <div className="collections full-width" style={{ backgroundColor: this.props.collections[0].BackgroundColor}}>
        <div className="text-with-decorations">
          <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
          <div className="text" dangerouslySetInnerHTML={{ __html: this.props.collections.shift().Title }} />
          <svg className="decoration svg-decoration-1 flipped"><use xlinkHref="/assets/images/sprite.svg#decoration-1" /></svg>
        </div>

        <div className="main-page-carousel">
          {
            this.props.collections.map((collection, index) => (
              <Link to={collection.URL} className="slide" key={index}>
                <img className="photo" src={collection.Image.Url} alt={collection.Title} />
                <span className="name" dangerouslySetInnerHTML={{ __html: collection.Title }} />
              </Link>
            ))
          }
        </div>
      </div>
    );
  }
}
