import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import getImageSize from 'app/mixins/imageSizes';


export class headerBanner extends Component {
  render() {
    const { pagewidth } = this.props.app;
    const { headerBanner } = this.props;
    const bg = getImageSize(pagewidth, headerBanner.BackgroundImage.Url, 'tallBanner');
    const image = getImageSize(pagewidth, headerBanner.Image.Url, 'centeredImage');
    return (
      <div
        className="header-banner full-width darkened"
        style={{ backgroundImage: `url(${bg})` }}
      >
        <Link
          to={headerBanner.URL}
          className="collection-look"
        >
          <div className="image" style={{ backgroundImage: `url(${image})` }} />
          <div className="text">
            <div className="title">{headerBanner.Title}</div>
            <div className="subtitle">{headerBanner.Text}</div>
          </div>
        </Link>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { app } = state;
  return {
    app,
  };
}

export default connect(mapStateToProps)(headerBanner);
