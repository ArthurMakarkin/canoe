import React from 'react';
import { Link } from 'react-router';

export default ({ campaign }) => (
  <div className="campaign full-width"  style={{ backgroundColor: campaign.BackgroundColor}}>
    <div className="text-with-decorations">
      <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
      <Link to={campaign.URL} className="text" dangerouslySetInnerHTML={{ __html: campaign.Title }} />
      <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
    </div>
    <div className="info" dangerouslySetInnerHTML={{ __html: campaign.Text }} />
    <div className="button-wrapper">
      <Link className="main-button dark" to={campaign.URL}>{campaign.Button.text}</Link>
    </div>
  </div>
);
