import React, { PureComponent } from 'react';
import fetchJsonp from 'fetch-jsonp';

export default class InstagramFeed extends PureComponent {
  state = {
    feed: [],
  }

  componentDidMount() {
    this.getPhotos();
  }

  getPhotos = () => {
    const URL = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=558996003.5dc505e.c0323dc721b74c47bd4cc31b188cda5d&count=8';
    fetchJsonp(URL).then(response => response.json()).then((responseJson) => {
      this.setState({ feed: responseJson.data });
    });
  }

  render() {

    return (

      <div className="instagram-feed full" >
        {this.state.feed ? this.state.feed.map((item, index) =>
          <a
            href={item.link}
            key={index}
            target="_blank"
            rel="noopener noreferrer"
            className="instagram-feed-item"
            style={{ backgroundImage: `url(${item.images.standard_resolution.url})` }}
          />
        ) : null}
      </div>
    );
  }
}

