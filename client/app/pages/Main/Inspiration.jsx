import React from 'react';

export default ({ inspiration }) => (
  <div className="inspiration full-width" style={{ backgroundColor: inspiration.BackgroundColor}}>
    <div className="content-wrapper">
      <div className="intro">
        <div className="title">{inspiration.Title}</div>
        <div className="text" dangerouslySetInnerHTML={{ __html: inspiration.Text }} />
        {/* <Link className="main-button dark" to="/brand/philosophy">Наша философия</Link> */}
      </div>
      <div className="image" style={{ backgroundImage: `url(${inspiration.Image.Url})` }} />
    </div>
  </div>
);
