import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import MediaQuery from 'react-responsive';

import Preloader from 'app/components/Preloader';
import MobileGallery from 'app/components/MobileGallery';
import Partners from './Partners';
import Quotes from './Quotes';
import InstagramFeed from './InstagramFeed';
import InstagramFeedMobile from './InstagramFeedMobile';
import News from './News';
import Facts from './Facts';
import Campaign from './Campaign';
import Technologies from './Technologies';
import Collections from './Collections';
import Inspiration from './Inspiration';
import Catalogue from './CataloguePart';
import HeaderBanner from './HeaderBanner';


import initScrollReveal from 'app/animations/main';

import * as actions from 'app/actions';

class MainPage extends Component {
  componentWillMount() {
    this.props.actions.fetchMain();
  }

  componentDidMount() {
    initScrollReveal();
  }

  render() {
    const main = this.props.main;
    const contents = {};

    if (main) {
      ['header-banner', 'campaign', 'facts', 'technologies', 'collections', 'catalogue-part', 'inspiration', 'news', 'socialmedias', 'partners_logo', 'recomedtations'].forEach((slug) => {
        contents[slug] = main.PageContent.find(c => c.block_name === `${slug}`).contents;
      });
      return (
        <section className="page-content main-page" style={{ backgroundColor: main.PageMeta.BackgroundColor}}>
          <div className="content-wrapper">
            { contents['header-banner'] &&
            contents['header-banner'][0] &&
            <HeaderBanner headerBanner={contents['header-banner'][0]} />
            }

            { contents.campaign &&
            contents.campaign.map((campaign, index) => (
              <Campaign key={index} campaign={campaign} />
            ))
            }

            { contents.facts &&
              <div className="facts-container">
                <MediaQuery query='(min-width: 769px)'>
                  <Facts facts={contents.facts} />
                </MediaQuery>
                <MediaQuery query='(max-width: 768px)'>
                  <MobileGallery name="facts" content={contents.facts} />
                </MediaQuery>
              </div>
            }

            {/* contents.technologies &&
              <div>
                <MediaQuery query='(min-width: 769px)'>
                  <Technologies technologies={contents.technologies} />
                </MediaQuery>
                <MediaQuery query='(max-width: 768px)'>
                  <MobileGallery name="technologies-main" content={contents.technologies} />
                </MediaQuery>
              </div>
              */
            }

            { contents.collections &&
              <div>
                <MediaQuery query='(min-width: 769px)'>
                  <Collections collections={contents.collections} />
                </MediaQuery>
                <MediaQuery query='(max-width: 768px)'>
                  <MobileGallery name="collections" content={contents.collections} />
                </MediaQuery>
              </div>
            }

            { contents['catalogue-part'] &&
            (contents['catalogue-part'][0]) &&
              <Catalogue catalogue={contents['catalogue-part'][0]} />
            }

            <MediaQuery query='(min-width: 769px)'>
              <div>
                { contents.inspiration &&
                contents.inspiration.map((inspiration, index) => (
                  <Inspiration inspiration={inspiration} key={index} />
                ))
                }

                { contents.news &&
                  <News news={contents.news} />
                }
              </div>
            </MediaQuery>

            { contents.recomedtations &&
              <Quotes items={contents.recomedtations} />
            }

            { contents.partners_logo &&
              <Partners items={contents.partners_logo} />
            }

            { contents.socialmedias &&
            contents.socialmedias.map((social, socialIndex) => (
              <div key={socialIndex}>
                <MediaQuery query='(min-width: 769px)'>
                  <div className="socialmedias full-width">
                    <a href={social.URL} target="_blank" rel="noopener noreferrer" className="hashtag">{social.Title}</a>
                    <div className="text" dangerouslySetInnerHTML={{ __html: social.Text }} />
                    <a href={social.URL} target="_blank" rel="noopener noreferrer" className="instagram-link">Instagram</a>
                    <InstagramFeed />
                  </div>
                </MediaQuery>
                <MediaQuery query='(max-width: 768px)'>
                  <div>
                    <InstagramFeedMobile hashtag={social.Title} />
                  </div>
                </MediaQuery>
              </div>
            ))
            }

          </div>
        </section>
      );
    }
    return <Preloader />;
  }
}


function mapStateToProps(state) {
  const { main } = state;
  return {
    main,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
