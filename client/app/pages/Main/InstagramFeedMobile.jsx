import React, { PureComponent } from 'react';
import fetchJsonp from 'fetch-jsonp';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';

const $ = window.jQuery;

export default class InstagramFeedMobile extends PureComponent {
  state = {
    feed: [],
  }

  componentDidMount() {
    this.getPhotos();
  }

  getPhotos = () => {
    const URL = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=558996003.5dc505e.327739f7f7c34fe8805a7a5714cdfa3c&count=8';
    fetchJsonp(URL).then(response => response.json()).then((responseJson) => {
      this.setState({ feed: responseJson.data });
    });
  }

  onRefresh(inst) {
    $(".instagram-feed .instagram-container").width($(".instagram-feed .instagram-container .instagram-item").length * 120);
  }

  render() {
    const options = {
      scrollX: true,
      scrollY: false,
      // snap: '.item',
      // probeType: 3,
      // bounce: false,
      // bounceTime: 0,
      // deceleration: 0,
      // momentum: false,
      // mouseWheel: true,
      click: true,
      preventDefault: false,
      eventPassthrough: true,
    }

    return (
      <div className="instagram">
        { this.props.hashtag &&
          <div className="instagram__header">
            <div className="instagram__header__title">{this.props.hashtag}</div>
            <div className="instagram__header__description">Вдохновение подарило нам <strong>Canoe</strong>. Мы дарим вам вдохновение на новые открытия и свершения.</div>
          </div>
        }
        {this.state.feed ? <ReactIScroll
            className="instagram-feed b-iscroll"
            onRefresh={(inst) => this.onRefresh(inst)}
            options={options}
            iScroll={iScroll}>
              <div className="instagram-container b-iscroll-container">
                {this.state.feed.map((item, index) =>
                  <a
                    href={item.link}
                    key={index}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="instagram-item"
                    style={{ backgroundImage: `url(${item.images.standard_resolution.url})` }}
                  />
                )}
              </div>
          </ReactIScroll> : null}
        <a href={`https://www.instagram.com/explore/tags/${this.props.hashtag.replace('#', '')}`} target="_blank" rel="noopener noreferrer" className="instagram-link">Открыть в Instagram</a>
      </div>
    );
  }
}

