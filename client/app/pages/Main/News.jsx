import React from 'react';

export default ({ news }) => (
  <div className="news" style={{ backgroundColor: news[0].BackgroundColor}}>
    <div className="huge-title">
      <span className="text">{news.shift().Title}</span>
    </div>
    <div className="news-list">
      {
        news.map((newsItem, index) => (
          <div className="news-list-item" key={index}>
            <div className="date">
              <span className="number">{newsItem.Date.day}</span>
              <span className="month">{newsItem.Date.month}</span>
            </div>
            <a
              href={newsItem.URL}
              className="photo"
              style={{ backgroundImage: `url(${newsItem.Image.Url})` }}
              target="_blank"
              rel="noopener noreferrer"
            />
            <a
              href="http://www.the-village.ru/village/business/story/149849-canoe"
              className="title"
              target="_blank"
              rel="noopener noreferrer"
            >
              {newsItem.Title}
            </a>
            <div className="annotation" dangerouslySetInnerHTML={{ __html: newsItem.Text }} />
            <a
              href={newsItem.URL}
              target="_blank"
              rel="noopener noreferrer"
              className="link"
            >
              узнать подробнее
            </a>
          </div>
        ))
      }
    </div>
  </div>
);

