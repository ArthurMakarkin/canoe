import React, { PureComponent } from 'react';
import { Link } from 'react-router';

const $ = window.jQuery;

export default class InstagramFeed extends PureComponent {
  componentDidMount() {
    this.initSliders();
  }

  initSliders() {
    const sliders = $('.main-page-carousel');
    if (sliders.hasClass('slick-initialized')) return;

    const prevArrow = '<button  class="slick-arrow slick-prev"><svg class="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" stroke-miterlimit="10"><path d="M7.707.354l-7 7 7 7M.707 7.354h18"/></g></svg></button>';
    const nextArrow = '<button  class="slick-arrow slick-next"><svg class="arrow" width="18.707" height="14.707" viewBox="0 0 18.707 14.707"><g fill="none" stroke="currentColor" stroke-miterlimit="10"><path d="M11 14.354l7-7-7-7M18 7.354H0"/></g></svg></button>';

    const settings = {
      centerMode: true,
      variableWidth: true,
      initialSlide: 1,
      centerPadding: '40px',
      focusOnSelect: true,
      speed: 500,
      autoplay: true,
      pauseOnHover: false,
      autoplaySpeed: 5000,
      prevArrow,
      nextArrow,
    };

    sliders.slick(settings);
  }

  render() {
    return (
      <div className="technologies">
        <div className="huge-title">
          <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
          <Link to="/technologies" className="text">Технологии</Link>
          <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
        </div>
        <div className="main-page-carousel technologies-carousel full-width">
          {
            this.props.technologies.map((technology, index) => (
              <Link to={technology.URL} className="slide" key={index}>
                <img className="photo" src={technology.Image.Url} alt={technology.Title} />
                <span className="name" dangerouslySetInnerHTML={{ __html: technology.Title }} />
              </Link>
            ))
          }
        </div>
      </div>
    );
  }
}
