import React, { PureComponent } from 'react';
import ReactIScroll from 'react-iscroll';
import iScroll from 'iscroll/build/iscroll';
import MediaQuery from 'react-responsive';
import classNames from 'classnames';

const $ = window.jQuery;

export default class Quotes extends PureComponent {
  constructor(props){
    super(props);

    this.quotes = props.items;

    this.prev = this.prev.bind(this);
    this.next = this.next.bind(this);
    this.updateArrows = this.updateArrows.bind(this);

    this.state = {
      active: 0,
      leftArrowHidden: true,
      rightArrowHidden: false
    }

  }

  prev(){
    this.instance.prev();
    this.updateArrows();
    // this.instance.scrollBy(, 0);
  }

  next(){
    this.instance.next();
    this.updateArrows();
  }

  updateArrows(){
    const currentPage = this.instance.currentPage.pageX;
    let leftArrowHidden = false;
    let rightArrowHidden = false;
    if(currentPage === 0){
      leftArrowHidden = true;
    } else if (currentPage === this.instance.pages.length - 1) {
      rightArrowHidden = true;
    }
    this.setState({
      active: currentPage,
      leftArrowHidden,
      rightArrowHidden
    });
  }

  onRefresh(inst) {
    const scroller = $(inst.scroller);
    this.instance = inst;
    const items = scroller.find(".quote");
    const itemWidth = $(window).width() <= 768 ? $(window).width() : $('.partners-list').width()/2 + 20;
    items.width(itemWidth);
        // const frameWidth = this.quotes.length * ($(window).width() <= 768 ? $(window).width() : 560);

    scroller.width(items.length * itemWidth);
    this.updateArrows();
  }

  render() {
    const options = {
      scrollX: true,
      scrollY: false,
      snap: true,
      // bounce: true,
      // bounceEasing: 'quadratic',
      // bounceTime: 500,
      // snap: window.device.phone ? '.partner-logo' : true,
      momentum: false,
      click: true,
      preventDefault: false,
      eventPassthrough: true
    }

    const { leftArrowHidden, rightArrowHidden, active } = this.state;

    return (

      <div className="partners-list">  
        <div className="wrapper wrapper-quotes">
          <p className="title">Нас рекомендуют</p>
          <MediaQuery query='(min-width: 769px)'>
            {!leftArrowHidden && 
              <div onClick={this.prev} className="arrow-bg left" onClick={this.prev}>
                <svg className="arrow svg-arrow-left">
                  <use xlinkHref="/assets/images/sprite.svg#arrow-left" />
                </svg>
              </div>
            }
            {!rightArrowHidden && 
              <div onClick={this.next} className="arrow-bg right">
                <svg className="arrow svg-arrow-right">
                  <use xlinkHref="/assets/images/sprite.svg#arrow-right" />
                </svg>
              </div>
            }
          </MediaQuery>
          <ReactIScroll
            className="b-iscroll"
            options={options}
            onRefresh={(inst) => this.onRefresh(inst)}
            onScrollEnd={this.updateArrows}
            iScroll={iScroll}>
            <div className="quotes b-iscroll-container">
              {this.quotes.map((quote, i) =>
                <div key={`quote-${i}`} className="quote">
                  <div className="padding">
                    <div className="in">
                      <p className="text">{quote.Text}</p>
                      <p className="author">{quote.Title}</p>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </ReactIScroll>
          <ul className="dots">
            {this.quotes.map((quote, i) =>
              <li key={`quotes-dots-${i}`} className={classNames({"active": active === i})} />
            )}
          </ul>
        </div>
      </div>
    );
  }
}

