import React from 'react';
import SimilarProducts from 'app/components/SimilarProducts';
import { Link } from 'react-router';
import * as filters from 'app/constants/Filters';


export default ({ catalogue }) => (
  <div className="catalogue-part" style={{ backgroundColor: catalogue.BackgroundColor}}>
    <div className="huge-title">
      <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
      <Link to={catalogue.URL} className="text">{catalogue.Title}</Link>
      <svg className="decoration svg-decoration-2"><use xlinkHref="/assets/images/sprite.svg#decoration-2" /></svg>
    </div>

    <div className="subtitle" dangerouslySetInnerHTML={{ __html: catalogue.Text }} />

    <SimilarProducts
      filters={filters.MAIN_PAGE}
      title=""
    />
  </div>
);
