import React from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';

export default ({ facts }) => (
  <div className="facts full-width" style={{ backgroundColor: facts[0].BackgroundColor}}>
    <div className="wrapper">
      {
        facts.map((fact, index) => (
          <div
            className={classNames({
              row: true,
              reversed: index % 2 === 1,
            })}
            key={index}
          >
            <Link to={fact.ImageURL} className="photo">
              <div className="image" style={{ backgroundImage: `url(${fact.Image.Url})` }} />
            </Link>
            {' '}
            <Link to={fact.URL} className="fact fact-1">
              <div className="number" dangerouslySetInnerHTML={{ __html: fact.Title }} />
              <div className="text-wrapper">
                <div className="text" dangerouslySetInnerHTML={{ __html: fact.Text }} />
              </div>
            </Link>
          </div>
        ))
      }
    </div>
  </div>
);
