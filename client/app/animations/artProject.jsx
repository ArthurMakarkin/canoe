import scrollReveal from 'scrollreveal';

const sr = scrollReveal();

export default function initScrollReveal() {
  if(document.getElementById('appContainer').classList.contains('mobile')){
    return false;
  }

  sr.reveal('.diagonal-blocks .block > .image', {
    duration: 1500,
    delay: 500,
    distance: '200px',
    easing: 'ease',
  });

  sr.reveal('.diagonal-blocks .block > .images', {
    duration: 1500,
    delay: 500,
    distance: '200px',
    easing: 'ease',
  });

  sr.reveal('.diagonal-blocks .block:first-child .text', {
    duration: 1500,
    delay: 0,
    distance: '0px',
    easing: 'ease',
    scale: 1,
  });

  sr.reveal('.diagonal-blocks .block:not(:first-child) .text', {
    duration: 1500,
    delay: 1000,
    distance: '0px',
    easing: 'ease',
    scale: 1,
  });

  sr.reveal('.products-block .text', {
    duration: 1500,
    delay: 0,
    distance: '0px',
    easing: 'ease',
    scale: 1,
  });

  sr.reveal('.products-block .products', {
    duration: 1500,
    delay: 500,
    distance: '200px',
    easing: 'ease',
  });

  sr.reveal('.centered-photo .image', {
    duration: 1500,
    distance: '100px',
    scale: 0.8,
    opacity: 1,
    easing: 'ease',
  });

  sr.reveal('.centered-photo .bg-left', {
    duration: 1500,
    distance: '200px',
    easing: 'ease',
    origin: 'left',
  });

  sr.reveal('.centered-photo .text:not(.columns)', {
    duration: 1500,
    delay: 0,
    distance: '0px',
    easing: 'ease',
    scale: 1,
  });

  sr.reveal('.centered-photo .columns', {
    duration: 1500,
    delay: 500,
    distance: '0px',
    easing: 'ease',
    scale: 1,
  });

  sr.reveal('.centered-photo .bg-right', {
    duration: 1500,
    distance: '200px',
    easing: 'ease',
    origin: 'right',
  });
}
