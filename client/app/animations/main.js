import scrollReveal from 'scrollreveal';

const sr = scrollReveal();

export default function initScrollReveal() {
  if(document.getElementById('appContainer').classList.contains('mobile')){
    return false;
  }

  sr.reveal('.header-banner .image', {
    duration: 2000,
    delay: 100,
    distance: '0',
    scale: 0.95,
    opacity: 0,
    easing: 'ease',
  });

  sr.reveal('.facts .image', {
    duration: 1500,
    delay: 100,
    distance: '200px',
    scale: 1,
    opacity: 0,
    easing: 'ease',
  });

  sr.reveal('.inspiration .image', {
    duration: 1500,
    delay: 100,
    distance: '0',
    scale: 1,
    opacity: 0,
    easing: 'ease',
  });
}
