import scrollReveal from 'scrollreveal';

const sr = scrollReveal();

export default function initScrollReveal() {
  if(document.getElementById('appContainer').classList.contains('mobile')){
    return false;
  }

  sr.reveal('.video .icons', {
    duration: 2000,
    delay: 100,
    distance: 0,
    scale: 1,
    opacity: 0,
    easing: 'ease',
  });

  sr.reveal('.technologie-description .title, .technologie-description .text', {
    duration: 1500,
    delay: 100,
    distance: '200px',
    scale: 1,
    opacity: 0,
    easing: 'ease',
  });

  sr.reveal('.technologie-trapezium .title, .technologie-trapezium .text, .technologie-diagram-description .content', {
    duration: 1500,
    delay: 100,
    distance: '200px',
    scale: 1,
    opacity: 0,
    easing: 'ease',
  });

  sr.reveal('.technologie-photos-grid .photo, .technologie-two-photos .photo', {
    duration: 1500,
    delay: 100,
    distance: '100px',
    scale: 1,
    opacity: 0,
    easing: 'ease',
  });

  sr.reveal('.technologie-diagrams', {
    duration: 1500,
    delay: 0,
    distance: 0,
    scale: 1,
    opacity: 0,
    easing: 'ease',
  });

  sr.reveal('.technologie-diagrams .column:first-child', {
    duration: 1500,
    delay: 500,
    distance: '150px',
    scale: 1,
    opacity: 0,
    easing: 'ease',
    origin: 'left',
  });

  sr.reveal('.technologie-diagrams .column:last-child', {
    duration: 1500,
    delay: 500,
    distance: '150px',
    scale: 1,
    opacity: 0,
    easing: 'ease',
    origin: 'right',
  });

  sr.reveal('.technologie-style-banner', {
    duration: 1500,
    delay: 0,
    distance: '200px',
    scale: 1,
    opacity: 0,
    easing: 'ease',
    viewFactor: 0.1,
  });
}
