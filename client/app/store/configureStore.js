import { applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk';
import persistState from 'redux-localstorage';

import rootReducer from 'app/reducers';

export default function configureStore(initialState) {
  const isDev = process.env.NODE_ENV === 'development';

  return createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(thunk),
      persistState(['app', 'user']),
      isDev && window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  );
}
