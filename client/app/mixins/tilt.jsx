const $ = window.jQuery;

export default function tiltAnimation(id) {
  setTimeout(function(){
    const block = $(`#${id}`);
    if(!block.hasClass('inited')){
      block.addClass('inited');
      let windowWidth;
      let minLeft;

      function handleOrientationChange(){
        windowWidth = $(window).width();
        minLeft = windowWidth/2 - 318;
      }

      handleOrientationChange();

      function handleOrientation(event) {
        let gamma = event.gamma;

        if(gamma < -40){
          gamma = -40;
        } else if(gamma > 40){
          gamma = 40;
        }

        block.css({
          left: minLeft + gamma * minLeft/40
        });
      }

      window.removeEventListener("deviceorientation", handleOrientation, true);
      window.addEventListener("deviceorientation", handleOrientation, true);
      window.removeEventListener('orientationchange', handleOrientationChange, true);
      window.removeEventListener('resize', handleOrientationChange, true);
      window.addEventListener('orientationchange', handleOrientationChange, true);
      window.addEventListener('resize', handleOrientationChange, true);
    }
  }, 200)
}
