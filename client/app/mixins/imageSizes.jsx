// tallBanner 1334х560 — высокий заходной слайд (главная), баннер в карточке товара
// shortBanner 1334х360 — низкий заходной слайд (каталог), баннер в карточке товара
// centeredImage 1238х560 — центрированная картинка-баннер
// product 1500x1872 — фото товара размерами под зум
// preview 326х408 — превью товара
// square 560х560 — превью товара


export default function getImageSize(windowWidth, src, type) {
  // console.log(width, src, type);
  let width = 0,
     height = 0;

  if(windowWidth >= 1000) {
    return src;
  } else {
    switch(type){
      case 'tallBanner':
        width = 1334;
        height = 560;
        break;
      case 'shortBanner':
        width = 1334;
        height = 360;
        break;
      case 'centeredImage':
        width = 1238;
        height = 560;
        break;
      case 'product':
        width = 1500;
        height = 1872;
        break;
      case 'preview':
        width = 326;
        height = 408;
        break;
      case 'square':
        width = 560;
        height = 560;
        break;
    }
    return src.replace('/image/', `/image_thumb/${width}/${height}/`);
  }
}
