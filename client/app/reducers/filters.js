import * as types from 'app/constants/Actions';

const initialState = {
  filters: [],
};

export default function filters(state = initialState, action) {
  switch (action.type) {
    case types.RECEIVE_FILTERS:
      return action.filters;
    default:
      return state;
  }
}
