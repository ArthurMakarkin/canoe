import * as types from 'app/constants/Actions';

const initialState = {
  current: null,
  prefetched: [],
};

export default function product(state = initialState, action) {
  switch (action.type) {
    case types.RECEIVE_PRODUCT:
      return Object.assign({}, state, {
        current: action.product,
      });
    case types.PREFETCH_PRODUCT:
      return Object.assign({}, state, {
        prefetched: state.prefetched.concat(action.product),
      });
    case types.CLEAR_PREFETCHED_PRODUCTS:
      return Object.assign({}, state, {
        prefetched: [],
      });
    default:
      return state;
  }
}
