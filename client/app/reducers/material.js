import * as types from 'app/constants/Actions';

const initialState = [];

export default function material(state = initialState, action) {
  switch (action.type) {
    case types.RECEIVE_MATERIAL:
      return Object.assign({}, action.material, {
        id: action.id,
      });
    default:
      return state;
  }
}
