import * as types from 'app/constants/Actions';

const initialState = null;

export default function main(state = initialState, action) {
  switch (action.type) {
    case types.RECEIVE_MAIN:
      return action.data;
    default:
      return state;
  }
}
