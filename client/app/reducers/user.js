import * as types from 'app/constants/Actions';

const initialState = {
  AccountUUID: 0,
  AccountTYPE: 'retail',
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case types.RECEIVE_USER:
      return action.user;
    default:
      return state;
  }
}
