import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import app from './app';
import basket from './basket';
import errors from './errors';
import filters from './filters';
import user from './user';
import product from './product';
import products from './products';
import material from './material';
import materials from './materials';
import main from './main';

export default combineReducers({
  app,
  basket,
  errors,
  filters,
  user,
  product,
  products,
  material,
  materials,
  main,
  routing: routerReducer,
});
