import * as types from 'app/constants/Actions';

const initialState = [];

export default function materials(state = initialState, action) {
  switch (action.type) {
    case types.RECEIVE_MATERIALS:
      return action.materials;
    default:
      return state;
  }
}
