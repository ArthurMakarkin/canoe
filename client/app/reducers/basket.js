import * as types from 'app/constants/Actions';

const initialState = {};

export default function app(state = initialState, action) {
  switch (action.type) {
    case types.RECEIVE_BASKET:
      return action.basket;
    default:
      return state;
  }
}
