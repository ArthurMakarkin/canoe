import * as types from 'app/constants/Actions';
import * as filters from 'app/constants/Filters';

const initialState = {
  activeFilters: filters.DEFAULT,
  sort: 'catalog',
  catalogueView: 'grid',
  wholesaleMode: false,
  pagewidth: 0,
  footer: true,
};

export default function app(state = initialState, action) {
  switch (action.type) {
    case types.UPDATE_FILTERS:
      return Object.assign({}, state, {
        activeFilters: action.filters,
      });
    case types.UPDATE_CATALOGUE_VIEW:
      return Object.assign({}, state, {
        catalogueView: action.view,
      });
    case types.UPDATE_CATALOGUE_SORTING:
      return Object.assign({}, state, {
        sort: action.sort,
      });
    case types.SET_WHOLESALE_MODE:
      return Object.assign({}, state, {
        wholesaleMode: action.wholesaleMode,
      });
    case types.SET_PAGE_WIDTH:
      return Object.assign({}, state, {
        pagewidth: action.pagewidth,
      });
    case types.HIDE_FOOTER:
      return Object.assign({}, state, {
        footer: action.footer,
      });
    case types.SHOW_FOOTER:
      return Object.assign({}, state, {
        footer: action.footer,
      });
    default:
      return state;
  }
}
