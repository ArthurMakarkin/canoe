import React from 'react';
import { render } from 'react-dom';
import { Router, Route, useRouterHistory } from 'react-router';
import qs from 'qs';
import createBrowserHistory from 'react-router/node_modules/history/lib/createBrowserHistory';

const browserHistory = useRouterHistory(createBrowserHistory)({
  parseQueryString(...args) {
    return qs.parse.apply(null, args);
  },
});

import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';

import App from 'app/containers/App';

import Main from 'app/pages/Main';
import Catalogue from 'app/pages/Catalogue';
import Product from 'app/pages/product/Product';
import Orders from 'app/pages/Orders/Orders';
import Order from 'app/pages/Order/Order';
import Profile from 'app/pages/Profile';
import Basket from 'app/pages/Basket/Basket';
import Material from 'app/pages/Material';

import Legend from 'app/pages/Brand/Legend';
import About from 'app/pages/Brand/About';
import Philosophy from 'app/pages/Brand/Philosophy';

import Technologies from 'app/pages/Techologies/Index';
import Teflon from 'app/pages/Techologies/Teflon';
import H2Dry from 'app/pages/Techologies/H2Dry';
import HighTwist from 'app/pages/Techologies/HighTwist';
import ThreeDTube from 'app/pages/Techologies/3DTube';
import NanoSilver from 'app/pages/Techologies/NanoSilver';

import Contest from 'app/pages/Contest/Contest';
import Lookbook from 'app/pages/Collections/Lookbook';
import ArtProject from 'app/pages/Collections/ArtProject';
import NightSpeed from 'app/pages/Collections/NightSpeed';
import NewHorizons from 'app/pages/Collections/NewHorizons';

import Contacts from 'app/pages/Contacts';
import Sizes from 'app/pages/Sizes';
import Conditions from 'app/pages/Conditions';

import NotFound from 'app/pages/NotFound';

import configureStore from 'app/store/configureStore';
import * as actions from 'app/actions';

import * as filters from 'app/constants/Filters';

import './styles';

const APP_VERSION = '1.0.1';

if (localStorage.getItem('appVersion') !== APP_VERSION) {
  localStorage.removeItem('redux');
  localStorage.setItem('appVersion', APP_VERSION);
}

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

store.dispatch(actions.fetchBasket());

const updateProduct = (nextState, replace, callback) => {
  const { URLName } = nextState.params;
  store.dispatch(actions.fetchProduct(URLName));
  callback();
};

const updateMaterial = (nextState, replace, callback) => {
  const { id } = nextState.params;
  store.dispatch(actions.fetchMaterial(id));
  store.dispatch(actions.fetchMaterials());
  callback();
};

const setFilter = (nextState, replace) => {
  switch (nextState.params.style) {
    case 'all':
      store.dispatch(actions.updateFilters(filters.ALL));
      break;
    case 'men':
      store.dispatch(actions.updateFilters(filters.MEN));
      break;
    case 'women':
      store.dispatch(actions.updateFilters(filters.WOMEN));
      break;
    case 'boys':
      store.dispatch(actions.updateFilters(filters.BOYS));
      break;
    case 'girls':
      store.dispatch(actions.updateFilters(filters.GIRLS));
      break;
    case 'children':
      store.dispatch(actions.updateFilters(filters.CHILDREN));
      break;
    case 'premium':
      store.dispatch(actions.updateFilters(filters.PREMIUM));
      break;
    case 'neo_classic':
      store.dispatch(actions.updateFilters(filters.NEO_CLASSIC));
      break;
    case 'street_fashion':
      store.dispatch(actions.updateFilters(filters.STREET_FASHION));
      break;
    case 'teens':
      store.dispatch(actions.updateFilters(filters.TEENS));
      break;
    default:
      store.dispatch(actions.updateFilters(nextState.params.style.split("+")));
      break;
  }
  replace('/catalogue');
};

const hideFooter = () => {
  store.dispatch(actions.hideFooter());
}

const showFooter = () => {
  store.dispatch(actions.showFooter());
}

const auth = (nextState, replace) => {
  replace('/catalogue');
  setTimeout(() => {
    document.querySelector('.header-forms').classList.add('active');
  }, 500);
};

const signup = (nextState, replace) => {
  replace('/catalogue');
  setTimeout(() => {
    document.querySelector('.header-forms').classList.add('active');
    document.querySelector('.header-forms').classList.add('registration');
  }, 500);
};

const logout = (nextState, replace) => {
  store.dispatch(actions.logOut());
  replace('/catalogue');
};

render(
  <Provider store={store}>
    <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
      <Route component={App}>
        <Route path="/" component={Main} />

        <Route path="/catalogue" component={Catalogue} />
        <Route path="/catalogue/:style" onEnter={setFilter} />
        <Route
          path="/catalogue/product/:URLName"
          component={Product}
          onEnter={updateProduct}
        />

        <Route path="/orders" component={Orders} />
        <Route path="/orders/:id" component={Order} />
        <Route path="/profile" component={Profile} />
        <Route path="/basket" component={Basket} />

        <Route path="/auth" onEnter={auth} />
        <Route path="/signup" onEnter={signup} />
        <Route path="/logout" onEnter={logout} />

        <Route path="/brand/legend" component={Legend} />
        <Route path="/brand/about" component={About} />
        <Route path="/brand/philosophy" component={Philosophy} />

        <Route path="/technologies" component={Technologies} />
        <Route path="/technologies/teflon" component={Teflon} />
        <Route path="/technologies/h2dry" component={H2Dry} />
        <Route path="/technologies/high-twist" component={HighTwist} />
        <Route path="/technologies/3d-tube" component={ThreeDTube} />
        <Route path="/technologies/nano-silver" component={NanoSilver} />

        <Route
          path="/materials/:id"
          component={Material}
          onEnter={updateMaterial}
        />

        <Route path="/collections/horizons" component={NewHorizons} />
        <Route path="/collections/night" component={NightSpeed} />
        <Route path="/collections/art-project" component={ArtProject} />
        <Route path="/collections/:lookbook" component={Lookbook} />

        <Route path="/contest" component={Contest} onEnter={hideFooter} onLeave={showFooter} />

        <Route path="/contacts" component={Contacts} />
        <Route path="/sizes" component={Sizes} />
        <Route path="/conditions" component={Conditions} />

        <Route path="/order/success" component={Basket} />
        <Route path="/order/fail" component={Basket} />

        <Route path="*" component={NotFound} onEnter={hideFooter} onLeave={showFooter} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);
